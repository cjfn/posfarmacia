
@extends('layouts.app')



@section('content')

<meta name="csrf-token" content="{{ csrf_token() }}" />



<div class="container" ng-app="app" ng-controller="myCtrl">
    <div class="row" >
 
        <div class="col-md-12">

             
    <div class="panel panel-primary">
                <div class="panel-heading">AJUSTES DE TUS PRODUCTOS E INVENTARIO
                </div>
                
                
    <div class="panel-body">
    <h2>Realiza configuraciones en tus productos y en tus ventas</h2>
    @if ($message = Session::get('success'))

      <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
      </div>
    @endif
    
    <center>
      
      <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#home">Inventario</a></li>
        <li><a data-toggle="tab" href="#menu1">Ventas</a></li>
      </ul>
      <div class="alert alert-warning alert-dismissable">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>¡Cuidado! <span class="fa fa-warning"></span></strong> Todos los cambios que realices seran monitoreados
              </div>
               <div class="container">
      <div class="row">
        
        <div class="col-sm-10">
        <input id="nSearch" class="form-control" type="text" placeholder="Busqueda por codigo o nombre" aria-label="Search">
        </div>
        <div class="col-sm-2">

          <div class="btn btn-default" onclick="searchData()"><span class="fa fa-search"></span></div>
          <span class="fa fa-refresh" title="Nueva Busqueda" onclick="newSearch()"></span>
        </div>
      </div>
    </div>

      <div class="tab-content">
        <div id="home" class="tab-pane fade in active">
          <h3>INVENTARIO</h3>
          <div class="table-responsive">
          <table class="table table-bordered" id="MyTable">
          <a href="{{ URL::previous() }}" class="btn btn-default"><span class="fa fa-mail-reply"></span>Atras</a>
         
          </center>

           <thead>
        <tr>
          <th>opciones</th>
          <th>Cod </th>
          <th>Nombre</th>
          <th>Precio Compra</th>
          <th>Precio Venta</th>
          <th>existencias</th>
          <th>vencimiento </th>
          <th>generico </th>
          <th>Sucursal </th>
          <th>Laboratorio </th>
          
        </tr>
      </thead>
      <tbody>
      @foreach($data as $x)
        <tr>
          <td>
                    <button class="btn btn-info" data-toggle="modal" data-target="#viewModal" onclick="fun_view('{{$x -> id}}')" title="ver"><span class="fa fa-eye"></span> DETALLES</button>


                    @if(Auth::user()->nombre_tipo_usuario=='ADMIN'||Auth::user()->nombre_tipo_usuario=='ENCARGADO')
                    <button class="btn btn-warning" data-toggle="modal" data-target="#editModal" onclick="fun_edit('{{$x -> id}}')"><span class="fa fa-pencil-square" title="editar"></span>MODIFICAR</button>
                    @else

                    @endif
                    
                </td>
          <td><strong>{{$x -> codigo_producto}}</strong></td>
          <td>{{$x -> nombre}}</td>
          <td>{{$x -> precio_compra}}</td>
          <td>{{$x -> precio_venta}}</td>
          <td>{{$x -> existencia}}</td>
          <td>{{$x -> fecha_vencimiento}}</td>
          <td>{{$x -> generico}}</td>
          <td>{{$x -> sucursalname}}</td>
          <td>{{$x -> lab}}</td>

          
              </tr>
             @endforeach
            </tbody>
          </table>

        {{ $data->links() }}
        </div>
        </div>
        <div id="menu1" class="tab-pane fade">
          <h3>Administra tus facturas</h3>
         <div class="table-responsive">
          <table class="table table-bordered" id="MyTable2">
          <center>
          

          
          </center>
            <thead>
              <tr>
                <th>Numero</th>
                <th>total</th>
                <th>fecha</th>
                <th>opciones</th>
               
              </tr>
            </thead>
            <tbody>
            @foreach($data2 as $x)
              <tr>
                <td>{{$x -> id}}</td>
                <td>{{$x -> total}}</td>
                <td>{{$x -> fecha_created}}</td>
               

                <td>
                   
                    <a href="{{ url('/detalle_factura_show') }}/{{$x -> id}}" class="btn btn-info"> <span class="fa fa-eye"></span>DETALLES</a>
                    
                </td>
              </tr>
             @endforeach
            </tbody>
          </table>
        </div>
          <input type="hidden" name="hidden_view_fac" id="hidden_view_fac" value="{{url('laboratorios/view')}}">
          <input type="hidden" name="hidden_delete_fac" id="hidden_delete_fac" value="{{url('laboratorios/delete')}}">
        </div>
        <div id="menu2" class="tab-pane fade">
          <h3>Menu 2</h3>
          <p>Some content in menu 2.</p>
        </div>
      
<br/>



    <input type="hidden" name="hidden_view" id="hidden_view" value="{{url('medicinas/view')}}">
    <input type="hidden" name="hidden_delete" id="hidden_delete" value="{{url('medicinas/delete')}}">
    <!-- Add Modal start -->
    <div class="modal fade" id="addModalProd" role="dialog" >
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content"  >
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" >&times;</button>
            <h4 class="modal-title">Agregar nuevo producto</h4>
          </div>
          <div class="modal-body">
            <form action="{{ url('medicinas') }}" method="post">
              {{ csrf_field() }}



              <div class="form-group">
                  <label for="first_name">Codigo:*</label>
                  <input type="text" class="form-control" id="codigo_producto" ng-change="buscar(codigo)" ng-model="codigo" tabindex="1" name="codigo_producto" required="true" disabled>
                </div>
              <div class="form-group">
                  <label for="nombre">Nombre del Producto:*</label>
                  <input type="text" class="form-control" tabindex="2" id="nombre" name="nombre" required="true">
                </div>
                <div class="form-group">
                  <label for="precio_venta">Precio de venta:*</label>
                  <input type="number" class="form-control" tabindex="3" id="precio_venta" name="precio_venta" required="true" ng-change="calculo(venta, desc)" ng-model="venta">
                </div>
              
                
                <div class="form-group">
                  <label for="last_name">Aplica descuento:</label>
                  <input type="radio" id="aplica_descuento" name="aplica_descuento" value="4" tabindex="4" checked> Si<br>
                </div>
                <div class="form-group">
                  <label for="last_name">Porcentaje descuento:</label>
                  <input type="number" title="ejemplo: 5 = 5%" class="form-control"   name="descuento" id="descuento" tabindex="5" required="true" ng-change="calculo(venta, desc)" value="0" ng-model="desc">
                </div>
                 <div class="form-group">
                  <label for="last_name">Descuento:</label>
                  <input type="number" class="form-control" id="porcentaje_descuento" name="porcentaje_descuento" required="true" readonly="readonly">
                </div>
                
                <div class="form-group">
                  <label for="precio_compra">Precio de compra*:</label>
                  <input type="text" class="form-control" id="precio_compra" readonly="readonly" name="precio_compra">
                </div>
                <div class="form-group">
                  <label for="existencia">Cantidad ingreso*:</label>
                  <input type="number" class="form-control" id="existencia" name="existencia" tabindex="6"required="true">
                </div>
                <div class="form-group">
                  <label for="factura">Factura:</label>
                  <input type="text" title="Numero de factura referencia de compra" class="form-control" id="factura" name="factura" tabindex="7">
                </div>
                <div class="form-group">
                  <label for="laboratorio_id">Laboratorio:</label>
                 
                 
                </div>
                
               
                  <input type="hidden" class="form-control" value="0" id="registro" tabindex="10" name="registro" required="true">
                
                <div class="form-group">
                  <label for="fecha_nacimiento">Fecha de vencimiento:</label>
                  <input type="date" class="form-control" id="fecha_vencimiento"  tabindex="11" name="fecha_vencimiento" required="true" disabled>
                </div>

                <div class="form-group">
                  <label for="fecha_nacimiento">generico:</label>
                   <select  name="generico" id="generico" class="form-control">
                      <option value="NO">NO</option>
                      <option value="SI">SI</option>
                   </select>
                </div>
              
              
              <button type="submit" class="btn btn-success" ng-click="addItemB()"  tabindex="13">Agregar</button>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-window-close"></span>Cerrar</button>
          </div>
        </div>
        
      </div>
    </div>
    <!-- add code ends -->
  <!-- View Modal start -->
    <div class="modal fade" id="viewModal" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Detalles</h4>
          </div>
          <div class="modal-body">
            <center><span class="fa fa-camera fa-3x"></span></center>
            <p><b>Codigo de producto : </b><span id="view_codigo_producto" class="text-success"></span></p>
            <p><b>Aplica Descuento : </b><span id="view_aplica_descuento" class="text-success"></span></p>
            <p><b>Porcentaje de descuento : </b><span id="view_porcentaje_descuento" class="text-success"></span></p>
            <p><b>Nombre : </b><span id="view_nombre" class="text-success"></span></p>
             <p><b>Precio de compra : </b><span id="view_precio_compra" class="text-success"></span></p>
            <p><b>Precio de venta : </b><span id="view_precio_venta" class="text-success"></span></p>
            <p><b>Existencias : </b><span id="view_existencia" class="text-success"></span></p>
            <p><b>Factura : </b><span id="view_factura" class="text-success"></span></p>
            <p><b>Laboratorio : </b><span id="view_laboratorio_id" class="text-success"></span></p>
            <p><b>Sucursal : </b><span id="view_sucursal" class="text-success"></span></p>
            <p><b>Registro : </b><span id="view_registro" class="text-success"></span></p>
            <p><b>Fecha de vencimiento : </b><span id="view_fecha_vencimiento" class="text-success"></span></p>
            <p><b>Generico : </b><span id="view_generico" class="text-success"></span></p>
            <p><b>Observaciones : </b><span id="view_observaciones" class="text-success"></span></p>
            
            
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"></button>
          </div>
        </div>
        
      </div>
    </div>
    <!-- view modal ends -->
    <!-- Edit Modal start -->
    <div class="modal fade" id="editModal" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Editar</h4>
          </div>
          <div class="modal-body">
            <form action="{{ url('medicinas/update') }}" method="post">
              {{ csrf_field() }}
              <div class="form-group">
                <div class="form-group">
                  <label for="">Codigo_producto :</label>
                  <input type="number" class="form-control" id="edit_codigo_producto" name="edit_codigo_producto" readonly="readonly">
                </div>
                <div class="form-group">
                  <input type="hidden" class="form-control" id="edit_aplica_descuento" name="edit_aplica_descuento">
                </div>
                <div class="form-group">
                  <label> Porcentaje descuento</label>
                  <input type="text" class="form-control" id="edit_porcentaje_descuento" name="edit_porcentaje_descuento">
                </div>
                <div class="form-group">

                  <label> Nombre</label>
                  <input type="text" class="form-control" id="edit_nombre" name="edit_nombre">
                </div>
                 <div class="form-group">

                  <label> Precio venta</label>
                  <input type="text" class="form-control" id="edit_precio_venta" name="edit_precio_venta">
                </div>
                 <div class="form-group">

                  <label> Precio compra</label>
                  <input type="text" class="form-control" id="edit_precio_compra" name="edit_precio_compra">
                </div>
                 <div class="form-group">

                  <label for="edit_existencia">Nueva existencia: :</label>
                  <input type="number" class="form-control" id="edit_existencia" name="edit_existencia">
                </div>
                 <div class="form-group">

                  <label> #factura</label>
                  <input type="number" class="form-control" id="edit_factura" name="edit_factura">
                </div>
                 <div class="form-group">

                  <label> laboratorio nuevo</label>

                  <input type="hidden" class="form-control" id="edit_laboratorio_id" name="edit_laboratorio_id">
                  <select id="laboratorio_id" onclick="val()" name="laboratorio_id" required="true"  tabindex="8" class="form-control">
                      @foreach($lab as $l)
                  
                      <option value="{{ $l -> id}}">Cod: {{ $l -> id}} -{{ $l -> nombre}} </option>
                  
                  @endforeach
                   </select>
                </div>
                <div class="form-group">
                  <label> sucursal/bodega nueva destino</label>
                  <input type="hidden" class="form-control" name="edit_sucursal" id="edit_sucursal">

                  <select id="sucursal" onclick="valsuc()" name="sucursal" title="seleccione sucursal de bodega" required="true" tabindex="9" class="form-control">
                      @foreach($sucursal as $s)
                  
                      <option value="{{ $s -> id}}">Cod: {{ $s -> id}} - {{ $s -> nombre}} </option>
                  
                  @endforeach
                   </select>
                 



                </div>
                 <div class="form-group">
                  <input type="hidden" class="form-control" id="edit_registro" name="edit_registro">
                </div>
                 <div class="form-group">
                  <label>Fecha de vencimiento</label>
                  <input type="text" class="form-control" id="edit_fecha_vencimiento" name="edit_fecha_vencimiento" readonly="readonly" >
                </div>
                 <div class="form-group">

                  <label>Generico:</label>
                   <input type="text"  name="edit_generico" id="edit_generico" class="form-control">
                   
                  
                </div>
                <div class="form-group">
                  <label for="edit_nombre">Observaciones :</label>
                  <textarea class="form-control" id="edit_observaciones" required="true" name="edit_observaciones">
                    Producto devuelto por fecha de vencimiento
                  </textarea>
                </div>
               
              </div>
              
              <button type="submit" class="btn btn-default"  ng-keydown="addItemC()">Modificar</button>
              <input type="hidden" id="edit_id" name="edit_id">
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>
          <input type="hidden" id="search" value="{{ url('/live_search/action/') }}">
        </div>
        
      </div>
    </div>
    <!-- Edit code ends -->
 
  </div>
  </div>
  </div>

    <script type="text/javascript">
      function val() {
          d = document.getElementById("laboratorio_id").value;
          document.getElementById("edit_laboratorio_id").value=d;
          //alert(d);
      }

      function searchData() {
        // busqueda de data
        var nSearch = $("#nSearch").val();
        var urlVar = $("#search").val();
        var union = urlVar+"/"+nSearch;
        $.ajax({
          url: union,
          method: 'get',
          dataType: 'json',
          success: function (response) {
            console.log(response);
            $('.pagination').hide();
            var carga = response.length;
            //alert(response[0].especie + ' ' + carga);
            $("#MyTable tr").remove(); 
            $('#MyTable > tbody:last-child').append('<tr>'+
                '<th>opciones</th>'+
          '<th>Cod </th>'+
          '<th>Nombre</th>'+
          '<th>Precio Compra</th>'+
          '<th>Precio Venta</th>'+
          '<th>existencias</th>'+
          '<th>vencimiento </th>'+
          '<th>generico </th>'+
          '<th>Sucursal </th>'+
          '<th>Laboratorio </th>'+
              '</tr>');
            for (j = 0; j < carga; j++) {
              $('#MyTable > tbody:last-child').append('<tr>' +
                '<td>'+
                '<button class="btn btn-warning" data-toggle="modal" data-target="#editModal" onclick="fun_edit(' + response[j].id+')"><span class="fa fa-pencil-square" title="editar"></span>MODIFICAR</button>'+
                '</td>'+
                '<td class="border" style="line-height:12pt;"><!--D-->' + response[j].codigo_producto + '</td>' +
                '<td class="border" style="line-height:12pt;"><!--D-->' + response[j].nombre + '</td>' +
                '<td class="border" style="line-height:12pt;"><!--D-->' + response[j].precio_compra + '</td>' +
                '<td class="border" style="line-height:12pt;"><!--D-->' + response[j].precio_venta + '</td>' +
                '<td class="border" style="line-height:12pt;"><!--D-->' + response[j].existencia + '</td>' +
                '<td class="border" style="line-height:12pt;"><!--D-->' + response[j].fecha_vencimiento + '</td>' +
                '<td class="border" style="line-height:12pt;"><!--D-->' + response[j].generico + '</td>' +
                '<td class="border" style="line-height:12pt;"><!--D-->' + response[j].sucursalname + '</td>' +
                '<td class="border" style="line-height:12pt;"><!--D-->' + response[j].lab + '</td>' +
                '<td>'+
                  '<button class="btn btn-info" data-toggle="modal" data-target="#viewModal" onclick="fun_view('+response[j].id+')" title="ver"><span class="fa fa-eye"></span><strong>Ver Detalles</strong></button>'+
                '</td></tr >');
            }
            },

           error: function(response){
                 //alert("Error: "+response);
                 /*
                  <thead>
                    <tr>
                      <th>Cod </th>
                      <th>Nombre</th>
                      <th>Precio Compra</th>
                      <th>Precio Venta</th>
                      <th>existencias</th>
                      <th>vencimiento </th>
                      <th>generico </th>
                      <th>Sucursal </th>
                      <th>Laboratorio </th>
                      <th>opciones</th>
                    </tr>
                  </thead>
                  */
            }
          });
      }


      


      function valsuc() {
        alert('sucursal de origen no puede se modificada, realizar tralsado');
        /*
          d = document.getElementById("sucursal").value;
          document.getElementById("edit_sucursal").value=d;
          //alert(d);
          */
      }

        var app = angular.module('app', []).controller('myCtrl', function ($scope, $http) {
            // consultar todos los datos
            
                //datos en consola
            $scope.calculo = function (i, j) {
               
                var n1 = parseFloat(i).toFixed(2);
                var n2 = parseFloat(j).toFixed(2);

                if (j > 100) {
                    document.getElementById("porcentaje_descuento").value = "";
                    document.getElementById("precio_compra").value = "";
                    alert("Porcentaje de descuento debe de ser menor al 100%");
                }
                else {

                    var pdesc = (n1 * n2) / 100;
                    var pcompra = n1 - pdesc;

                    document.getElementById("porcentaje_descuento").value = pdesc;
                    document.getElementById("precio_compra").value = pcompra;
                }

            }

               $scope.addItemB = function(){
                    
        

                          var usuario = "{{ Auth::user()->email }}";
                          var tipo_proceso = "CREA PRODUCTO";
                          var codigo = document.getElementById("codigo_producto").value;
                          var descripcion = codigo;

                          //alert(id);
                          $http.post("{{ url('api/bitacora') }}?usuario="+usuario+"&tipo_proceso="+tipo_proceso+"&descripcion="+descripcion);

                          console.log("agregado en log");
                          //alert("agregado en log");

                    }

                     $scope.addItemC = function(){
                    
        

                          var usuario = "{{ Auth::user()->email }}";
                          var tipo_proceso = "MODIFICA PRODUCTO";
                          var codigo = document.getElementById("edit_codigo_producto").value;
                          var cantidad = document.getElementById("edit_existencia").value;
                          var descripcion = codigo+" cant."+cantidad;

                          //alert(id);
                          $http.post("{{ url('api/bitacora') }}?usuario="+usuario+"&tipo_proceso="+tipo_proceso+"&descripcion="+descripcion);

                          console.log("agregado en log");
                          alert("agregado en log");

                    }


             // buscar detalles de producto por id
             $scope.buscar = function (i) {

                $scope.errortext = "";
                var cod = parseInt(i);
                //servicio que busca todo de producto por id
                $http.get("{{ url('api/productos/') }}/" + cod).then(function (response) {
                    $scope.items = response.data;
                    $scope.results = [];
                    //datos en consola
                    //console.log(response.data[0]["nombre"]);
                    console.log(response.data);

                        if(response.data.length>1)
                    {
                        alert("Producto con este codigo ya existente");
                        



                    }
                    //alert("Producto con este codigo ya existente");
                    var name = response.data[0]["nombre"];
                    var precio = response.data[0]["precio_venta"];
                    var existencia = response.data[0]["existencia"];


                    
                    document.getElementById("nombre").value = name;
                    document.getElementById("precio_venta").value = precio;
                    document.getElementById("existencia").value = existencia;
                   
                    document.getElementById("nombre").focus();
 
                  });
               

                
                
              

            }




            

            });
        
        
    </script>

 <link href="{{ url('/') }}/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">

                 
                 <link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/1.10.15/css/jquery.dataTables.css">
                <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
                <script src="//code.jquery.com/jquery-2.1.3.min.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.js"></script>

                <script language="javascript" type="text/javascript">
                                    
                
                    $('#addModalProd').on('shown.bs.modal', function(){
                      $('#codigo_producto').focus();
                    });
                </script>

  <script type="text/javascript">
   function fun_view(id)
    {
      var view_url = $("#hidden_view").val();
      $.ajax({
        url: view_url,
        type:"GET", 
        data: {"id":id}, 
        success: function(result){
          //console.log(result);
          $("#view_codigo_producto").text(result.codigo_producto);
          $("#view_aplica_descuento").text(result.aplica_descuento);
          $("#view_porcentaje_descuento").text(result.porcentaje_descuento);
          $("#view_nombre").text(result.nombre);
          $("#view_precio_venta").text(result.precio_venta);
          $("#view_precio_compra").text(result.precio_compra);
          $("#view_existencia").text(result.existencia);
          $("#view_factura").text(result.factura);
          $("#view_laboratorio_id").text(result.lab);
          $("#view_sucursal").text(result.sucursal);
          $("#view_registro").text(result.registro);
          $("#view_fecha_vencimiento").text(result.fecha_vencimiento);
          $("#view_generico").text(result.generico);
          $("#view_observaciones").text(result.observaciones);
        }
      });
    }
 
    function fun_edit(id)
    {
      var view_url = $("#hidden_view").val();
      $.ajax({
        url: view_url,
        type:"GET", 
        data: {"id":id}, 
        success: function(result){
          //console.log(result);
          $("#edit_id").val(result.id);
          $("#edit_codigo_producto").val(result.codigo_producto);
          $("#edit_aplica_descuento").val(result.aplica_descuento);
          $("#edit_porcentaje_descuento").val(result.porcentaje_descuento);
          $("#edit_nombre").val(result.nombre);
          $("#edit_precio_venta").val(result.precio_venta);
          $("#edit_precio_compra").val(result.precio_compra);
          $("#edit_existencia").val(result.existencia);
          $("#edit_factura").val(result.factura);
          $("#edit_laboratorio_id").val(result.laboratorio_id);
          $("#edit_sucursal").val(result.sucursal);
          $("#edit_registro").val(result.registro);
          $("#edit_fecha_vencimiento").val(result.fecha_vencimiento);
          $("#edit_generico").val(result.generico);
          $("#edit_observaciones").val(result.observaciones);
        }
      });
    }
 
    function fun_delete(id)
    {
      var conf = confirm("Esta seguro que desea eliminar??");
      if(conf){
        var delete_url = $("#hidden_delete").val();
        $.ajax({
          url: delete_url,
          type:"POST", 
          data: {"id":id,_token: "{{ csrf_token() }}"}, 
          success: function(response){
            alert(response);
            location.reload(); 
          }
        });
      }
      else{
        return false;
      }
    }

    
  </script>


           


          </div>

          <!-- /.col-lg-12 -->

      </div>



  </div>

@endsection
