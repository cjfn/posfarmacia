
@extends('layouts.app')



@section('content')

<div class="container">
    <div class="row">
    <div class="col-md-3">
     <!-- Sidebar -->


    </div>
    <div class="col-md-12" >
         <div class="panel panel-default">
             
            <div class="panel panel-principal">
                <center>
                  <strong>
                 <a href="{{ url('/medicinas') }}" title="Busca prodctos disponibles en tu inventario" class="btn btn-default"> <span class="fa fa-search"></span>BUSCA PRODUCTOS</a>
                </strong>
                <strong>
                 <a href="{{ url('/medicinas') }}" class="btn btn-warning"> <span class="fa fa-search"></span>BUSCA LABORATORIOS</a>
                </strong>
                 
                 <strong>
                 <a href="#" class="btn btn-primary" > <span class="fa fa-cogs"></span>AJUSTES</a>
                </strong>
                <strong>
                 <a href="#" class="btn btn-success"> <span class="fa fa-money"></span>FACTURAS</a>
                </strong>
                <strong>
                 <a href="#" class="btn btn-success"> <span class="fa fa-cube"></span>CAJA CHICA</a>
                </strong>
             </center>
            </div>
        </div>
       

    <div class="panel panel-primary">
      <div class="panel-heading"> <h3 class="text-center ">DETALLES DE PEDIDO O FACTURA  <span class="btn btn-default btn-lg"><strong>NO.{{ $id }} </strong></span> <span class="btn btn-success btn-lg"><strong> TOTAL Q.{{ $factura["total"]}} </strong></span> </h3>
      </div>
      <div class="panel-body">

      <div ng-app="myApp" ng-controller="myCtrl">
     
       <div class="table-responsive">
            <table class="table table-bordered table-hover" >
              <thead>
                <tr>
                  <th>Cod</th>
                  <th>Producto</th>
                  <th>Cantidad</th>
                  <th>Subtotal</th>
                  <th>Opciones</th>
                  
                </tr>
              </thead>
              <tbody>
              @foreach($data as $x)
                <tr>
                  <td>{{$x -> codigo_producto}}</td>
                  <td>{{$x -> nombre}}</td>
                  <td>{{$x -> cantidad}}</td>
                  <td>{{$x -> subtotal}}</td>

                  <td>
                     
                      <button class="btn btn-danger" onclick="fun_delete('{{$x -> id}}')"><span class="fa fa-times-circle"></span>Eliminar</button>
                  </td>
                </tr>
               @endforeach
              </tbody>
            </table>
            <center>

                 </strong></span> <span class="btn btn-success btn-lg"><strong> <span class="fa fa-money"></span> TOTAL Q.{{ $factura["total"]}} </strong></span>
                 <input type="hidden" value="{{$factura['total']}}" name="total" id="total">
                 
                 <strong>
                 
                </strong>
            </center>

    <!-- Add Modal start -->
    <div class="modal fade" id="addModal" role="dialog" data-backdrop="static" data-keyboard="false">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <div class="container">
              <div class="row">
                 <div class="col-sm-3">
                  <h4 class="modal-title">PEDIDO No. </h4>
                  </div>
               <div class="col-sm-3"> <input type="text"  class="form-control" id="id_factura" name="id_factura" disabled></h4></div>
              </div>
            </div>
          </div>
          <div class="modal-body">
            <div class="row">
               <div class="col-sm-2">Nombre</div>
               <div class="col-sm-4"><input type="text" name="nombre" class="form-control"></div>
               <div class="col-sm-1">Nit</div>
               <div class="col-sm-3"><input type="text" name="nit" class="form-control"></div></div>
               
              </div>
              <div class="row">
                 <div class="col-sm-2">Direccion</div>
               <div class="col-sm-8"><input type="text" name="direccion" class="form-control"> 
                <br/>
            </div>
            
              </div>
              <div class="row">
                <div class="col-sm-12">
              <center>
                 
              <strong>
                <span ng-click="addItemF()" class="btn btn-success btn-lg" ><span class="fa fa-plus"></span>Agregar Productos</span>
              </strong>
            </center>
              <br/>
            </div>
              </div>
          </div>
          <div class="modal-footer">
           
          </div>
            
               </div>
            </div>

          </div>
            
             
               
              


               </div>
                 

                </div>
              
               <h2>
               
            </h2>

              
        </div>
        
      </div>
    </div>

        
    </div>


     <script>
        function loaded() {
            alert("Image is loaded");
        }

      var app = angular.module('myApp', []).controller('myCtrl', function ($scope, $http) {
        

           $scope.addFact = function(){
                      var usuario = "{{ Auth::user()->email }}";
                      $http.post("{{ url('api/ventas') }}?usuario="+usuario).then(function (response) {
                          $scope.items = response.data;
                          $scope.results = [];

                          document.getElementById("id_factura").value=response.data.id;

                          //alert("agregado en log");

                      });
                    }

          $scope.addItemF = function (x, y) {
                        $scope.errortext = "";
                        var id = document.getElementById("id_factura").value;
                        //alert(id);

                         if(id){
                           location.href ="{{ url('detalle_factura/') }}"+"/"+id;
                           }
                            else {
                            
                           }

                    }
            
        $scope.addItem = function () {
            $scope.errortext = "";
                var factura = document.getElementById("factura").value;
                var codigo_producto = document.getElementById("codigo_producto").value;
                var cantidad = parseInt(document.getElementById("cantidad").value);
                var subtotal = document.getElementById("subtotal").value;
                var fecha_vence = document.getElementById("fecha_vence").value;
                var existencia = parseInt(document.getElementById("existencia").value);
                var usuario = "{{ Auth::user()->email }}";

                //alert("factura"+factura + "codigo" + codigo_producto + "cantidad" + cantidad +  "subtotal" +subtotal + "vence" +fecha_vence);
                if(existencia<cantidad)
                {
                    alert('cantidad de productos mayor a la existente en inventario');
                    
                }
                else
                {

                      $http.post("{{ url('api/detalle') }}?factura="+factura+"&codigo_producto="+codigo_producto+"&cantidad="+cantidad+"&subtotal="+subtotal+"&fecha_vencimiento="+fecha_vence+"&usuario"+usuario);
                 

                               
                                 //alert("Click en el boton regargar para actualizar la pagina");
                                  console.log("agregado en log");
                                  //alert("agregado en log");
                                 $('#referesh').focus();

                                  alert("producto agregado exitosos");
                                 location.reload(true);
                 
                }


            
                       

                
                
                
            }

             $scope.calculo = function (i, j, k) {

                var valor = document.getElementById("valor").value;
                var cantidad = document.getElementById("cantidad").value;
                var descuento = document.getElementById("descuento").value;
               
                var resultado = (cantidad*valor)-((cantidad*valor)*(descuento/100));
                

              //lert(resultado);

                    document.getElementById("subtotal").value = resultado;
                

            }

             // buscar detalles de producto por id
             $scope.buscar = function (i) {

                $scope.errortext = "";
                var cod = parseInt(i);
                //servicio que busca todo de producto por id
                $http.get("{{ url('api/productos/') }}/" + cod).then(function (response) {
                    $scope.items = response.data;
                    $scope.results = [];
                    //datos en consola
                    //console.log(response.data[0]["nombre"]);
                    console.log(response.data);

                        if(response.data.length>1)
                    {
                        alert("Producto tiene dos existencias, seleccione fecha de vencimiento y laboratorio de origen");
                        document.getElementById("cantidad").focus();
                        for(var prop in response.data)
                        {
                            // var laboratorio = response.data[prop]["laboratorio_id"];
                             //alert(response.data[prop]["laboratorio_id"]);
                             //alert(response.data[prop]["fecha_vencimiento"]);
                             $("#fecha_vence").append('<option value='+response.data[prop]["fecha_vencimiento"]+'>'+response.data[prop]["fecha_vencimiento"]+'</option>');

                             $("#laboratorio").append('<option value='+response.data[prop]["laboratorio_id"]+'>'+response.data[prop]["laboratorio_id"]+'</option>');

                                var name = response.data[0]["nombre"];
                                var precio = response.data[0]["precio_venta"];
                                var existencia = response.data[0]["existencia"];

                                  document.getElementById("descripcion").value = name;
                                    document.getElementById("valor").value = precio;
                                    document.getElementById("existencia").value = existencia;
             
                        }



                    }
                    var name = response.data[0]["nombre"];
                    var precio = response.data[0]["precio_venta"];
                    var existencia = response.data[0]["existencia"];
                    var laboratorio = response.data[0]["laboratorio_id"];

                    var vencimiento = response.data[0]["fecha_vencimiento"];


                    
                    document.getElementById("descripcion").value = name;
                    document.getElementById("valor").value = precio;
                    document.getElementById("existencia").value = existencia;
                    $("#fecha_vence").append('<option value='+vencimiento+'>'+vencimiento+'</option>');

                    $("#laboratorio").append('<option value='+laboratorio+'>'+laboratorio+'</option>');

                    document.getElementById("cantidad").focus();
 
                  });
               

                
                
              

            }




    });

        
</script>

    </div>
    </div>
</div>
@endsection