<?php

namespace App\model\sucursal;

use Illuminate\Database\Eloquent\Model;

class sucursal extends Model
{
    //
     protected $table = 'sucursales';
	 protected $primarykey='id';
	 public $timestamps = false;
    
    protected $fillable = [
        'nombre', 'direccion', 'telefono', 'encargado', 'fecha_create', 'fecha_update', 'activo'
    ];
}
