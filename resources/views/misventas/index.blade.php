
@extends('layouts.app')


@section('content')

<meta name="csrf-token" content="{{ csrf_token() }}" />



<div class="container" ng-app="factura">
    <div class="row">
 
        <div class="col-md-12" ng-controller="addFactura">
          <div class="col-md-12">
            <div class="panel panel-default">
            <div class="panel-heading">Menu Acceso rapido - INGRESOS</div>
            <div class="panel-body">
               <a href="{{ url('/laboratorios') }}" title="ingrese laboratorios" class="btn btn-success"><span class="fa fa-plus"></span>Laboratorios</a>
            <a href="{{ url('/medicinas') }}" title="ingrese nuevos productos a su inventario" class="btn btn-success"><span class="fa fa-plus"></span>Medicinas</a>
            <a href="{{ url('/sucursales') }}" title="ingrese nuevas sucursales de su empresa para asignarles productos" class="btn btn-success"><span class="fa fa-plus"></span>sucursales</a>
            <a href="{{ url('/facturas_ingreso') }}" title="ingrese nuevas facturas con ingreso de nuevo producto" class="btn btn-success"><span class="fa fa-plus"></span>Mis Facturas</a>
            <a href="{{ url('/misventas') }}" title="ingrese nuevos pedidos o facturas de ventas" class="btn btn-success"><span class="fa fa-plus"></span>Pedidos</a>
          </center>
            </div>
          </div>

             
    <div class="panel panel-primary">
                <div class="panel-heading"><strong>MIS VENTAS VENTAS</strong>
                   
                </div>
    <div class="panel-body">

                <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>Crea</strong> Nuevos pedidos y lleva el control de las ventas de tus productos
              </div>
    <h2>TUS VENTAS</h2>
    @if ($message = Session::get('success'))

      <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
      </div>
    @endif
    <div class="table-responsive">
    <table class="table table-bordered" id="MyTable">
    <center>
      <button   type="button"  ng-click="addFact()" class="btn btn-success" data-toggle="modal" id="addNFact" name="addNFact" data-target="#addModal"><span class="fa fa-plus"></span>Agregar Nuevo Producto </button>



    </center>
      <thead>
        <tr>
          <th>Numero</th>
          <th>total</th>
          <th>fecha</th>
          <th>opciones</th>
         
        </tr>
      </thead>
      <tbody>
      @foreach($data as $x)
        <tr>
          <td>{{$x -> id}}</td>
          <td>{{$x -> total}}</td>
          <td>{{$x -> fecha_created}}</td>
         

          <td>
             
              <a href="{{ url('/detalle_factura_show') }}/{{$x -> id}}" class="btn btn-info"> <span class="fa fa-eye"></span>DETALLES</a>
              
          </td>
        </tr>
       @endforeach
      </tbody>
    </table>
  </div>
    <input type="hidden" name="hidden_view" id="hidden_view" value="{{url('laboratorios/view')}}">
    <input type="hidden" name="hidden_delete" id="hidden_delete" value="{{url('laboratorios/delete')}}">
    <!-- Add Modal start -->
    <div class="modal fade" id="addModal" role="dialog" data-backdrop="static" data-keyboard="false">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <div class="container">
              <div class="row">
                 <div class="col-sm-3">
                  <h4 class="modal-title">PEDIDO No. </h4>
                  </div>
               <div class="col-sm-3"> <input type="text"  class="form-control" id="id_factura" name="id_factura" disabled></h4></div>
              </div>
            </div>
          </div>
          <div class="modal-body">
            <div class="row">
               <div class="col-sm-2">Nombre</div>
               <div class="col-sm-4"><input type="text" name="nombre" class="form-control"></div>
               <div class="col-sm-1">Nit</div>
               <div class="col-sm-3"><input type="text" name="nit" class="form-control"></div></div>
               
              </div>
              <div class="row">
                 <div class="col-sm-2">Direccion</div>
               <div class="col-sm-8"><input type="text" name="direccion" class="form-control"> 
                <br/>
            </div>
            
              </div>
              <div class="row">
                <div class="col-sm-12">
              <center>
                 
              <strong>
                <span ng-click="addItem()"  class="btn btn-success btn-lg" ><span class="fa fa-plus"></span>Procesar</span>
              </strong>
            </center>
              <br/>
            </div>
              </div>
          </div>
          <div class="modal-footer">
           
          </div>
            
               </div>
            </div>

          </div>
            
             
               
              


               </div>
                </div>
              
               <h2>
               
            </h2>

              
        </div>
        
      </div>
    </div>
    <!-- add code ends -->
 
    <!-- View Modal start -->
    <div class="modal fade" id="viewModal" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Detalles</h4>
          </div>
          <div class="modal-body">
            <p><b>Nombre : </b><span id="view_nombre" class="text-success"></span></p>
            <p><b>Direccion : </b><span id="view_direccion" class="text-success"></span></p>
            <p><b>Telefono : </b><span id="view_telefono" class="text-success"></span></p>
            <p><b>Correo : </b><span id="view_correo" class="text-success"></span></p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"></button>
          </div>
        </div>
        
      </div>
    </div>
    <!-- view modal ends -->
 
    <!-- Edit Modal start -->
    <div class="modal fade" id="editModal" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Editar</h4>
          </div>
          <div class="modal-body">
            <form action="{{ url('cursos/update') }}" method="post">
              {{ csrf_field() }}
              <div class="form-group">
                <div class="form-group">
                  <label for="edit_nombre">Nombre:</label>
                  <input type="text" class="form-control" id="edit_nombre" name="edit_nombre">
                </div>
                <div class="form-group">
                  <label for="edit_descripcion">Direccion :</label>
                  <input type="text" class="form-control" id="edit_direccion" name="edit_descripcion">
                </div>
                <div class="form-group">
                  <label for="edit_telefono">telefono :</label>
                  <input type="text" class="form-control" id="edit_telefono" name="edit_telefono">
                </div>
                <div class="form-group">
                  <label for="edit_correo">Correo :</label>
                  <input type="text" class="form-control" id="edit_correo" name="edit_correo">
                </div>
               
              </div>
              
              <button type="submit" class="btn btn-default">Modificar</button>
              <input type="hidden" id="edit_id" name="edit_id">
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>
          
        </div>
        
      </div>
    </div>
    <!-- Edit code ends -->
 
  </div>
  </div>
  </div>

 <link href="{{ url('/') }}/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">

                 
                 <link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/1.10.15/css/jquery.dataTables.css">
                <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
                <script src="//code.jquery.com/jquery-2.1.3.min.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.js"></script>
                <script type="text/javascript">
                  var app = angular.module('factura', []).controller('addFactura', function ($scope, $http) {
                      // consultar todos los datos


                    $scope.addFact = function(){
                      var usuario = "{{ Auth::user()->email }}";
                      $http.post("{{ url('api/ventas') }}?usuario="+usuario).then(function (response) {
                          $scope.items = response.data;
                          $scope.results = [];

                          document.getElementById("id_factura").value=response.data.id;

        
                          //alert("agregado en log");

                      });
                    }

                     //agregar
                    $scope.addItem = function (x, y) {
                        $scope.errortext = "";
                        var id = document.getElementById("id_factura").value;
                        //alert(id);

                         if(id){
                           location.href ="{{ url('detalle_factura/') }}"+"/"+id;
                           }
                            else {
                            
                           }

                    }
                  });

                  

                </script>


                <script language="javascript" type="text/javascript">
                                    
                    $(document).ready(function () {
                      $('#MyTable').DataTable( {
                        
                            "language": {
                                "lengthMenu": "Cantidad por pagina",
                                "zeroRecords": "Nada que mostrar",
                                "info": "Mostrando pagina _PAGE_ de _PAGES_",
                                "infoEmpty": "No records available",
                                "infoFiltered": "(filtrados from _MAX_ total records)",
                                "sSearch": "Buscar:",
                                 "dom": '<"top"Blf>rt<"bottom"ip>',
                                "buttons": ['copy', 'excel', 'csv', 'pdf', 'print'],
                                "select": true,
                            }
                        } );
                  } );
                </script>

<script>
    var app = angular.module('myApp', []).controller('myCtrl', function ($scope, $http) {
        $http.get("{{ url('api/ventas') }}").then(function (response) {
            $scope.myData = response.data;
            $scope.results = [];

            console.log(response.data);


           
        });

         $scope.addItemB = function () {
                        var usuario = "{{ Auth::user()->email }}";
                        var tipo_proceso = "CREA FACTURA";
                        var codigo = "FACTURA"+document.getElementById("id_factura").value;
                        var descripcion = codigo;

                        //alert(id);
                        $http.post("{{ url('api/bitacora') }}?usuario="+usuario+"&tipo_proceso="+tipo_proceso+"&descripcion="+descripcion);

                        console.log("agregado en log");
                        alert("agregado en log");

                       

                    }
  });


       </script>

                 
  <script type="text/javascript">


    function fun_view(id)
    {
      var view_url = $("#hidden_view").val();
      $.ajax({
        url: view_url,
        type:"GET", 
        data: {"id":id}, 
        success: function(result){
          //console.log(result);
          $("#view_nombre").text(result.nombre);
          $("#view_direccion").text(result.direccion);
          $("#view_telefono").text(result.telefono);
          $("#view_correo").text(result.correo);
        }
      });
    }
 
    function fun_edit(id)
    {
      var view_url = $("#hidden_view").val();
      $.ajax({
        url: view_url,
        type:"GET", 
        data: {"id":id}, 
        success: function(result){
          //console.log(result);
          $("#edit_id").val(result.id);
          $("#edit_nombre").val(result.nombre);
          $("#edit_direccion").val(result.direccion);
          $("#edit_telefono").val(result.telefono);
          $("#edit_correo").val(result.correo);
        }
      });
    }
 
    function fun_delete(id)
    {
      var conf = confirm("Esta seguro que desea eliminar??");
      if(conf){
        var delete_url = $("#hidden_delete").val();
        $.ajax({
          url: delete_url,
          type:"POST", 
          data: {"id":id,_token: "{{ csrf_token() }}"}, 
          success: function(response){
            alert(response);
            location.reload(); 
          }
        });
      }
      else{
        return false;
      }
    }
  </script>


           


          </div>

          <!-- /.col-lg-12 -->

      </div>



  </div>

@endsection

