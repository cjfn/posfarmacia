<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\model\medicina\medicina;
use App\model\laboratorio\laboratorio;
use App\model\sucursal\sucursal;
use App\model\ventas\ventas;
use App\model\factura\factura;
use App\model\detalle_factura\detalle_factura;
use DB;

class apiDetalleController extends Controller
{
    //
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
         $data = detalle_factura::all()->toArray();

            return response()->json($data);
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        try{
            $data = new detalle_factura;
            $data -> factura = $request -> factura;
            $data -> codigo_producto = $request ->  codigo_producto;
            $data -> cantidad = $request -> cantidad;
            $data -> subtotal = $request -> subtotal;
            $data -> fecha_vencimiento = $request -> fecha_vencimiento;
            $data -> usuario = $request -> usuario ;
            $data -> idunico = $request -> idunico ;
             
            $data -> save(); 



            return response()->json(['status'=>true, 'Producto agregado a factura'],200);
        }
        catch(Exception $e)
        {

            return response()->json(['status'=>false, 'error'],400);
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
           

            $info=medicina::join('sucursales as s', 'medicinas.sucursal','=','s.id')
            ->select('medicinas.id','medicinas.codigo_producto','medicinas.aplica_descuento','medicinas.porcentaje_descuento','medicinas.nombre', 'medicinas.precio_venta','medicinas.existencia','medicinas.factura','medicinas.laboratorio_id','medicinas.registro','medicinas.fecha_vencimiento','medicinas.fecha_created','medicinas.fecha_updated','medicinas.sucursal', 'medicinas.activo', 'medicinas.generico','medicinas.observaciones','medicinas.usuario','s.nombre as sucursalname')
            ->where('existencia','>', 0)
            ->where('codigo_producto', 'like', $id)->get();

            
              return response()->json($info);
        }
        catch(Exception $e)
        {
            return response()->json(['status'=>false, 'error'],400);    
        }

                
            
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
