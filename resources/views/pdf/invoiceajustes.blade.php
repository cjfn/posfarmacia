
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Example 2</title>
    <style>
      table, td, th {    
    border: 1px solid black;
    text-align: left;
}

table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    padding: 15px;
}
    </style>





  </head>
  <body>

          <CENTER>
          <h2>INVENTARIO <STRONG> FARMACIA LA SALUD </STRONG></h2>
          <H5>FECHA: {{ $date }} - REPORTE DE AJUSTES DE PRODUCTOS </H5>


          MES {{ $mes }}

         

          AÑO {{ $year}}
  

          
      <table border="1">
        <thead>
          <tr>
            <th class="">Cod.</th>
            <th class="">Nombre</th>
            <th class="">Precio Q.</th>
            <th class="">Cant.</th>
            <th class="">factura</th>
            <th class="">Lab</th>
            <th class="">Sucursal</th>
            <th class="">Fecha Ingreso</th>
            <th class="">Fecha Modificacion</th>
            <th class="">Usuario/Observaciones</th>
            
            
      

          </tr>
        </thead>
        <tbody>
          
             @foreach($data as $key => $dat)
             <tr>
            <td class="">{{ $dat->codigo_producto }}</td>
            <td> {{ $dat->nombre }}</td>
            <td> {{ $dat->precio_venta }}</td>
            <td> {{ $dat->existencia }}</td>
            <td> {{ $dat->factura }}</td>
            <td> {{ $dat->lab }}</td>
            <td> {{ $dat->sucursalname }}</td>
             <td> {{ date("Y-m-d", strtotime($dat->fecha_created )) }}</td>
             <td> {{ date("Y-m-d", strtotime($dat->updated )) }}</td>
             <td><strong> {{ $dat->usuario }}: </strong>
              {{ $dat->observaciones }}</td>
            
            </tr>
            @endforeach

          

        </tbody>
        <CENTER>
          
        </CENTER>
       
      </table>
  </body>
</html>
