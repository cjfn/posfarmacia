<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\model\ventas\ventas;
use App\model\factura\factura;
use App\model\ventas\detalle_factura;
use App\model\medicina\medicina;
use Auth;

class misventasController extends Controller
{
    //

     public function index()
    {
         
         $data = medicina::all();
         $productos = factura::where('activo','=', '1')
         ->where('usuario','=', Auth::user()->email)
         ->orderBy('id','desc')->get();

            return view('misventas.index',['data'=>$productos, 'productos'=>$productos]);
    }

}
