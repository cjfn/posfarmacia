
@extends('layouts.app')



@section('content')

<meta name="csrf-token" content="{{ csrf_token() }}" />



<div class="container" ng-app="factura">
    <div class="row">
 
        <div class="col-md-12" ng-controller="addFactura">

             
    <div class="panel panel-primary">
                <div class="panel-heading"><strong>DETALLE DE FACTURA MOD</strong>
                </div>
    <div class="panel-body">
    <h2>AGREGA PRODUCTOS A TU ORDEN DE COMPRA(FACTURA)</h2>
    @if ($message = Session::get('success'))

      <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
      </div>
    @endif
    <div class="table-responsive">
    <table class="table table-bordered" id="MyTable">
    <center>
    

     <span ng-click="addFact()" class="btn btn-default" data-toggle="modal" data-target="#addModal" >Agregar</span>
    </center>
      <thead>
        <tr>
          <th>Numero</th>
          <th>total</th>
          <th>fecha</th>
          <th>opciones</th>
         
        </tr>
      </thead>
      <tbody>
      @foreach($data as $x)
        <tr>
          <td>{{$x -> id}}</td>
          <td>{{$x -> total}}</td>
          <td>{{$x -> fecha_created}}</td>
         

          <td>
              <button class="btn btn-info" data-toggle="modal" data-target="#viewModal" onclick="fun_view('{{$x -> id}}')" ><span class="fa fa-eye"></span>Ver</button>
              <button class="btn btn-warning" data-toggle="modal" data-target="#editModal" onclick="fun_edit('{{$x -> id}}')"><span class="fa fa-pencil-square"></span>Editar</button>
              <button class="btn btn-danger" onclick="fun_delete('{{$x -> id}}')"><span class="fa fa-times-circle"></span>Eliminar</button>
          </td>
        </tr>
       @endforeach
      </tbody>
    </table>
  </div>
    <input type="hidden" name="hidden_view" id="hidden_view" value="{{url('laboratorios/view')}}">
    <input type="hidden" name="hidden_delete" id="hidden_delete" value="{{url('laboratorios/delete')}}">
    <!-- Add Modal start -->
    <div class="modal fade" id="addModal" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
             <div class="col-sm-2">
              <h4 class="modal-title">PEDIDO No. </h4>
              </div>
              <div class="col-sm-2"> <input type="text"  class="form-control" id="id_factura" name="id_factura" disabled></h4></div>
               <div class="col-sm-8"> <strong><H5 id="demo"></H5></strong> </div>
                 </div>
                 <script>
                  document.getElementById("demo").innerHTML = Date();
                  </script></div>
               <div class="col-sm-8"></div>

              
              

          <div class="modal-body">
            <form action="{{ url('ventas') }}" method="post">
              {{ csrf_field() }}

            <div ng-app="myApp" ng-controller="myCtrl">

            <div class="container">
                <div class="row">
                  <div class="col-sm-1"><label for="last_name">Codigo:</label></div>                   
                  <div class="col-sm-1">
                  <input type="text" class="form-control" id="codigo_producto" name="codigo" required="true"> </div>
                  <div class="col-sm-1"><label for="last_name">Descripcion:</label></div>                   
                  <div class="col-sm-2">
                  <input type="text" class="form-control" id="descripcion" name="descripcion" required="true"> </div>
                  <div class="col-sm-1"><label for="last_name">Valor:</label></div>                   
                  <div class="col-sm-1">
                  <input type="text" class="form-control" id="valor" name="valor" required="true"> </div>
                  <div class="col-sm-4"></div>
                </div>
                <div class="row">
                  <div class="col-sm-1"><label for="last_name">Cantidad:</label></div>                   
                  <div class="col-sm-1">
                  <input type="text" class="form-control" id="cantidad" name="cantidad" required="true"> </div>
                  <div class="col-sm-1"><label for="last_name">Descuento:</label></div>                   
                  <div class="col-sm-2">
                  <input type="text" class="form-control" id="descuento" name="descuento" required="true"> </div>
                  <div class="col-sm-1"><label for="last_name">Subtotal:</label></div>                   
                  <div class="col-sm-1">
                  <input type="text" class="form-control" id="subtotal" name="subtotal" required="true"> </div>
                  <div class="col-sm-4"></div>
                </div>

                <div class="row">
                  <div class="col-sm-1"><label for="last_name">Fecha Vence:</label></div>                   
                  <div class="col-sm-1">
                  <input type="text" class="form-control" id="cantidad" name="cantidad" required="true"> </div>
                  <div class="col-sm-1"><label for="last_name">Laboratorio:</label></div>                   
                  <div class="col-sm-3">
                  <input type="text" class="form-control" id="descuento" name="descuento" required="true"> </div>
                   <button type="submit" class="btn btn-success"><span class="fa fa-plus"></span>Agregar</button>
                </div>
                  <!-- DETALLE DE FACTURA -->
                 <div class="table-responsive">
                  <table class="table table-bordered table-hover">
        <thead>
            
            <tr>
                <td>
                   <strong> CODIGO </strong>
                </td>
                <td>
                   <strong> NOMBRE USUARIO </strong>
                </td>

                 <td>
                   <strong> OPCIONES </strong>
                </td>
            </tr>
            
        </thead>
        <tbody>
            <tr ng-repeat="x in myData">
                <td>
                   
                </td>
                <td> 
                   
                </td>
                <td> <span ng-click="removeItem($index, x.NOMBRE_USUARIO)" class="btn btn-danger" >Eliminar</span></td>
            </tr>
        </tbody>
    </table>
                 </div>
                
              </div>

              <div class="form-group">
                <div class="form-group">
                  </div>

               
                   
                  </div>

                  

                </div>
                <div class="form-group">
                  
                </div>
               
              </div>
              
             
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-window-close"></span>Cerrar</button>
          </div>
        </div>
        
      </div>
    </div>
    <!-- add code ends -->
 
    <!-- View Modal start -->
    <div class="modal fade" id="viewModal" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Detalles</h4>
          </div>
          <div class="modal-body">
            <p><b>Nombre : </b><span id="view_nombre" class="text-success"></span></p>
            <p><b>Direccion : </b><span id="view_direccion" class="text-success"></span></p>
            <p><b>Telefono : </b><span id="view_telefono" class="text-success"></span></p>
            <p><b>Correo : </b><span id="view_correo" class="text-success"></span></p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"></button>
          </div>
        </div>
        
      </div>
    </div>
    <!-- view modal ends -->
 
    <!-- Edit Modal start -->
    <div class="modal fade" id="editModal" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Editar</h4>
          </div>
          <div class="modal-body">
            <form action="{{ url('cursos/update') }}" method="post">
              {{ csrf_field() }}
              <div class="form-group">
                <div class="form-group">
                  <label for="edit_nombre">Nombre:</label>
                  <input type="text" class="form-control" id="edit_nombre" name="edit_nombre">
                </div>
                <div class="form-group">
                  <label for="edit_descripcion">Direccion :</label>
                  <input type="text" class="form-control" id="edit_direccion" name="edit_descripcion">
                </div>
                <div class="form-group">
                  <label for="edit_telefono">telefono :</label>
                  <input type="text" class="form-control" id="edit_telefono" name="edit_telefono">
                </div>
                <div class="form-group">
                  <label for="edit_correo">Correo :</label>
                  <input type="text" class="form-control" id="edit_correo" name="edit_correo">
                </div>
               
              </div>
              
              <button type="submit" class="btn btn-default">Modificar</button>
              <input type="hidden" id="edit_id" name="edit_id">
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>
          
        </div>
        
      </div>
    </div>
    <!-- Edit code ends -->
 
  </div>
  </div>
  </div>

 <link href="{{ url('/') }}/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">

                 
                 <link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/1.10.15/css/jquery.dataTables.css">
                <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
                <script src="//code.jquery.com/jquery-2.1.3.min.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.js"></script>
                <script type="text/javascript">
                  var app = angular.module('factura', []).controller('addFactura', function ($scope, $http) {
                      // consultar todos los datos


                    $scope.addFact = function(){
                      
                      
        

                          var subtotal =document.getElementById("subtotal").value();

                          if(subtotal)
                          {
                            $http.post("{{ url('api/ventas') }}").then(function (response) {
                            $scope.items = response.data;
                            $scope.results = [];

                          }
                          else
                          {
                            alert("debe de agregar por lo menos un producto");
                          }

                     
                    }
                  });

                  

                </script>


                <script language="javascript" type="text/javascript">
                                    
                    $(document).ready(function () {
                      $('#MyTable').DataTable();
                  } );
                </script>

<script>
    var app = angular.module('myApp', []).controller('myCtrl', function ($scope, $http) {
        $http.get("{{ url('api/ventas') }}").then(function (response) {
            $scope.myData = response.data;
            $scope.results = [];

            console.log(response.data);
           
        });

       </script>

                 
  <script type="text/javascript">


    function fun_view(id)
    {
      var view_url = $("#hidden_view").val();
      $.ajax({
        url: view_url,
        type:"GET", 
        data: {"id":id}, 
        success: function(result){
          //console.log(result);
          $("#view_nombre").text(result.nombre);
          $("#view_direccion").text(result.direccion);
          $("#view_telefono").text(result.telefono);
          $("#view_correo").text(result.correo);
        }
      });
    }
 
    function fun_edit(id)
    {
      var view_url = $("#hidden_view").val();
      $.ajax({
        url: view_url,
        type:"GET", 
        data: {"id":id}, 
        success: function(result){
          //console.log(result);
          $("#edit_id").val(result.id);
          $("#edit_nombre").val(result.nombre);
          $("#edit_direccion").val(result.direccion);
          $("#edit_telefono").val(result.telefono);
          $("#edit_correo").val(result.correo);
        }
      });
    }
 
    function fun_delete(id)
    {
      var conf = confirm("Esta seguro que desea eliminar??");
      if(conf){
        var delete_url = $("#hidden_delete").val();
        $.ajax({
          url: delete_url,
          type:"POST", 
          data: {"id":id,_token: "{{ csrf_token() }}"}, 
          success: function(response){
            alert(response);
            location.reload(); 
          }
        });
      }
      else{
        return false;
      }
    }
  </script>


           


          </div>

          <!-- /.col-lg-12 -->

      </div>



  </div>

@endsection

