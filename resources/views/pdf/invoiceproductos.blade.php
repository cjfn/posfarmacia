
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Example 2</title>
    <style>
      table, td, th {    
    border: 1px solid black;
    text-align: left;
}

table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    padding: 15px;
}
    </style>





  </head>
  <body>

          <CENTER>
          <h2>INVENTARIO <STRONG> FARMACIA LA SALUD </STRONG></h2>
          <H5>FECHA: {{ $date }} - REPORTE DE INGREROS INVENTARIO DE PRODUCTOS </H5>


       

          
      <table border="1">
        <thead>
          <tr>
            <th class="">Cod.</th>
            <th class="">Nombre</th>
            <th class="">Cant.</th>
            <th class="">Precio Venta</th>
            <th class="">Precio Compra</th>
             <th class="">% Descuento</th>
            <th class="">Factura</th>
           
            <th class="">Laboratorio</th>
            <th class="">Sucursal</th>
            <th class="">Fecha Ingreso</th>
            <th class="">Vencimiento</th>
            <th class="">Creado/Modificado por</th>
           
          </tr>
        </thead>
        <tbody>
          
             @foreach($data as $key => $dat)
             <tr>
            <td class=""><strong>{{ $dat->codigo_producto }}</strong></td>
            <td> {{ $dat->nombre }}</td>
            <td> {{ $dat->existencia }}</td>
            <td> Q.{{ $dat->precio_venta }}</td>
            <td> Q.{{ $dat->precio_compra }}</td>
            <td> {{ $dat->porcentaje_descuento/100 }}</td>
            
            <td> {{ $dat->factura }}</td>
            <td> {{ $dat->laboratorio_id }}</td>
            <td> {{ $dat->sucursal }}</td>
          
             <td> {{ date("Y-m-d", strtotime($dat->fecha_created )) }}</td>
             <td> {{ $dat->fecha_vencimiento }}</td>
             <td> {{ $dat->usuario }}</td>
          
            </tr>
            @endforeach

          

        </tbody>
        <CENTER>
          
        </CENTER>
       
      </table>
  </body>
</html>
