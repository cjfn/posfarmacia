
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Example 2</title>
    <style>
      table, td, th {    
    border: 1px solid black;
    text-align: left;
}

table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    padding: 15px;
}
    </style>





  </head>
  <body>

          <CENTER>
          <h2>VENTAS <STRONG> FARMACIA LA SALUD </STRONG></h2>
          <H5>FECHA: {{ $date }} - CANTIDAD DE DETALLES VENTAS: {{$data_total}} </H5>
          <H3>TOTAL Q. {{$data_total_ventas}}

            @if (!$mes)
            @else
            {{ $mes }} 
            @endif

             @if (!$year)
            @else
            AÑO {{ $year }} 
            @endif
          
      <table border="1">
        <thead>
          <tr>
            <th class="">Cod. Producto</th>
            <th class="">Nombre</th>
            <th class="">Precio compra</th>
            <th class="">Precio venta</th>
            <th class="">Cantidad</th>
            <th class="">Subtotal</th>
            <th class="">Descuento Cliente</th>
            <th class="">Ganancia</th>
            <th class="">Pedido/Factura</th>
            <th class="">FechaVenta</th>
            <th class="">Vendido Por</th>
            
      

          </tr>
        </thead>
        <tbody>
{{ $total_ganacia=0}}

{{ $nganancia=0}}
          
             @foreach($data as $key => $dat)
             <tr>
             

        
            <td class="">{{ $dat->codigo_producto }}</td>
            <td> {{ $dat->nombre }}</td>
            <td> Q.{{ $dat->precio_compra }}</td>
            <td> Q.{{ $dat->precio_venta }}</td>
            <td> {{ $dat->cantidad }}</td>
            <td> Q.{{ $dat->subtotal }}</td>
            <td> {{(round(((($dat->precio_venta*$dat->cantidad)/$dat->subtotal)-1),2))*100}}%</td>
            {{$ganancia=($dat->subtotal)-($dat->precio_compra*$dat->cantidad)}}
            <td>Q.{{$ganancia}}</td>
            <td> {{ $dat->id }}</td>
             <td> {{ date("Y-m-d", strtotime($dat->fecha_created )) }}</td>
            <td> {{ $dat->usuario }}</td>
            </tr>
            {{$nganancia=$ganancia}}


             {{$total_ganacia=$total_ganacia+$nganancia}}
            @endforeach

          
            <tr>  
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td>TOTAL</td>
              <td>Q.{{$data_total_ventas}} </td>
              <td>GANANCIA</td>
              <td>Q.{{$total_ganacia}}</td>
              <td></td>
              <td></td>
              <td></td>


            </tr>
          

        </tbody>
        <CENTER>
          <H2><STRONG>TOTAL VENTAS Q. {{$data_total_ventas}}   TOTAL GANANCIA Q {{$total_ganacia}}</STRONG></H2>
        </CENTER>
       
      </table>
  </body>
</html>
