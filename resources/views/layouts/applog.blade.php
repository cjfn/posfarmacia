<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'miPos') }}</title>

      <!-- Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">
    <!-- Styles --><!-- Styles -->
    <link rel="stylesheet" href="{{ url('/')}}/css/bootstrap.min.css">
    <!-- Styles -->
    <link rel="stylesheet" href="{{ url('/')}}/css/font-awesome.min.css">
     <link href="{{ url('/') }}/css/bootstrap.css" rel="stylesheet" type="text/css">
    <!-- Custom Fonts -->
    <link href="{{ url('/') }}/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Mis Estilos CSS-->
    <link href="{{ url('/') }}/css/estilos.css" rel="stylesheet" type="text/css">
    <link rel="shortcut icon" type="image/x-icon" href="{{ url('/') }}/favicon.ico" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />   
    <script src="{{ url('/') }}/js/Chart.js"></script>
    <script src="{{ url('/') }}/js/angular.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-i18n/1.6.9/angular-locale_en-us.min.js"></script>
    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>

        function myFunction() {
            /*
           alert("Su sistema esta funcionando a un 100%, los problemas fueron corregidos, estamos trabajando arduamente por que todo este bien, no olvides hacer el pago con tu proveedor, de lo contrario el servicio sera suspendido, Bienvenido!!!");
           */
        }

    </script>
</head>
<body onload="myFunction()">
    <div id="app">
         <br/>
         <br/>
         <br/>
         <br/>
         <br/>
         <br/>
         <br/>
         <br/>
         <br/>
         
<div class="main">
     @yield('content')
</div>
       
    </div>

    <!-- Scripts -->

    

    <script type="text/javascript" language="javascript" src="{{ url('/') }}/Datatables/media/js/jquery.dataTables.js">
 <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js"></script>
  


    <script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.12.4.js">
    </script>
 

    <script type="text/javascript" language="javascript" class="init"></script>

   
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}


</body>
</html>
