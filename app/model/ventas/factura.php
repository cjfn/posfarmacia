<?php

namespace App\\model\ventas;

use Illuminate\Database\Eloquent\Model;

class factura extends Model
{
    //

     protected $table = 'facturas';
	 protected $primarykey='id';
	 public $timestamps = false;
    
    protected $fillable = [
        'total', 'fecha_created', 'usuario', 'fecha_updated'
    ];
}
