
@extends('layouts.app')



@section('content')

<meta name="csrf-token" content="{{ csrf_token() }}" />



<div class="container">
  <center>

 <span  tabindex="4" onclick="printDiv('areaImprimir')" onkeypress="printDiv('areaImprimir')" value="imprimir div" class="btn btn-danger btn-lg" ><span class="fa fa-print"></span>IMPRIMIR</span>
</center>
<div id="areaImprimir" >


          <CENTER>
          <H5>FECHA: {{ $date }} - REPORTE DE INGREROS INVENTARIO DE PRODUCTOS </H5>
          <H4>DEL {{substr($from, 0, 10)}} AL {{substr($to, 0, 10)}}</H4>

          
      <table class="table table-bordered">
        <thead>
          <tr>
            <th class="">Cod.</th>
            <th class="">Nombre</th>
            <th class="">Cant.</th>
            <th class="">Precio Venta</th>
            <th class="">Precio Compra</th>
             <th class="">% Descuento</th>
            <th class="">Factura</th>
           
            <th class="">Laboratorio</th>
            <th class="">Sucursal</th>
            <th class="">Fecha Ingreso</th>
            <th class="">Vencimiento</th>
            <th class="">Creado/Modificado por</th>
           
          </tr>
        </thead>
        <tbody>
          
             @foreach($data as $key => $dat)
             <tr>
            <td class=""><strong>{{ $dat->codigo_producto }}</strong></td>
            <td> {{ $dat->nombre }}</td>
            <td> {{ $dat->existencia }}</td>
            <td> Q.{{ $dat->precio_venta }}</td>
            <td> Q.{{ $dat->precio_compra }}</td>
            <td> {{ $dat->porcentaje_descuento/100 }}</td>
            
            <td> {{ $dat->factura }}</td>
            <td> {{ $dat->lab }}</td>
            <td> {{ $dat->sucursalname }}</td>
             <td> {{ date("Y-m-d", strtotime($dat->fecha_created )) }}</td>
             <td> {{ $dat->fecha_vencimiento }}</td>
             <td> {{ $dat->usuario }}</td>
          
            </tr>
            @endforeach

          

        </tbody>
        <CENTER>
          
        </CENTER>
       
      </table>
      </div>
  

  </div>

  <script type="text/javascript">
    

      function printDiv(nombreDiv) {
     var contenido= document.getElementById(nombreDiv).innerHTML;
     var contenidoOriginal= document.body.innerHTML;

     document.body.innerHTML = contenido;

     window.print();

     document.body.innerHTML = contenidoOriginal;
}
  </script>

@endsection

