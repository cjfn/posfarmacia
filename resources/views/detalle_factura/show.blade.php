
@extends('layouts.app')



@section('content')
<style type="text/css" media="print">
    @page 
    {
        size: auto;   /* auto is the initial value */
        margin: 0mm;  /* this affects the margin in the printer settings */
    }

    @media print {
    .footer,
    #non-printable {
        display: none !important;
    }
    #printable {
        display: block;
    }
}
</style>

<div class="container">
    <div class="row">
    <div class="col-md-3">
     <!-- Sidebar -->


    </div>
    <div class="col-md-12" >
         <div class="panel panel-default">
             
            <div class="panel panel-principal">
                <center>
                  <strong>
                 
                   <a href="" class="btn btn-success" data-toggle="modal" data-target="#ModalSearch"><span class="fa fa-plus"></span>Busca Productos</button> </a>
            
                </strong>
                <strong>
                 <a href="{{ url('/medicinas') }}" onclick="search()" class="btn btn-default"> <span class="fa fa-search"></span>BUSCA LABORATORIOS</a>
                </strong>
                <strong>
                 <a href="{{ url('/sucursales') }}" class="btn btn-default"> <span class="fa fa-search"></span>BUSCA SUCURSALES</a>
                </strong>
                 
                 <strong>
                 <a href="#" class="btn btn-primary" title="PARA AJUSTES DE INVENTARIO ACUDA AL GERENTE O JEFE DE AGENCIA">  <span class="fa fa-cogs"></span>AJUSTES</a>
                </strong>
                <strong>
                 <a href="#" class="btn btn-success" <span class="fa fa-money"></span>FACTURA ELECTRONICA</a>
                </strong>
                <strong>
                 <a href="#" class="btn btn-success" > <span class="fa fa-cube"></span>CAJA CHICA</a>
                </strong>
             </center>
            </div>
        </div>


       

    <div class="panel panel-primary">
      <div class="panel-heading"> <h3 class="text-center ">DETALLES DE PEDIDO O FACTURA {{$sucursal["nombre"]}} <span class="btn btn-default btn-lg"><strong>NO.{{ $id }} </strong></span> <span class="btn btn-success btn-lg"><strong> TOTAL Q.{{ $factura["total"]}} </strong></span>

                  <span class="fa fa-refresh" title="Problemas click en Recargar"></span>
                  <input title="si no aparece el producto haz click aqui!" type="button" class="btn btn-warning" value="Recargar" id="referesh" name="referesh" onclick="window.location.reload()" style="font-family: Arial; font-size: 10 pt">
                   </h3>

                  
      </div>
      <div class="panel-body">

     


      <div ng-app="myApp" ng-controller="myCtrl">
        <div class="col-md-12">
        <div class="panel panel-principal">
        <form action="{{ url('factura/update') }}" method="post">
              {{ csrf_field() }}
              <input type="hidden" name="factura" id="factura" value="{{ $id }}">
             <input type="hidden" class="form-control" id="idunico" readonly="true" readonly="readonly" name="IdUnico" required="true" ng-model="idunico"> </div>
          
            <div class="container">
                <div class="row">
                  <div class="col-sm-1"><label for="last_name">Codigo Producto:</label></div>                   
                  <div class="col-sm-3">
                 <div class="input-group">
                  <input type="text" class="form-control" id="codigo_producto" title="Ingrese codigo de producto, si no aparece verifique existenciaen productos" name="codigo_producto"  ng-keypress=""  ng-model="codigo" required="true" tabindex="1" > 
                  <span class="input-group-btn">
                    <span class="btn btn-success" ng-model="data" tabindex="2" title="presione ENTER para consultar" ng-keypress="buscar(codigo)" ng-click="buscar(codigo)"><span class="fa fa-search"></span>BUSCA </span>
                  </span>
                </div>
                  
                </div>
                  <div class="col-sm-1"><label for="last_name">Descripcion:</label></div>                   
                  <div class="col-sm-3">
                  <input type="text" class="form-control" ng-model="nombre" readonly="readonly" id="descripcion" name="descripcion" required="true"> </div>
                  <div class="col-sm-1"><label for="last_name">Valor:</label></div>                   
                  <div class="col-sm-2">
                  <input type="text" class="form-control" id="valor" readonly="true" readonly="readonly" name="valor" required="true" ng-model="valor"> </div>

                
                </div>
                <div class="row">
                  <div class="col-sm-1"><label for="last_name">Cantidad:</label></div>                   
                  <div class="col-sm-2">
                  <input type="number" class="form-control" title="Necesario Ingrese cantidad de productos a venta" id="cantidad" tabindex="3" name="cantidad" ng-change="calculo(valor, cantidad, descuento)" value="0"  ng-model="cantidad" required="true"> </div>
                  <div class="col-sm-1"><label for="last_name">Descuento:</label></div>                   
                  <div class="col-sm-1">
                  <input type="text" class="form-control" id="descuento" name="descuento" ng-change="calculon(valor, cantidad, descuento)" tabindex="4"  ng-model="descuento" placeholder="0" required="true" value="0"> </div>
                  <div class="col-sm-1"><label for="last_name">Subtotal:</label></div>                   
                  <div class="col-sm-2">
                  <input type="text" class="form-control" id="subtotal" name="subtotal" readonly="readonly" required="true"> </div>
                  <div class="col-sm-1">Existencias
                  </div>
              
                  <div class="col-sm-1">
                  <input type="number" class="form-control" id="existencia" name="existencia" readonly="readonly" required="true"> </div>
              
                </div>
                <br/>

                <div class="row">
                  <div class="col-sm-12"><label for="last_name">Seleccionar por fecha vencimiento:</label>
                  </div>
                  <div class="row">                  
                  <div class="col-sm-9">
                 
                  <select id="fecha_vence" tabindex="5" onchange="nuevoitem()" class="form-control" name="fecha_vence"  title="seleccione fecha de vencimiento" required="true">
                    <option value="">Seleccione un producto</option>
                    
                  </select>
                  </div>

                  <span  ng-click="addItem()" ng-keypress="addItem()"   class="btn btn-success" tabindex="6"><span class="fa fa-plus"></span>Agregar</span>
                  
                  <span class="fa fa-refresh" title="Problemas click en Recargar"></span>
                  <input title="si no aparece el producto haz click aqui!" type="button" class="btn btn-warning" value="Recargar" id="referesh" name="referesh" onclick="window.location.reload()" style="font-family: Arial; font-size: 10 pt">
                </div>
              <div class="row">
              <div id="id_div_contenedor" class="col-sm-8">
              </div>
               <div class="row">
              <div id="id_div_codigos_unicos" class="col-sm-8">
              </div>
              </div>
                 
                 
                  <form>
                 

              </div>
            </form>
            



          
           
        </div>
      </div>
      <div id="areaImprimir" style="display: none;">
<pre>
<input type="hidden" id="id" name="id" value={{ $total = 0 }}>        
 FARMACIA LA SALUD 
 {{$sucursal["nombre"]}} 
 SOLICITUD DE MEDICINA PEDIDO NO. {{ $id }}
 <script type="text/javascript">
 var d = new Date();
	var month = new Array();
	month[0]="1";
	month[1]="2";
	month[2]="3";
	month[3]="4";
	month[4]="5";
	month[5]="6";
	month[6]="7";
	month[7]="8";
	month[8]="9";
	month[9]="10";
	month[10]="11";
	month[11]="12";
var mes = month[d.getMonth()];



 document.write(d.getDate()+"/"+mes+"/"+d.getFullYear()+"  "+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds());
 </script>
----------------------------------------
<input type="hidden" id="id" name="id" value={{ $contador = 0 }}>
  TOTAL PRODUCTOS {{$count}}               
  @foreach($data as $x)
<input type="hidden" value="{{$contador =  $contador + 1}}">
  Codigo:{{$x -> codigo_producto}}
  Nombre:{{$x -> nombre}}
  Cantidad: {{$x -> cantidad}}
  Precio ud: {{$x -> precio_venta}}
  Subtotal: Q.{{$x -> subtotal}}
  Cod1: 	{{(round(((($x->cantidad*$x->precio_venta)/$x -> subtotal)-1),2))*100}}
  Cod2: 	{{ $x->porcentaje_descuento}} 
  @endforeach

  @if($contador=$count)
     TOTAL Q.{{ $factura["total"]}} 
  @endif      
</pre>            
            
      </div>
       <input type="hidden" id="id" name="id" value={{$id}}>
        <input type="hidden"  id="sucursal" name="sucursal" value="{{$sucursal["nombre"]}}">

      <div  class="">
        <h5>SOLICITUD DE MEDICINA PEDIDO NO. {{ $id }}
       <div class="table-responsive">
            <table class="table table-bordered table-hover" >
              <thead>
                <tr>
                  <th>Cod</th>
                  <th>Producto</th>
                  <th>Cantidad</th>
                  <th>Precio unidad</th>
                   <th>Descuento</th>
                  <th>Subtotal</th>
                 
                  
                </tr>
              </thead>
              <tbody>
              @foreach($data as $x)
                <tr>
                  <td>{{$x -> codigo_producto}}</td>
                  <td>{{$x -> nombre}}</td>
                  <td>{{$x -> cantidad}}</td>
                  <td>{{$x -> precio_venta}}</td>

                   <td>
                    {{(round(((($x->cantidad*$x->precio_venta)/$x -> subtotal)-1),2))*100}}%</td>
                  <td>{{$x -> subtotal}}</td>

                 
                </tr>
               @endforeach
              </tbody>
            </table>

            TOTAL Q.{{ $factura["total"]}}
          </div>
          </div>
            <center>

                 </strong></span> <span class="btn btn-success btn-lg"><strong> <span class="fa fa-money"></span> TOTAL Q.{{ $factura["total"]}} </strong></span>
                 <input type="hidden" value="{{$factura['total']}}" name="total" id="total">
                 @if($factura["total"]==0)

                 @else
                 <strong>
                  <span ng-click="addFact()" ng-keypress="addFact()" tabindex="7" onclick="printDiv('areaImprimir')" onkeypress="printDiv('areaImprimir')" value="imprimir div" class="btn btn-danger btn-lg" ><span class="fa fa-plus"></span>CIERRRE DE VENTA</span>

                
                </strong>

             
                @endif
            </center>
            
            <!-- View Modal start -->
    <div class="modal fade" id="ModalSearch" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"><span class="fa fa-search"></span>Busca Productos</h4>
          </div>
          <div class="modal-body">
            <div class="table-responsive">
            <table class="table table-bordered" id="MyTable">
            <center>
             
            
            </center>
              <thead>
                <tr>
                  <th>Cod </th>
                  <th>Nombre</th>
                  <th>Precio Compra</th>
                  <th>Precio Venta</th>
                  <th>existencias</th>
                  <th>vencimiento </th>
                  <th>generico </th>
                  <th>Sucursal </th>
                  <th>Laboratorio </th>
                  <th>opciones</th>
                </tr>
              </thead>
              <tbody>
              @foreach($data2 as $x)
                <tr>
                  <td><strong>{{$x -> codigo_producto}}</strong></td>
                  <td>{{$x -> nombre}}</td>
                  <td>{{$x -> precio_compra}}</td>
                  <td>{{$x -> precio_venta}}</td>
                  <td>{{$x -> existencia}}</td>
                  <td>{{$x -> fecha_vencimiento}}</td>
                  <td>{{$x -> generico}}</td>
                  <td>{{$x -> sucursal}}</td>
                  <td>{{$x -> lab}}</td>

                  <td>
                      <button class="btn btn-info" data-toggle="modal" data-target="#viewModal" onclick="fun_view('{{$x -> id}}')" title="ver"><span class="fa fa-eye"></span><strong>Ver Detalles</strong></button>
                     
                      
                  </td>
                </tr>
               @endforeach
              </tbody>
            </table>
          </div>
            
            
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"></button>
          </div>
        </div>
        
      </div>
    </div>
    <!-- view modal ends -->

    <!-- Add Modal start -->
    <div class="modal fade" id="addModal" role="dialog" data-backdrop="static" data-keyboard="false">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
           <div class="container">
              <div class="row">
                 <div class="col-sm-3">
                  <h4 class="modal-title">PEDIDO No. </h4>
                  </div>
               <div class="col-sm-3"> <input type="text"  class="form-control" id="id_factura" name="id_factura" readonly="readonly"></h4></div>
               <div class="col-sm-1">
                  <h4 class="modal-title">FECHA </h4>
                  </div>
               <div class="col-sm-2"> <input type="text"  class="form-control" id="date" name="date" readonly="readonly"></h4></div>
              </div>
            </div>
          </div>
          <div class="modal-body">
            <div class="form-group">
            
              Nit<input type="text"  tabindex="1" name="nit" id="nit" placeholder="agrege nit de cliente" class="form-control">
            </div>
            <div class="form-group">
               Nombre<input type="text" name="nombre" id="nombre" placeholder="ingrese nombre de comprador" class="form-control">
             </div>
            <div class="form-group">
              Direccion<input type="text" name="direccion" placeholder="ingrese direccion" class="form-control"> 
              
            </div>
                 
              </div>
              <div class="row">
                <div class="col-sm-12">
              <center>
                 
              <strong>
                <span onclick="newventa()" onkeypress="newventa()" tabindex="2" ng-keydown="addItemf()" id="addfac" title="enter para agregar productos" name="addfac" class="btn btn-success btn-lg" ><span class="fa fa-plus"></span>Procesar</span>
              </strong>
            </center>
              <br/>
            </div>
              </div>
          </div>
          <div class="modal-footer">
           
          </div>
            
               </div>
            </div>

          </div>
            
             
               
              


               </div>
                 

                </div>
              
               <h2>
               
            </h2>

              
        </div>
        
      </div>
    </div>

        
    </div>
<input type="hidden" id="search" value="{{ url('/live_search/actionSearchIdUnico/') }}">


 <link href="{{ url('/') }}/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">

                 
                 <link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/1.10.15/css/jquery.dataTables.css">
                <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
                <script src="//code.jquery.com/jquery-2.1.3.min.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.js"></script>

     


     <script>
      
        function toprint()
                  {
                    //alert("me llamas");
                    var id= document.getElementById('id').value;
                    var sucursal= document.getElementById('sucursal').value;
                    var url="{{ url('/invoicethisventa?id=') }}"+ id + "&sucursal="+sucursal;
                    //location.href="{{ url('/invoicethisventa?id=') }}"+ id + "&sucursal="+sucursal;
                    window.open(url, "factura", "width=800, height=600");
                  }
                
      // funcion que imprime nombre

      function printDiv(nombreDiv) 
      {
       var contenido= document.getElementById(nombreDiv).innerHTML;
       var contenidoOriginal= document.body.innerHTML;
       document.body.innerHTML = contenido;
       window.print();
       document.body.innerHTML = contenidoOriginal;
      }


      function searchData() {
        // busqueda de data
        var urlVar = $("#search").val();
        var union = urlVar+"/"+nSearch;
        $.ajax({
          url: union,
          method: 'get',
          dataType: 'json',
          success: function (response) {
            console.log(response);
           
            },

           error: function(response){
            }
          });
      }




        function loaded() {
            alert("Image is loaded");
        }

        function nuevoitem()
        {
          var cantidad = parseInt(document.getElementById("cantidad").value);


          if(!cantidad)
                  {
                    alert("cantidad no puede ser vacia")
                  }
                  else
                  {
                     var fecha_vence = document.getElementById("fecha_vence").value;

                      alert(fecha_vence);
                      searchData(fecha_vence);
/*
                      var cantidad_exist_sel = document.getElementById(fecha_vence).value;
                      var id_unicos = document.getElementById(fecha_vence+'1').value;
                      alert("Precio:"+cantidad_exist_sel);
                      alert("IdUnico:"+id_unicos);
                      document.getElementById("idunico").value = id_unicos;
                      //alert(cantidad);
                      //document.getElementById("existencia").style.visibility="visible";
                      document.getElementById("valor").style.visibility="visible";
                      var valor = parseInt(document.getElementById("valor").value=cantidad_exist_sel);
                      var cantidad = document.getElementById("cantidad").value;
                      var descuento = document.getElementById("descuento").value;
                      var resultado = (cantidad*valor)-((cantidad*valor)*(descuento/100));
                      document.getElementById("subtotal").value = resultado;
                      */
                  }
          
         
         
        }


          function searchData(idunico) {
        //alert("buscar datos");
        // busqueda de data
        var urlVar = $("#search").val();
        var union = urlVar+"/"+idunico;
        //alert(union);
        $.ajax({
          url: union,
          method: 'get',
          dataType: 'json',
          success: function (response) {
            console.log(response);
            var largo = response.length;
            console.log(response[0].idunico);
            $("#descripcion").val(response[0].nombre)
            $("#valor").val(response[0].precio_venta);
            $("#existencia").val(response[0].existencia);
            $("#idunico").val(response[0].idunico);
            var descuento = $("#descuento").val();
            var cantidad =  $("#cantidad").val();
            var subtotal = cantidad*response[0].precio_venta;
            $("#subtotal").val(subtotal);
            // cargo inputs

            //alert("llego");

            },

           error: function(response){
            }
          });
      }


        function newventa()
        {
          var pedido = document.getElementById("id_factura").value;
          location.href="{{ url('/detalle_factura') }}/"+ pedido;
        }

        

      //ANGULAR  JS CODE

      var app = angular.module('myApp', []).controller('myCtrl', function ($scope, $http) {
                    $scope.addFact = function(){
                        var usuario = "{{ Auth::user()->email }}";
                                $http.post("{{ url('api/ventas') }}?usuario="+usuario).then(function (response) {
                                    $scope.items = response.data;
                                    $scope.results = [];
                                    document.getElementById("id_factura").value=response.data.id;
                                    Date.prototype.toString = function() { return this.getDate()+"/"+(this.getMonth()+1)+"/"+this.getFullYear(); } 

                                  var miFecha = new Date(); 
                                  //alert( miFecha );   // o lo que es lo mismo: alert( miFecha.toString() );  
                                  document.getElementById("date").value = miFecha;
                                  document.getElementById("nit").focus();
                                    //alert("agregado en log");
                                });

                                $('#addModal').modal('show');
                                var contenidoOriginal= document.body.innerHTML;
                                document.body.innerHTML = contenido;
                                window.print();
                                document.body.innerHTML = contenidoOriginal;
                                var id = {{ $id }};

                    }

           $scope.addItemf = function () {
                      alert('hola');

                    }

        $scope.addItem = function () {
            $scope.errortext = "";
                var factura = document.getElementById("factura").value;
                var codigo_producto = document.getElementById("codigo_producto").value;
                var cantidad = parseInt(document.getElementById("cantidad").value);
                var subtotal = document.getElementById("subtotal").value;
                var existencia = parseInt(document.getElementById("existencia").value);
                var usuario = "{{ Auth::user()->email }}";
                var idunico = document.getElementById("idunico").value;

                //alert(factura+ " "+ codigo_producto+" "+ cantidad+" "+subtotal+" "+existencia+" "+usuario+" "+idunico);

                //alert("factura"+factura + "codigo" + codigo_producto + "cantidad" + cantidad +  "subtotal" +subtotal + "idunico" +idunico);
               
                
                  if(!cantidad)
                  {
                    alert("cantidad no puede ser vacia")
                  }
                  else
                  {
                      $http.post("{{ url('api/detalle') }}?factura="+factura+"&codigo_producto="+codigo_producto+"&cantidad="+cantidad+"&subtotal="+subtotal+"&usuario="+usuario+"&idunico="+idunico);
                 

                               
                                 //alert("Click en el boton regargar para actualizar la pagina");
                                  console.log("agregado en log");
                                  //alert("agregado en log");
                                 $('#referesh').focus();

                                  alert("producto agregado exitosos");
                                 location.reload(true);
                 
                  }


                    
                


            
                       

                
                
                
            }

             $scope.calculo = function (i, j, k) {
              //alert("calculo");

                var valor = document.getElementById("valor").value;
                var cantidad = document.getElementById("cantidad").value;
                var descuento = document.getElementById("descuento").defaulValue=0;

                var n1 = parseFloat(valor).toFixed(2);//venta
                var n2 = parseFloat(descuento).toFixed(2);//descuento



                if(!n2)
                {
                  
                   var descuento = document.getElementById("descuento").defaulValue=0;
                  n2=document.getElementById('porcentaje_descuento').value;

                  //alert(n2);
                }
                

                if (j > 100) {
                    document.getElementById("porcentaje_descuento").value = "";
                    document.getElementById("precio_compra").value = "";
                    alert("Porcentaje de descuento debe de ser menor al 100%");
                }
                else {

                    

                    //var pdes = (n1 * n2) / 100;
                    //var pcompr = n1 - pdes;

                    var pdes = (n2/100);
                    var pcompr = (n1/((pdes)+1)*cantidad);


                    var pdesc = parseFloat(pdes).toFixed(2);
                    var pcompra = parseFloat(pcompr).toFixed(2);



                    document.getElementById("subtotal").value = pcompra;
                }



/*
                var valor = document.getElementById("valor").value;
                var cantidad = document.getElementById("cantidad").value;
                var descuento = document.getElementById("descuento").value;
               
                var resultado = (cantidad*valor)-((cantidad*valor)*(descuento/100));
                */

              //lert(resultado);
            }

                    
                


  $scope.calculon = function (i, j, k) {

                //alert("si");

                var valor = document.getElementById("valor").value;
                var cantidad = document.getElementById("cantidad").value;
                var descuento = document.getElementById("descuento").value;

                var n1 = parseFloat(valor).toFixed(2);//venta
                var n2 = parseFloat(descuento).toFixed(2);//descuento



                if(!n2)
                {
                  
                   var descuento = document.getElementById("descuento").defaulValue=0;
                  n2=document.getElementById('porcentaje_descuento').value;
                  //alert(n2);
                }
                

                if (j > 100) {
                    document.getElementById("porcentaje_descuento").value = "";
                    document.getElementById("precio_compra").value = "";
                    alert("Porcentaje de descuento debe de ser menor al 100%");
                }
                else {

                    

                    //var pdes = (n1 * n2) / 100;
                    //var pcompr = n1 - pdes;

                    var pdes = (n2/100);
                    var pcompr = (n1/((pdes)+1)*cantidad);


                    var pdesc = parseFloat(pdes).toFixed(2);
                    var pcompra = parseFloat(pcompr).toFixed(2);



                    document.getElementById("subtotal").value = pcompra;
                }


                   
                

            }

             // buscar detalles de producto por id
             $scope.buscar = function (i) {
              //alert("si busco");

             
              document.getElementById("fecha_vence").options.length = 0;
              //

                $scope.errortext = "";
                var cod = (i);

                //servicio que busca todo de producto por id
                $http.get("{{ url('api/productos/') }}/" + cod).then(function (response) {
                    $scope.items = response.data;

                    $scope.results = [];



                    //datos en consola
                    console.log(response.data);
                    if(response.data==0)
                    {
                      alert("Producto agotado o inexistente");
                    }
                    var largo = response.data.length;
                    var sucursal = parseInt("{{ Auth::user()->sucursal }}");
                    var i = 0;
                    
                    if(response.data[0]["id_suc"]!=sucursal)
                    {
                      $("#fecha_vence").append('<option value="">Seleccione un producto</option>');

                         // alert("otra sucursal");
                    }
                    if(largo>1)// si hay mas de un resultado
                    {
                      alert("ingrese cantidad y seleccione un producto");
                      document.getElementById("cantidad").focus(); 
                     for(prop in response.data)
                        {
                         var idsucu=response.data[prop]["id_suc"];
                          //alert(idsucu);                        
                             if(sucursal===idsucu)
                              {
                                     //alert("solo uno");
                                     if(prop==0)
                                     {
                                      $("#fecha_vence").append('<option value="">Seleccione un producto</option>');
                                     }
                                     //alert(response.data[1]["id_suc"]+"  mio:"+sucursal);
                                    $("#fecha_vence").append('<option value='+response.data[prop]["idunico"]+'>'+response.data[prop]["fecha_vencimiento"]+'  - Producto:'+response.data[prop]["nombre"]+' - Exist.:'+response.data[prop]["existencia"]+' - precio:'+response.data[prop]["precio_venta"]+' - Sucursal:'+response.data[prop]["sucursal"]+'</option>');
                                      var idunico= response.data[prop]["idunico"];
                                      //alert(idunico);
                                      var name = response.data[i]["nombre"];
                                      var precio1 = response.data[i]["precio_venta"];
                                      var existencia = response.data[i]["existencia"];
                                        //alert(sucursal);
                                       if (!$.trim(response.data[i])){   
                                              alert("Existen productos, agrege cantidad y descuento");
                                          }
                                          else{   
                                             // alert("What follows is not blank: " + response.data[i]);
                                          }
                               i=i+1;
                               console.log(i);
                              }
                        }
                    }
                    else
                    {


                     for(prop in response.data)
                        {
                         var idsucu=response.data[prop]["id_suc"];
                          //alert(idsucu);                        
                             if(sucursal===idsucu)
                              {
                                     //alert("solo uno");
                                     if(prop==0)
                                     {
                                      $("#fecha_vence").append('<option value="">Seleccione un producto</option>');
                                     }
                                    $("#fecha_vence").append('<option value='+response.data[prop]["idunico"]+'>'+response.data[prop]["fecha_vencimiento"]+'  - Producto:'+response.data[prop]["nombre"]+' - Exist.:'+response.data[prop]["existencia"]+' - precio:'+response.data[prop]["precio_venta"]+' - Sucursal:'+response.data[prop]["sucursal"]+'</option>');
                                      var idunico= response.data[prop]["idunico"];
                                      //alert(idunico);
                                      var name = response.data[i]["nombre"];
                                      var precio1 = response.data[i]["precio_venta"];
                                      var existencia = response.data[i]["existencia"];
                                      document.getElementById("descripcion").value=name;
                                      document.getElementById("valor").value=precio1;
                                      document.getElementById("existencia").value=existencia;
                                      document.getElementById("idunico").value=idunico;
                                      document.getElementById("cantidad").focus();
                                        //alert(sucursal);
                                       if (!$.trim(response.data[i])){   
                                              alert("Existen productos, agrege cantidad y descuento");
                                          }
                                          else{   
                                             // alert("What follows is not blank: " + response.data[i]);
                                          }
                               i=i+1;
                               console.log(i);
                              }
                        }
                    }
                   
                    
                  });
            }
});

        
</script>

    </div>
    </div>
</div>
@endsection
