<?php

namespace App\model\laboratorio;

use Illuminate\Database\Eloquent\Model;

class laboratorio extends Model
{
     protected $table = 'laboratorios';
	 protected $primarykey='id';
	 public $timestamps = false;
    
    protected $fillable = [
        'nombre', 'direccion', 'telefono', 'correo'
    ];
}
