<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\model\bitacora\bitacora;
use App\model\medicina\medicina;
use App\User;
use 

class bitacoraController extends Controller
{
    //
     public function existproduct($id, $fecha)
    {
        try{
      
            $info=medicina::where('codigo_producto', 'like', $id)
            ->where('fecha_vencimiento','=', $fecha)
            ->where('existencia','>', 0)
            ->get();

            



            return response()->json($info);

           
             
        }
        catch(Exception $e)
        {
            return response()->json(['status'=>false, 'error'],400);    
        }
    }

      public function store(Request $request)
    {
        //

        try{
        	$user = User::where('email', 'like', $request -> usuario )->count();

        	if($user != 1)
        	{
        		 $data = new bitacora;
	            $data -> usuario = $request -> usuario;
	            $data -> tipo_proceso = $request -> tipo_proceso;
	            $data -> descripcion = 'ALERTA';

	            //$data -> usuario = $request -> usuario;
	            $data -> save(); 
	            $id = $data->id;
	           
	           return response()->json(['status'=>true, 'bitacora creada USUARIO DESCONOCIDO', 'id' => $id],200);
        	}
        	else
        	{
        		
	            $data = new bitacora;
	            $data -> usuario = $request -> usuario;
	            $data -> tipo_proceso = $request -> tipo_proceso;
	            $data -> descripcion = $request -> descripcion;

	            //$data -> usuario = $request -> usuario;
	            $data -> save(); 

	            $id = $data->id;
	            return response()->json(['status'=>true, 'bitacora creada', 'id' => $id],200);

        	}

           
        }
        catch(Exception $e)
        {

            return response()->json(['status'=>false, 'error'],400);
        }
    }

}
