<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\model\medicina\medicina;
use App\model\laboratorio\laboratorio;
use App\model\sucursal\sucursal;
use App\model\facturas_ingreso\facturas_ingreso;
use DB;
use Auth;


class LiveSearchMedicinaController extends Controller
{
    //
    function action(Request $request)
    {
      $output = '';
      $id = $request->id;
     
       $info = medicina::join('sucursales as s', 'medicinas.sucursal','=','s.id')
        ->join('laboratorios as l','medicinas.laboratorio_id','=','l.id')
        
        ->select(DB::raw('DISTINCT(medicinas.id)'), 'medicinas.codigo_producto','medicinas.aplica_descuento','medicinas.factura','medicinas.porcentaje_descuento','medicinas.nombre', 'medicinas.precio_compra', 'medicinas.precio_venta','medicinas.laboratorio_id','medicinas.sucursal','medicinas.observaciones','medicinas.existencia','medicinas.fecha_vencimiento','medicinas.generico', 's.nombre as sucursalname', 'l.nombre as lab')
         ->where('medicinas.codigo_producto', 'like', '%'.$id.'%')
         ->orWhere('medicinas.nombre', 'like', '%'.$id.'%')
         ->orderBy('medicinas.nombre', 'desc')
         ->get();
         
      	return response()->json($info);
     
    }

    function actionSearchIdUnico(Request $request)
    {
    	 $output = '';
      $id = $request->id;
     
       $info = medicina::where('idunico', 'like', '%'.$id.'%')
         ->get();
         
      	return response()->json($info);
    }

}
