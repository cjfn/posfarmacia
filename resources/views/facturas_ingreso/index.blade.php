
@extends('layouts.app')



@section('content')

<meta name="csrf-token" content="{{ csrf_token() }}" />



<div class="container">
    <div class="row">
 
        <div class="col-md-12">
         
            <div class="panel panel-default">
            <div class="panel-heading">Menu Acceso rapido - INGRESOS</div>
            <div class="panel-body">
               <a href="{{ url('/laboratorios') }}" title="ingrese laboratorios" class="btn btn-success"><span class="fa fa-plus"></span>Laboratorios</a>
            <a href="{{ url('/medicinas') }}" title="ingrese nuevos productos a su inventario" class="btn btn-success"><span class="fa fa-plus"></span>Medicinas</a>
            <a href="{{ url('/sucursales') }}" title="ingrese nuevas sucursales de su empresa para asignarles productos" class="btn btn-success"><span class="fa fa-plus"></span>sucursales</a>
            <a href="{{ url('/facturas_ingreso') }}" title="ingrese nuevas facturas con ingreso de nuevo producto" class="btn btn-success"><span class="fa fa-plus"></span>Mis Facturas</a>
            <a href="{{ url('/misventas') }}" title="ingrese nuevos pedidos o facturas de ventas" class="btn btn-success"><span class="fa fa-plus"></span>Pedidos</a>
          </center>
            </div>
          </div>

             
    <div class="panel panel-primary">
                <div class="panel-heading"><strong>FACTURAS DE INGRESO</strong>
                </div>
    <div class="panel-body">
    <h2>Agrega facturas de ingreso de mercaderia de la institucion</h2>
    @if ($message = Session::get('success'))

      <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
      </div>
    @endif
    <div class="table-responsive">
    <table class="table table-bordered" id="MyTable">
    <center>
       <a href="{{ URL::previous() }}"><span class="fa fa-mail-reply"></span>Atras</a>
    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#addModal"><span class="fa fa-plus"></span>Agregar</button>
    <a href="{{ url('/medicinas') }}" class="btn btn-warning"><span class="glyphicon glyphicon-download-alt"></span>Agrega productos a tu inventario</a>
    <br/>

    <div class="alert alert-warning alert-dismissable">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>¡Cuidado! <span class="fa fa-warning"></span></strong> Todos los cambios que realices seran monitoreados
              </div>
    </center>
      <thead>
        <tr>
          <th>Numero</th>
          <th>Serie</th>
          <th>Cantidad</th>
          <th>Total Q.</th>
          <th>Opciones</th>
        </tr>
      </thead>
      <tbody>
      @foreach($data as $x)
        <tr>
          <td>{{$x -> numero}}</td>
          <td>{{$x -> serie}}</td>
           <td>{{$x -> cantidad_total}}</td>
          <td>{{$x -> total_factura_ingreso}}</td>
         

          <td>
              <button class="btn btn-info" data-toggle="modal" data-target="#viewModal" onclick="fun_view('{{$x -> id}}')"><span class="fa fa-eye"></span>Ver</button>
              <button class="btn btn-warning" data-toggle="modal" data-target="#editModal" onclick="fun_edit('{{$x -> id}}')"><span class="fa fa-pencil-square"></span>Agregar productos</button>
              <button class="btn btn-danger" title="opcion desabilitada"><span class="fa fa-times-circle"></span>Eliminar</button>
          </td>
        </tr>
       @endforeach
      </tbody>
    </table>
  </div>
    <input type="hidden" name="hidden_view" id="hidden_view" value="{{url('facturas_ingreso/view')}}">
    <input type="hidden" name="hidden_delete" id="hidden_delete" value="{{url('facturas_ingreso/delete')}}">
    <!-- Add Modal start -->
    <div class="modal fade" id="addModal" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Agregar nueva factura de ingreso</h4>
          </div>
          <div class="modal-body">
            <form action="{{ url('facturas_ingreso') }}" method="post">
              {{ csrf_field() }}
              <div class="form-group">
                <div class="form-group">
                  <label for="first_name">Numero:</label>
                  <input type="number" class="form-control" id="numero" name="numero" required="true">
                </div>
                <div class="form-group">
                  <label for="last_name">Serie:</label>
                  <input type="text" class="form-control" id="serie" name="serie" required="true">
                </div>
                <div class="form-group">
                  <label for="last_name">Cantidad total:</label>
                  <input type="number" title="suma total de productos en factura" class="form-control" id="cantidad_total" name="cantidad_total" required="true">
                </div>
                <div class="form-group">
                  <label for="last_name">Total Q.:</label>
                  <input type="text" class="form-control" id="total_ingreso" name="total_ingreso" required="true">
                </div>
              </div>
              
              <button type="submit" class="btn btn-default"><span class="fa fa-plus"></span>Agregar</button>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-window-close"></span>Cerrar</button>
          </div>
        </div>
        
      </div>
    </div>
    <!-- add code ends -->
 
    <!-- View Modal start -->
    <div class="modal fade" id="viewModal" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Detalles</h4>
          </div>
          <div class="modal-body">
            <p><b>Numero : </b><span id="view_numero" class="text-success"></span></p>
            <p><b>Serie : </b><span id="view_serie" class="text-success"></span></p>
            <p><b>Cantidad total de ingresos: </b><span id="view_cantidad_total" class="text-success"></span></p>
            <p><b>Total Q. : </b><span id="view_total_ingreso" class="text-success"></span></p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"></button>
          </div>
        </div>
        
      </div>
    </div>
    <!-- view modal ends -->
 
   
 
  </div>
  </div>
  </div>

 <link href="{{ url('/') }}/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">

                 
                 <link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/1.10.15/css/jquery.dataTables.css">
                <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
                <script src="//code.jquery.com/jquery-2.1.3.min.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.js"></script>

                <script language="javascript" type="text/javascript">
                                    
                    $(document).ready(function () {
                      $('#MyTable').DataTable();

                      $('#addModal').on('shown.bs.modal', function() {
                        $('#nombre').focus();
                      });
                  } );
                </script>
                 

  <script type="text/javascript">


    function fun_view(id)
    {
      var view_url = $("#hidden_view").val();
      $.ajax({
        url: view_url,
        type:"GET", 
        data: {"id":id}, 
        success: function(result){
          //console.log(result);
          $("#view_serie").text(result.serie);
          $("#view_numero").text(result.numero);
          $("#view_total_ingreso").text(result.total_ingreso);
          $("#view_cantidad_total").text(result.cantidad_total);
        }
      });
    }
 
    
 
  </script>


           


          </div>

          <!-- /.col-lg-12 -->

      </div>



  </div>

@endsection

