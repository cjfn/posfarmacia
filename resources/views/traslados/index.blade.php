
@extends('layouts.app')



@section('content')

<meta name="csrf-token" content="{{ csrf_token() }}" />



<div class="container" ng-app="myApp" ng-controller="myCtrl" >
    <div class="row">
 
        <div class="col-md-12">

             
    <div class="panel panel-primary">
                <div class="panel-heading">TRASLADOS DE PRODUCTOS 
                </div>
    <div class="panel-body">
    <h2>Traslada un producto a otra sucursal asignados a un usuario</h2>
    @if ($message = Session::get('success'))

      <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
      </div>
    @endif
    <div class="table-responsive">
    <table class="table table-bordered" id="MyTable2">
    <center>
      <a href="{{ URL::previous() }}"><span class="fa fa-mail-reply"></span>Atras</a>

       @if(Auth::user()->nombre_tipo_usuario=='ADMIN'||Auth::user()->nombre_tipo_usuario=='ENCARGADO')
                   <button type="button" class="btn btn-success" data-toggle="modal" id="btnaddprod" name="btnaddprod" data-target="#addModalProd" onclick="focusprod()"><span class="fa fa-plus"></span>Crear Nuevo traslado</button>
                    @else

                    @endif
    
    </center>
      <thead>
        <strong>
        <tr>
          <th><strong>Opciones Traslado</strong></th>

          <th>Producto </th>
          <th>Cantidad Entrega </th>
          <th>Destino</th>
          <th>Creado por:</th>
          <th>Entreago a:</th>
          <th>Estado </th>
          <th>Actualizado</th>
        </tr>
        </tr>
      </thead>
      <tbody>
      @foreach($data as $x)
        <tr>
          <td>

            @if($x->estado=='BODEGA TRANSITORIA')
               <button class="btn btn-warning" data-toggle="modal" data-target="#viewModal" onclick="fun_view('{{$x -> id}}')" title="ESTADO: BODEGA TRANSITORIA"><span class="fa fa-inbox"></span><strong>Detalles Traslado <strong>{{$x -> id}}</strong> </strong></button>

          @elseif($x->estado=='RECIBIDO')
                 <button class="btn btn-success" data-toggle="modal" data-target="#viewModal" onclick="fun_view('{{$x -> id}}')" title="ESTADO: RECIBIDO EN DESTINO"><span class="fa fa-check"></span><strong>Detalles Traslado <strong>{{$x -> id}}</strong> </strong></button>
          @elseif($x->estado=='NO ENTREGADO')
                 <button class="btn btn-danger" data-toggle="modal" data-target="#viewModal" onclick="fun_view('{{$x -> id}}')" title="ESTADO: NO ENTREGADO A DESTINO"><span class="fa fa-times"></span><strong>Detalles Traslado <strong>{{$x -> id}}</strong> </strong></button>
          @endif

           @if($x->estado=='RECIBIDO')
                 <button class="btn btn-default" disabled="true" data-toggle="modal"  onclick="fun_edit('{{$x -> id}}')"><span class="fa fa-retweet" title="Cambiar opciones de traslado">Actualizar Estado <strong>{{$x -> id}}</strong></span></button>
          @else
                <button class="btn btn-default" data-toggle="modal" data-target="#editModal" onclick="fun_edit('{{$x -> id}}')"><span class="fa fa-retweet" title="Cambiar opciones de traslado">Actualizar Estado <strong>{{$x -> id}}</strong></span></button>
        
          @endif


            
          </td>
          <td><strong>{{$x -> producto_codigo}}</strong>  {{$x -> nombre}}</td>
           <td>{{$x -> cantidad}}</td>
          <td>{{$x -> bodega}}</td>
          <strong>
           <td><strong>{{ $x->usuario_created }}</strong> 
            <h6>
            {{ date("Y-m-d", strtotime($x->fecha_created )) }}
          </h6>
          </td>
     
             @if($x->estado=='BODEGA TRANSITORIA')
          <td style="color: orange"><strong><span class="fa fa-inbox"></span>{{$x -> usuario}}</strong></td>
          @elseif($x->estado=='RECIBIDO')
          <td style="color: green"><strong><span class="fa fa-check"></span>{{$x -> usuario}}</strong></td>
          @elseif($x->estado=='NO ENTREGADO')
          <td style="color: red"><strong><span class="fa fa-times"></span>{{$x -> usuario}}</strong></td>
          @endif
           
         </strong>
          @if($x->estado=='BODEGA TRANSITORIA')
          <td style="color: orange"><strong>{{$x -> estado}}</strong></td>
          @elseif($x->estado=='RECIBIDO')
          <td style="color: green"><strong>{{$x -> estado}}</strong></td>
          @elseif($x->estado=='NO ENTREGADO')
          <td style="color: red"><strong>{{$x -> estado}}</strong></td>
          @endif
          

          <td>
            @if($x->fecha_updated!=null)
            {{ date("Y-m-d", strtotime($x->fecha_updated )) }}
          </td>
        @endif
        </tr>
       @endforeach
      </tbody>
    </table>


    {{ $data->links() }}


    
    <input type="hidden" name="hidden_view" id="hidden_view" value="{{url('traslados/view')}}">
    <input type="hidden" name="hidden_delete" id="hidden_delete" value="{{url('traslados/delete')}}">
    <!-- Add Modal start -->
    <div class="modal fade" id="addModalProd" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" >&times;</button>
            <h4 class="modal-title">Agregar nuevo traslado</h4>
          </div>
          <div class="modal-body">
            <form action="{{ url('traslados') }}" method="post">
              {{ csrf_field() }}

              <div class="form-group">
                <label for="last_name">Codigo Producto:</label>                  
                 
                  <input type="text" class="form-control" id="producto_codigo" title="Ingrese codigo de producto, si no aparece verifique existenciaen productos" name="producto_codigo" ng-change="buscar(codigo)" ng-model="codigo" required="true" tabindex="1" > 
              </div>
              <div class="form-group">
                  <label for="last_name">Descripcion:</label>                 
                  <input type="text" class="form-control" ng-model="nombre" readonly="readonly" id="descripcion" name="descripcion" required="true"> 
              </div>
              <div class="form-group">
                  <label for="last_name">Existencias:</label>                  
                  <input type="text" class="form-control" id="valor" readonly="true" readonly="readonly" name="valor" required="true" ng-model="valor"> 
              </div>
                
             
             
              <div class="form-group">
                 
                  <input type="hidden" class="form-control" id="existencia" name="existencia" readonly="readonly" required="true"> 
              </div>
              
              <div class="form-group">
                  <label for="last_name">Productos por fecha de vencimiento:</label>
                  


                  <select id="fecha_vence" name="fecha_vence"  onchange="nuevoitem()" class="form-control"   title="seleccione fecha de vencimiento" required="true">
                  </select>
                  <div id="id_div_contenedor">

                  </div>
              </div>
             


             
              <div class="form-group">
                  <label for="precio_venta">Cantidad de traslado:</label>
                  <input type="number" onchange="validaCantidad()" class="form-control" name="cantidad" id="cantidad" required="true">
                </div>
              <div class="form-group">
                  <label for="nombre">Seleccione destino</label>
                  <select id="sucursal_destino" name="sucursal_destino" required="true"  class="form-control">
                    <option value=""> Seleccione sucursal para envio ... </option>
                      @foreach($sucursal as $l)
                  
                      <option value="{{ $l -> id}}">{{ $l -> nombre}}  </option>
                  
                  @endforeach
                   </select>
               </div>
                <div class="form-group">
                  <label for="precio_venta">Seleccione encargado:</label>
                    <option value=""> Seleccione usuario para envio ... </option>
                   <select id="usuario" name="usuario" required="true" class="form-control">
                      @foreach($user as $l)
                  
                      <option value="{{ $l -> email}}">{{ $l -> name}} - {{ $l -> email}}  </option>
                  
                  @endforeach
                   </select>
                </div>
              
               
                 <div class="form-group">
                  <label for="observaciones">observaciones:</label>
                  
                <textarea title="este campo es requerido"  required="true" class="form-control" id="OBSERVACIONES" name="OBSERVACIONES"></textarea>
              </div>
                
              </div>
              <center>
              
              <button type="submit" class="btn btn-success btn-lg"><span class="fa fa-plus"></span><span class="fa fa-random"></span>Crear Nueva Traslado</button>
            </center>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-window-close"></span>Cerrar</button>
          </div>
        </div>
        
      </div>
    </div>
    <!-- add code ends -->
 
    <!-- View Modal start -->
    <div class="modal fade" id="viewModal" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Detalles</h4>
          </div>
          <div class="modal-body">
            <p><b>Producto : </b><span id="view_producto_codigo" class="text-success"></span></p>
            <p><b>Cantidad : </b><span id="view_cantidad" class="text-success"></span></p>
            <p><b>Sucursal de Origen : </b><span id="view_sucursal_origen" class="text-success"></span></p>
            <p><b>Sucursal de Destino : </b><span id="view_sucursal_destino" class="text-success"></span></p>
            <p><b>Encargado : </b><span id="view_usuario" class="text-success"></span></p>

            <p><b>Estado : </b><span id="view_estado_tran" class="text-warning">
                                <span id="view_estado_no" class="text-danger">
                                <span id="view_estado" class="text-success"></span></p>
            <p><b>Observaciones : </b><span id="view_OBSERVACIONES" class="text-success"></span></p>
             
            
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"></button>
          </div>
        </div>
        
      </div>
    </div>
    <!-- view modal ends -->
 
    <!-- Edit Modal start -->
    <div class="modal fade" id="editModal" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Actualizar estado de traslado</h4>
          </div>
          <div class="modal-body">
            <form action="{{ url('traslados/update') }}" method="post" autocomplete="off">
              {{ csrf_field() }}
              <div class="form-group">
                <div class="form-group">
                  
                  <input type="hidden" id="edit_producto_codigo" name="edit_producto_codigo">
                   <input  id="edit_vencimiento" name="edit_vencimiento">
                  
                  
                  

                </div>
              <div class="form-group">
                  
                  <input type="hidden" class="form-control" name="edit_cantidad" id="edit_cantidad">
                </div>
              <div class="form-group">
                  
                  <input type="hidden" class="form-control" name="edit_sucursal_destino" id="edit_sucursal_destino">
               </div>
                <div class="form-group">

                  <input type="hidden" class="form-control" name="edit_usuario" id="edit_usuario">
                  
                </div>
                <div class="form-group">
                  Confirme Usuario *
                  <input type="text" name="confuser" required="true" placeholder="ingrese usuario" id="confuser" class="form-control">
                </div>
                 <div class="form-group">
                  Confirme Contraseña *
                  <input type="password" name="confpassword" placeholder="ingrese password" required="true"  id="confpassword" class="form-control">
                </div>
              
               
                 <div class="form-group">
                  <label for="edit_registro">Seleccione nuevo Estado :</label>
                  <select id="edit_estado" required="true" name="edit_estado" class="form-control" title="seleccione nuevo cambio de estado">
                    <option value="NO ENTREGADO">NO ENTREGADO</option>
                    <option value="RECIBIDO">RECIBIDO EN SUCURSAL</option>
                  </select>
                </div>
                 <div class="form-group">
                  <label for="edit_nombre">Observaciones :</label>
                  <textarea class="form-control" id="edit_OBSERVACIONES" required="true" name="edit_OBSERVACIONES"></textarea>
                </div>


                
               
              
              <center>
              <button type="submit" class="btn btn-danger btn-lg"><span class="fa fa-retweet"></span>Cambiar de estado</button>
            </center>
              <input type="hidden" id="edit_id" name="edit_id">
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>
          
        </div>
        
      </div>
    </div>
    <!-- Edit code ends -->
 
  </div>
  </div>
  </div>



 <link href="{{ url('/') }}/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">

                 
                 <link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/1.10.15/css/jquery.dataTables.css">
                <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
                <script src="//code.jquery.com/jquery-2.1.3.min.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.js"></script>

    <script language="javascript" type="text/javascript">

      function validaCantidad()
      {
        var exist = parseInt(document.getElementById("valor").value);
        var trasl = parseInt(document.getElementById("cantidad").value);

        if(trasl>exist)
        {
          alert("cantidad de tralsado mayor a existencias");
          document.getElementById("cantidad").value = "";
          document.getElementById("cantidad").focus();
        }

      }
      function nuevoitem()
        {
        

         
                     var fecha_vence = document.getElementById("fecha_vence").value;
                      //alert(fecha_vence);
                      var cantidad_exist_sel = document.getElementById(fecha_vence).value;
                      alert("Cantidad de existencias:"+cantidad_exist_sel);
                      document.getElementById("cantidad").focus();
                      
                      //alert(cantidad);
                      //document.getElementById("existencia").style.visibility="visible";
                      document.getElementById("valor").style.visibility="visible";

                      var valor = parseInt(document.getElementById("valor").value=cantidad_exist_sel);
                      
                      var cantidad = document.getElementById("cantidad").value;
                      
                     
               
                          //lert(resultado);

                                //document.getElementById("subtotal").value = resultado;
                  
          
         
         
        }


      var app = angular.module('myApp', []).controller('myCtrl', function ($scope, $http) {
        

        
            
             $scope.calculo = function (i, j, k) {

                var valor = document.getElementById("valor").value;
                var cantidad = document.getElementById("cantidad").value;
                var descuento = document.getElementById("descuento").value;
               
                var resultado = (cantidad*valor)-((cantidad*valor)*(descuento/100));
                

              //lert(resultado);

                    document.getElementById("subtotal").value = resultado;
                

            }

               function nuevoitem()
        {
          var cantidad = parseInt(document.getElementById("cantidad").value);

          if(!cantidad)
                  {
                    alert("cantidad no puede ser vacia")
                  }
                  else
                  {
                     var fecha_vence = document.getElementById("fecha_vence").value;
                      //alert(fecha_vence);
                      var cantidad_exist_sel = document.getElementById(fecha_vence).value;
                      alert("Precio:"+cantidad_exist_sel);
                      
                      //alert(cantidad);
                      //document.getElementById("existencia").style.visibility="visible";
                      document.getElementById("valor").style.visibility="visible";

                      var valor = parseInt(document.getElementById("valor").value=cantidad_exist_sel);
                      
                      var cantidad = document.getElementById("cantidad").value;
                      var descuento = document.getElementById("descuento").value;
                           
                      var resultado = (cantidad*valor)-((cantidad*valor)*(descuento/100));
                            

                          //lert(resultado);

                                document.getElementById("subtotal").value = resultado;
                  }
          
         
         
        }

             // buscar detalles de producto por id
             $scope.buscar = function (i) {

                $scope.errortext = "";
                var cod = parseInt(i);
                //servicio que busca todo de producto por id
                $http.get("{{ url('api/productos/') }}/" + cod).then(function (response) {
                    $scope.items = response.data;
                    $scope.results = [];
                    //datos en consola
                    //console.log(response.data[0]["nombre"]);
                    console.log(response.data);

                        if(response.data.length>1)
                    {
                        alert("Producto tiene dos existencias, seleccione por fecha de vencimiento");
                         var num=1;
                        var existr=0;
                        var creados="";
                        document.getElementById("fecha_vence").focus();
                        for(var prop in response.data)
                        {
                            // var laboratorio = response.data[prop]["laboratorio_id"];
                             //alert(response.data[prop]["laboratorio_id"]);
                             //alert(response.data[prop]["fecha_vencimiento"]);
                             $("#fecha_vence").append('<option value='+response.data[prop]["fecha_vencimiento"]+'>'+response.data[prop]["fecha_vencimiento"]+'  - Producto:'+response.data[prop]["nombre"]+' - Sucursal Origen:'+response.data[prop]["sucursal"]+'</option>');
                                existr=existr;
                                var name = response.data[0]["nombre"];
                                var precio = response.data[0]["precio_venta"];
                                var existencia = response.data[0]["existencia"];
                                var sucursal = response.data[0]["sucursal"];
                                var all_existencia= response.data[num]["existencia"];
                                var all_fechas= response.data[num]["fecha_vencimiento"];
                                var all_sucursales= response.data[num]["sucursal"];

                                document.getElementById("descripcion").value = name;
                                document.getElementById("valor").value = existencia;
                                document.getElementById("existencia").style.visibility="hidden";
                                //alert(all_existencia);


                                existr=existr+1;
                                num = num+1;


                                 var micapa = document.getElementById('id_div_contenedor');
                                creados = creados+'<input type="hidden" id="'+all_fechas+'" value="'+all_existencia+'" name="'+all_fechas+'" />';
                                micapa.innerHTML= creados;
                                
                                




             
                        }



                    }
                    var name = response.data[0]["nombre"];
                    var precio = response.data[0]["precio_venta"];
                    var existencia = response.data[0]["existencia"];
                    var sucursal = response.data[0]["sucursal"];
                    var sucursalname = response.data[0]["sucursalname"];

                    var vencimiento = response.data[0]["fecha_vencimiento"];


                    
                    document.getElementById("descripcion").value = name;
                    document.getElementById("valor").value = existencia;
                    document.getElementById("existencia").value = existencia;
                    $("#fecha_vence").append('<option value='+vencimiento+'>'+vencimiento+'</option>');

                    $("#sucursal_origen").append('<option value='+sucursal+'>'+sucursal+'</option>');

                    document.getElementById("cantidad").focus();
 
                  });
               

                
                
              

            }


      



    });

                                    
                    $(document).ready(function () {
                      $('#MyTable').DataTable();
                       $('#addModalProd').on('shown.bs.modal', function() {
                        $('#producto_codigo').focus();
                      });
                  } );
                
    function fun_view(id)
    {
      var view_url = $("#hidden_view").val();
      $.ajax({
        url: view_url,
        type:"GET", 
        data: {"id":id}, 
        success: function(result){
          //console.log(result);
          $("#view_producto_codigo").text(result.producto_codigo);
          $("#view_sucursal_origen").text(result.sucursal_origen);
          $("#view_sucursal_destino").text(result.bodega);
          if(result.estado=="BODEGA TRANSITORIA")
          {
            $("#view_estado_tran").text(result.estado);
          }
          if(result.estado=="NO ENTREGADO")
          {
            $("#view_estado_no").text(result.estado);
          }
          if(result.estado=="RECIBIDO")
          {
            $("#view_estado").text(result.estado);
          }
          //
          $("#view_usuario").text(result.usuario);
          $("#view_cantidad").text(result.cantidad);
          $("#view_OBSERVACIONES").text(result.OBSERVACIONES);
          
         
        }
      });
    }
 
    function fun_edit(id)
    {
      var view_url = $("#hidden_view").val();
      $.ajax({
        url: view_url,
        type:"GET", 
        data: {"id":id}, 
        success: function(result){
          //console.log(result);
          $("#edit_id").val(result.id);

          $("#edit_producto_codigo").val(result.cantidad);
          $("#edit_usuario").val(result.usuario);
          $("#edit_cantidad").val(result.cantidad);
          $("#edit_sucursal_destino").val(result.sucursal_destino);
          $("#edit_estado").val(result.estado);
          $("#edit_OBSERVACIONES").val(result.OBSERVACIONES);
          $("#edit_vencimiento").val(result.fecha_vencimiento);
         
        }
      });
    }
 
    function fun_delete(id)
    {
      var conf = confirm("Esta seguro que desea eliminar??");
      if(conf){
        var delete_url = $("#hidden_delete").val();
        $.ajax({
          url: delete_url,
          type:"POST", 
          data: {"id":id,_token: "{{ csrf_token() }}"}, 
          success: function(response){
            alert(response);
            location.reload(); 
          }
        });
      }
      else{
        return false;
      }
    }



    
  </script>


           


          </div>

          <!-- /.col-lg-12 -->

      </div>



  </div>

@endsection

