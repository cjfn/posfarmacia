<?php

namespace App\model\bitacora;

use Illuminate\Database\Eloquent\Model;

class bitacora extends Model
{
    //
    protected $table = 'bitacora';
	 protected $primarykey='id';
	 public $timestamps = false;
    
    protected $fillable = [
        'tipo_proceso', 'descripcion', 'usuario'
    ];
}
