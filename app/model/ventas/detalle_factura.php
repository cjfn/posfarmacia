<?php

namespace App\model\ventas;

use Illuminate\Database\Eloquent\Model;

class detalle_factura extends Model
{
    //

     protected $table = 'detalle_facturas';
	 protected $primarykey='id';
	 public $timestamps = false;
    
    protected $fillable = [
        'factura', 'codigo_producto', 'cantidad', 'subtotal', 'usuario', 'fecha_updated','idunico'
    ];
}
