<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => 'auth'], function () {



Route::get('/home', 'HomeController@index');

Route::get('laboratorios', 'laboratoriosController@index');
Route::post('laboratorios', 'laboratoriosController@add');
Route::get('laboratorios/view', 'laboratoriosController@view');
Route::post('laboratorios/update', 'laboratoriosController@update');
Route::post('laboratorios/delete', 'laboratoriosController@delete');
Route::get('select2-autocomplete-ajax', 'laboratoriosController@dataAjax');

Route::get('medicinas', 'medicinaController@index');
Route::post('medicinas', 'medicinaController@add');
Route::get('medicinas/view', 'medicinaController@view');
Route::post('medicinas/update', 'medicinaController@update');
Route::post('medicinas/delete', 'medicinaController@delete');

Route::get('sucursales', 'sucrusalesController@index');
Route::post('sucursales', 'sucrusalesController@add');
Route::get('sucursales/view', 'sucrusalesController@view');
Route::post('sucursales/update', 'sucrusalesController@update');
Route::post('sucursales/delete', 'sucrusalesController@delete');

Route::get('ventas', 'ventasController@index');
Route::post('ventas', 'ventasController@add');
Route::get('ventas/view', 'ventasController@view');
Route::post('ventas/update', 'ventasController@update');
Route::post('ventas/delete', 'ventasController@delete');


Route::get('misventas', 'misventasController@index');


Route::resource('factura','facturaController');
Route::get('factura', 'facturaController@index');
Route::post('factura', 'facturaController@add');
Route::get('factura/view', 'facturaController@view');
Route::post('factura/update', 'facturaController@update');
Route::post('factura/delete', 'facturaController@delete');


Route::resource('detalle_factura','detalle_facturaController');
Route::get('detalle_factura', 'detalle_facturaController@index');
Route::post('detalle_factura', 'detalle_facturaController@add');
Route::get('detalle_factura/view', 'detalle_facturaController@view');
Route::post('detalle_factura/update', 'detalle_facturaController@update');
Route::post('detalle_factura/delete', 'detalle_facturaController@delete');



Route::resource('detalle_factura_show','detalle_facturashowController');
Route::get('detalle_factura_show', 'detalle_facturashowController@index');
Route::post('detalle_factura_show', 'detalle_facturashowController@add');
Route::get('detalle_factura_show/view', 'detalle_facturashowController@view');


Route::resource('traslados','trasladosController');
Route::get('traslados', 'trasladosController@index');
Route::post('traslados', 'trasladosController@add');
Route::get('traslados/view', 'trasladosController@view');
Route::post('traslados/update', 'trasladosController@update');

Route::resource('facturas_ingreso','facturas_ingresoController');
Route::get('facturas_ingreso', 'facturas_ingresoController@index');
Route::post('facturas_ingreso', 'facturas_ingresoController@add');
Route::get('facturas_ingreso/view', 'trasladosController@view');
//Route::post('facturas_ingreso/update', 'trasladosController@update');


Route::get('ajustes', 'ajustesController@index');

Route::get('pdf', 'PdfControler@invoice');

Route::get('pdfVentasPorFecha', 'PdfControler@invoiceVentasPorFecha');
Route::get('invoiceVentasEntreFecha', 'PdfControler@invoiceVentasEntreFecha');

Route::get('reporte', 'PdfControler@reportes');

Route::get('invoiceAjustesAll', 'PdfControler@invoiceAjustesAll');
Route::get('invoiceAjustesFechas', 'PdfControler@invoiceAjustesFecha');



Route::get('invoiceProductosAll', 'PdfControler@invoiceProductosAll');
Route::get('productos', 'PdfControler@productos');



Route::get('invoiceTodosLosProductos', 'PdfControler@invoiceProductosAll');
Route::get('invoiceProductosFecha', 'PdfControler@invoiceProductosFecha');
Route::get('invoiceAjustesEntreFecha', 'PdfControler@invoiceAjustesEntreFecha');
Route::get('invoiceVencimientosEntreFecha', 'PdfControler@invoiceVencimientosEntreFecha');



Route::get('invoiceProductosEntreFecha', 'PdfControler@invoiceProductosEntreFecha');

Route::get('invoiceProductosEntreFechapdf', 'PdfControler@invoiceProductosEntreFechapdf');


Route::get('invoicethisventa', 'PdfControler@invoicethisventa');

Route::get('invoiceTopVentas', 'PdfControler@TopVentas');


Route::get('articles', 'medicinaController@existproduct');





Route::get('usuariosABC123DEF456usuariosABC123DEF456', 'usuariosController@index');
Route::post('usuariosABC123DEF456usuariosABC123DEF456', 'usuariosController@add');
Route::get('usuariosABC123DEF456usuariosABC123DEF456/view', 'usuariosController@view');
Route::post('usuariosABC123DEF456usuariosABC123DEF456/update', 'usuariosController@update');
Route::post('usuariosABC123DEF456usuariosABC123DEF456/delete', 'usuariosController@delete');

Route::get('/live_search/action/{id}', 'LiveSearchMedicinaController@action');
Route::get('/live_search/actionSearchIdUnico/{id}', 'LiveSearchMedicinaController@actionSearchIdUnico');







Route::get('laboratorios/getlaboratorios', 'laboratoriosController@getlaboratorios');

});


Auth::routes();