<?php

namespace App\Http\Controllers;


use App\model\medicina\medicina;

use Illuminate\Http\Request;
use DB;

class apiProductosController extends Controller
{
    //
     //
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
         $data = medicina::all()->toArray();

            return response()->json($data);
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
/*
        try{
            $data = new detalle_factura;
            $data -> factura = $request -> factura;
            $data -> codigo_producto = $request ->  codigo_producto;
            $data -> cantidad = $request -> cantidad;
            $data -> subtotal = $request -> subtotal;
            $data -> save(); 

            return response()->json(['status'=>true, 'detalle de factura creado'],200);
        }
        catch(Exception $e)
        {

            return response()->json(['status'=>false, 'error'],400);
        }
        */
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
      /*
            $info=medicina::rightjoin('sucursales as s', 'medicinas.sucursal','=','s.id')
            ->leftjoin('laboratorios as l','medicinas.laboratorio_id','=','l.id')
        
        ->select(DB::raw('DISTINCT(medicinas.id)'), 'medicinas.laboratorio_id', 'medicinas.codigo_producto', 'medicinas.nombre', 'medicinas.precio_compra', 'medicinas.precio_venta','medicinas.sucursal as id_suc','medicinas.existencia','medicinas.fecha_vencimiento','medicinas.generico', 's.nombre as sucursal', 'l.nombre as lab', 'medicinas.idunico')
            ->where('existencia','>', 0)
            ->where('codigo_producto', '=', $id)
            

            ->get();
            */

            $info = medicina::join('sucursales as s','medicinas.sucursal','=','s.id')
            ->join('laboratorios as l','medicinas.laboratorio_id','=','l.id')
            ->select('medicinas.laboratorio_id', 'medicinas.codigo_producto', 'medicinas.nombre', 'medicinas.precio_compra', 'medicinas.precio_venta','medicinas.sucursal as id_suc','medicinas.existencia','medicinas.fecha_vencimiento','medicinas.generico', 's.nombre as sucursal', 'l.nombre as lab', 'medicinas.idunico')
            ->where('existencia','>', 0)
            ->where('codigo_producto','=', $id)
            ->get();






           	return response()->json($info);

           
             
        }
        catch(Exception $e)
        {
            return response()->json(['status'=>false, 'error'],400);    
        }

                
            
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    //existencias de productos
    public function edit($id)
    {
     
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function bySucursal(Request $request)
    {
        
        

            return response()->json($info);
    }
}
