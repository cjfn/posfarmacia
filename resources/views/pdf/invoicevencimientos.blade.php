
@extends('layouts.app')



@section('content')

<meta name="csrf-token" content="{{ csrf_token() }}" />



<div class="container">
  <center>

 <span  tabindex="4" onclick="printDiv('areaImprimir')" onkeypress="printDiv('areaImprimir')" value="imprimir div" class="btn btn-danger btn-lg" ><span class="fa fa-print"></span>IMPRIMIR</span>
</center>
<div id="areaImprimir" >



          <CENTER>
          <h2> <STRONG> FARMACIA LA SALUD </STRONG></h2>
          <H5>FECHA: {{ $date }} - REPORTE DE FECHAS DE VENCIMIENTO DE PRODUCTOS </H5>
            <H4>DEL {{substr($from, 0, 10)}} AL {{substr($to, 0, 10)}}</H4>


          
      <table class="table table-bordered">
        <thead>
          <tr>
            <th class="">Cod.</th>
            <th class="">Nombre</th>
            <th class="">Precio Q.</th>
            <th class="">Cant.</th>
            <th class="">factura</th>
            <th class="">Lab</th>
            <th class="">Sucursal</th>
            <th class="">Fecha Vencimiento</th>
            
            
      

          </tr>
        </thead>
        <tbody>
          
             @foreach($data as $key => $dat)
             <tr>
            <td class="">{{ $dat->codigo_producto }}</td>
            <td> {{ $dat->nombre }}</td>
            <td> {{ $dat->precio_venta }}</td>
            <td> {{ $dat->existencia }}</td>
            <td> {{ $dat->factura }}</td>
            <td> {{ $dat->lab }}</td>
            <td> {{ $dat->sucursalname }}</td>
             <td> {{ $dat->fecha_vencimiento }}</td>
            </tr>
            @endforeach

          

        </tbody>
        <CENTER>
          
        </CENTER>
       
      </table>
   </div>

  <script type="text/javascript">
    

      function printDiv(nombreDiv) {
     var contenido= document.getElementById(nombreDiv).innerHTML;
     var contenidoOriginal= document.body.innerHTML;

     document.body.innerHTML = contenido;

     window.print();

     document.body.innerHTML = contenidoOriginal;
}
  </script>

@endsection


