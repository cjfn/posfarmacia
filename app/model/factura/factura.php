<?php

namespace App\model\factura;

use Illuminate\Database\Eloquent\Model;

class factura extends Model
{
    //
     protected $table = 'facturas';
	 protected $primarykey='id';
	 public $timestamps = false;
    
    protected $fillable = [
        'total', 'usuario', 'fecha_created', 'activo'
    ];
}
