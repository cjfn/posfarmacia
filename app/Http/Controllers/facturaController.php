<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\model\ventas\ventas;
use App\model\factura\factura;
use App\model\ventas\detalle_factura;
use App\model\medicina\medicina;

use Auth;

class facturaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
          $data = new detalle_factura;
            $data -> total = 0.0;
            $data -> activo = 0;
            $data -> usuario = Auth::user()->email ;
            $data -> save();
            return view('factura.create',['data'=>$productos, 'productos'=>$productos]);
    }

    public function update(Request $request)    {
        //
            $id = $request -> factura;
            $data = factura::find($id);
            $data -> total = $request -> subtotal;
            $data -> usuario = Auth::user()->email ;
            $data -> activo = 1;
            $data -> save();
            return back()
                    ->with('success','Record Updated successfully.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
            $id = $request -> edit_id;
            $data = cursos::find($id);
            $data -> nombre = $request -> edit_nombre;
            $data -> descripcion = $request -> edit_descripcion;
            $data -> save();
            return back()
                    ->with('success','Record Updated successfully.');

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
