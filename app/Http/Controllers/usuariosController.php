<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\model\sucursal\sucursal;
use DB;
use Auth;

class usuariosController extends Controller
{
    //
    public function index()
    {
        $sucursal = sucursal::where('activo','=', '1')->get();
        $data = User::where('activo','=', '1')->get();
        return view('User.index',['data'=>$data,'sucursal'=>$sucursal]);
    }

   public function add(Request $request)
        {
            $data = new User;
            $data -> name = $request -> name;
            $data -> email = $request -> email;
            $data -> password = bcrypt($request -> password);
            //$data -> tipo_usuario = $request -> tipo_usuario;
            $data -> nombre_tipo_usuario = $request -> nombre_tipo_usuario;

            $data -> sucursal = $request -> sucursal;

            $data -> save();
            return back()
                    ->with('success','Usuario agregado exitosamente.');
        }

        /*
         * View data
         */
        public function view(Request $request)
        {
            if($request->ajax()){
                $id = $request->id;
                $info = User::find($id);
                //echo json_decode($info);
                return response()->json($info);
            }
        }

        public function getlaboratorios(Request $request)
        {
            if($request->ajax()){
                
                $info = User::all();
                //echo json_decode($info);
                return response()->json($info);
            }
        }

         /*
        *   Update data
        */
        public function update(Request $request)
        {
            $id = $request -> edit_id;
            $data = User::find($id);
            $data -> name = $request -> edit_name;
            $data -> email = $request -> edit_email;
            $data -> password = bcrypt($request -> edit_password);
            $data -> nombre_tipo_usuario = $request -> edit_tipo_usuario;

            $data -> sucursal = $request -> edit_sucursal;
            $data -> save();
            return back()
                    ->with('finalizado','Datos modificados exitosamente.');
        }
 
        /*
        *   Delete record
        */
        public function delete(Request $request)
        {
            $id = $request -> id;
            $data = User::find($id);
             $data -> activo = false;
            $response = $data -> update();
            if($response)
                echo "Dato eliminado exitosamente.";
            else
                echo "There was a problem. Please try again later.";
        }

}
