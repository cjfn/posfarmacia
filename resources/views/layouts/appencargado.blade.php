<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'miPos') }}</title>

    <!-- Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">
    <!-- Styles --><!-- Styles -->
    <link rel="stylesheet" href="{{ url('/')}}/css/bootstrap.min.css">
    <!-- Styles -->
    <link rel="stylesheet" href="{{ url('/')}}/css/font-awesome.min.css">
     <link href="{{ url('/') }}/css/bootstrap.css" rel="stylesheet" type="text/css">
    <!-- Custom Fonts -->
    <link href="{{ url('/') }}/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Mis Estilos CSS-->
    <link href="{{ url('/') }}/css/estilos.css" rel="stylesheet" type="text/css">
    <link rel="shortcut icon" type="image/x-icon" href="{{ url('/') }}/favicon.ico" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />   
    <script src="{{ url('/') }}/js/Chart.js"></script>
    <script src="{{ url('/') }}/js/angular.min.js"></script>
    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
    <script type="text/javascript">
        function myfocus() {
     document.getElementById("codigo_producto").focus();
     document.getElementById("btnaddprod").focus();
      

     
}
    </script>
</head>
<body onload="myfocus()">
    <div id="app">
          <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <H4>FARMACIAS LA SALUD ZACAPA</H4>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ url('/login') }}">Login</a></li>
                            <li><a href="{{ url('/register') }}">Registro</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    <strong>Usuario {{ Auth::user()->name }} <span class="caret"></span></strong>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ url('/logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

      <nav class="navbar navbar-inverse sidebar" role="navigation">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-sidebar-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">ENCARGADO {{ Auth::user()->name }} </a>
            <center>
                <a href="#">
                <strong> </strong>
            </a>
            </center>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-sidebar-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="active"><a href="{{ url('/home') }}">Home<span class="glyphicon glyphicon-home"></a></li>
                 <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" title="ingrese o administre nuevas actividades en su inventario">Ingresos <span class="caret"></span><span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-download-alt"></span></a>
                    <ul class="dropdown-menu forAnimate" role="menu">
                        <li><a href="{{ url('/laboratorios') }}" title="ingrese laboratorios">Ingreso de Laboratorios</a></li>
                        <li><a href="{{ url('/medicinas') }}" title="ingrese nuevos productos a su inventario">Ingreso de Medicinas</a></li>
                        <li><a href="{{ url('/sucursales') }}" title="ingrese nuevas sucursales de su empresa para asignarles productos">Ingreso de sucursales</a></li>
                    </ul>
                </li>
                <li ><a href="{{ url('/ventas') }}" title="cree nuevos pedidos y facture sus pedidos">Ventas<span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-usd"></span></a></li>

                <li ><a href="{{ url('/ajustes') }}" title="Realice movimientos a sus productos">Ajustes<span style="font-size:16px;" class="pull-right hidden-xs showopacity fa fa-cogs"></span></a></li>

                <li ><a href="{{ url('/traslados') }}" title="Realice movimientos a sus productos">Traslados<span style="font-size:16px;" class="pull-right hidden-xs showopacity fa fa-random"></span></a></li>
               
               
               <li class="dropdown">
                    <a href="/reportes" class="dropdown-toggle" data-toggle="dropdown">Reportes <span class="caret"></span><span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-file"></span></a>
                    <ul class="dropdown-menu forAnimate" role="menu">
                        <li><a href="{{ url('/pdf') }}">Todas las ventas</a></li>
                        <li><a href="{{ url('/medicinas') }}">Ventas por fecha</a></li>
                        <li><a href="{{ url('/descuentos') }}">Ingresos por fechas</a></li>
                        <li><a href="{{ url('/laboratorios') }}">Inventario</a></li>
                        <li><a href="{{ url('/laboratorios') }}">Fechas de Vencimientos</a></li>
                        <li><a href="{{ url('/laboratorios') }}">Descargas o ajustes</a></li>
                        <li><a href="{{ url('/laboratorios') }}">Reporte administrativo</a></li>
                    </ul>
                </li>
                
                 

                
            </ul>
        </div>
    </div>
</nav>
<div class="main">
     @yield('content')
</div>
       
    </div>

    <!-- Scripts -->

    

    <script type="text/javascript" language="javascript" src="{{ url('/') }}/Datatables/media/js/jquery.dataTables.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js"></script>
  


    <script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.12.4.js">
    </script>
 

    <script type="text/javascript" language="javascript" class="init"></script>

   
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}

    <script src="{{ url('/') }}//js/Chart.bundle.min.js"></script>
    
    <script src="{{ url('/') }}//js/Chart.min.js"></script>
  

    <script src="{{ url('/') }}//js/myscript.js"></script>


    </script>
    </script>
    <script type="text/javascript" language="javascript" class="init"></script>

   

    <script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>


</body>
</html>
