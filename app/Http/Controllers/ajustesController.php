<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\medicina\medicina;
use App\model\laboratorio\laboratorio;
use App\model\sucursal\sucursal;

use App\model\factura\factura;
use App\model\ventas\detalle_factura;
use DB;

class ajustesController extends Controller
{
    //
     public function index()
    {
          $data = medicina::join('sucursales as s', 'medicinas.sucursal','=','s.id')
        ->join('laboratorios as l','medicinas.laboratorio_id','=','l.id')
        //->where('medicinas.existencia','>',0)
        
        ->select(DB::raw('DISTINCT(medicinas.id)'), 'medicinas.codigo_producto','medicinas.aplica_descuento','medicinas.porcentaje_descuento','medicinas.nombre', 'medicinas.precio_compra', 'medicinas.precio_venta','medicinas.laboratorio_id','medicinas.sucursal','medicinas.existencia','medicinas.fecha_vencimiento','medicinas.generico', 's.nombre as sucursalname', 'l.nombre as lab')
        ->paginate(10);
         $data2 = factura::orderBy('id', 'DESC')->get();
         $lab = laboratorio::where('activo','=', '1')->get();
         $sucursal = sucursal::where('activo','=', '1')->get();
            return view('ajustes.index',['data'=>$data, 'data2'=>$data2, 'lab'=>$lab,'sucursal'=>$sucursal]);
    }
}


