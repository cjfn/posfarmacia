<?php

namespace App\model\medicina;

use Illuminate\Database\Eloquent\Model;

class medicina extends Model
{
     protected $table = 'medicinas';
	 protected $primarykey='id';
	 public $timestamps = false;
    
    protected $fillable = [
        'codig_producto', 'aplica_descuento', 'porcentaje_descuento', 'nombre', 'precio_venta', 'precio_compra', 'existencia', 'factura', 'laboratorio_id','registro','fecha_vencimiento', 'sucursal', 'activo', 'generico', 'observaciones', 'usuario', 'fecha_updated','idunico'
    ];
}
