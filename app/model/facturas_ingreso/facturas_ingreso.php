<?php

namespace App\model\facturas_ingreso;

use Illuminate\Database\Eloquent\Model;

class facturas_ingreso extends Model
{
    //
     protected $table = 'facturas_ingreso';
	 protected $primarykey='id';
	 public $timestamps = false;
    
    protected $fillable = [
        'serie', 'numero', 'cantidad_total', 'total_factura_ingreso', 'observaciones', 'fecha_created'
    ];
}
