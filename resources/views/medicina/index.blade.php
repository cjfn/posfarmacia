
@extends('layouts.app')



@section('content')
 
<meta name="csrf-token" content="{{ csrf_token() }}" />



<style>
/* Center the loader */
#loader {
  position: absolute;
  left: 50%;
  top: 50%;
  z-index: 1;
  width: 150px;
  height: 150px;
  margin: -75px 0 0 -75px;
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid #3498db;
  width: 120px;
  height: 120px;
  -webkit-animation: spin 2s linear infinite;
  animation: spin 2s linear infinite;
}

@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}

/* Add animation to "page content" */
.animate-bottom {
  position: relative;
  -webkit-animation-name: animatebottom;
  -webkit-animation-duration: 1s;
  animation-name: animatebottom;
  animation-duration: 1s
}

@-webkit-keyframes animatebottom {
  from { bottom:-100px; opacity:0 } 
  to { bottom:0px; opacity:1 }
}

@keyframes animatebottom { 
  from{ bottom:-100px; opacity:0 } 
  to{ bottom:0; opacity:1 }
}

#myDiv {
  display: none;
  text-align: center;
}
</style>
<div class="container" ng-app="calc" ng-controller="myCtrl">
    <div class="row">
 
        <div class="col-md-12">
            <div class="panel panel-default">
            <div class="panel-heading">Menu Acceso rapido - INGRESOS</div>
            <div class="panel-body">
              <a href="{{ URL::previous() }}" class="btn btn-default" title="Este boton regresara a la venta activa si existe"><span class="fa fa-mail-reply"></span>Recuperar venta</a>
               <a href="{{ url('/laboratorios') }}" title="ingrese laboratorios" class="btn btn-success"><span class="fa fa-plus"></span>Laboratorios</a>
            <a href="{{ url('/medicinas') }}" title="ingrese nuevos productos a su inventario" class="btn btn-success"><span class="fa fa-plus"></span>Medicinas</a>
            <a href="{{ url('/sucursales') }}" title="ingrese nuevas sucursales de su empresa para asignarles productos" class="btn btn-success"><span class="fa fa-plus"></span>sucursales</a>
            <a href="{{ url('/facturas_ingreso') }}" title="ingrese nuevas facturas con ingreso de nuevo producto" class="btn btn-success"><span class="fa fa-plus"></span>Mis Facturas</a>
            <a href="{{ url('/misventas') }}" title="ingrese nuevos pedidos o facturas de ventas" class="btn btn-success"><span class="fa fa-plus"></span>Pedidos</a>
          </center>
            </div>
          </div>
          <center>
           

             
    <div class="panel panel-primary">
                <div class="panel-heading">TUS PRODUCTOS CLINICOS
                </div>
    <div class="panel-body">
    <h2>Administra tu inventario con productos de ingreso</h2>
    @if ($message = Session::get('success'))

      <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
      </div>
    @endif
    <div class="container">
      <div class="row">
        
        <div class="col-sm-10">
        <input id="nSearch" class="form-control" type="text" placeholder="Busqueda por codigo o nombre" aria-label="Search">
        </div>
        <div class="col-sm-2">

          <div class="btn btn-default" onclick="searchData()"><span class="fa fa-search"></span></div>
          <span class="fa fa-refresh" title="Nueva Busqueda" onclick="newSearch()"></span>
        </div>
      </div>
    </div>
    <div class="table-responsive">
    <center>
      <a href="{{ URL::previous() }}" class="btn btn-default"><span class="fa fa-mail-reply"></span>Atras</a>
      <button type="button" class="btn btn-success" data-toggle="modal" id="btnaddprod" name="btnaddprod" data-target="#addModalProd"><span class="fa fa-plus"></span>Agregar</button> 
            <h4 class="modal-title">Agregar nuevo producto</h4>
    
    </center>
    <!-- Search form -->
    
    <table class="table table-bordered table-hover" id="MyTable">
   
      <thead>
        <tr>
          <th>Cod </th>
          <th>Nombre</th>
          <th>Precio Compra</th>
          <th>Precio Venta</th>
          <th>existencias</th>
          <th>vencimiento </th>
          <th>generico </th>
          <th>Sucursal </th>
          <th>Laboratorio </th>
          <th>opciones</th>
        </tr>
      </thead>
      <tbody>
      @foreach($data as $x)
        <tr>
          <td><strong>{{$x -> codigo_producto}}</strong></td>
          <td>{{$x -> nombre}}</td>
          <td>{{$x -> precio_compra}}</td>
          <td>{{$x -> precio_venta}}</td>
          <td>{{$x -> existencia}}</td>
          <td>{{$x -> fecha_vencimiento}}</td>
          <td>{{$x -> generico}}</td>
          <td>{{$x -> sucursalname}}</td>
          <td>{{$x -> lab}}</td>

          <td>
              <button class="btn btn-info" data-toggle="modal" data-target="#viewModal" onclick="fun_view('{{$x -> id}}')" title="ver"><span class="fa fa-eye"></span><strong>Ver Detalles</strong></button>
             
              
          </td>
        </tr>
       @endforeach
      </tbody>
    </table>

      {{ $data->links() }}
  </div>
    <input type="hidden" name="hidden_view" id="hidden_view" value="{{url('medicinas/view')}}">
    <input type="hidden" name="hidden_delete" id="hidden_delete" value="{{url('medicinas/delete')}}">
    <input type="hidden" name="hidden_new_search" id="hidden_new_search" value="{{url('medicinas')}}">
    <div class="ajax-loader">
          <img src="{{ url('/imgs/loading.gif') }}" class="img-responsive" />
        </div>
      <!-- Add Modal start -->
    <div class="modal fade" id="addModalProd" role="dialog" >
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content"  >
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" >&times;</button>
            <h4 class="modal-title">Agregar nuevo producto</h4>
          </div>
          <div class="modal-body">
            <form action="{{ url('medicinas') }}" method="post">
              {{ csrf_field() }}



              <div class="form-group">
                  <label for="first_name">Codigo:*</label>
                  <input type="text" maxlength="50" class="form-control" id="codigo_producto" ng-change="buscar(codigo)" ng-model="codigo" tabindex="1" name="codigo_producto" required="true">
                </div>
              <div class="form-group">
                  <label for="nombre">Nombre del Producto:*</label>
                  <input type="text" class="form-control" tabindex="2" id="nombre" name="nombre" required="true">
                </div>
                <div class="form-group">
                  <label for="precio_venta" id="lblprecioant" style="visibility: hidden;">  Precio anterior:</label>
                 
                   <input type="text" class="form-control" disabled="true" style="visibility: hidden;"id="precio_anterior" name="precio_anterior" required="true" ng-model="venta">
                   Ingrese nuevo Precio de venta:*

                  <input type="text" class="form-control" tabindex="3" id="precio_venta" name="precio_venta" required="true" ng-change="calculo(venta, desc)" ng-model="venta">
                </div>
              
                
                <div class="form-group">
                  <label for="last_name">Aplica descuento:</label>
                  <input type="radio" id="aplica_descuento" name="aplica_descuento" value="1" tabindex="4" checked> Si<br>
                </div>
                <div class="form-group">
                  <label for="last_name">Porcentaje descuento:</label>
                  <input type="number" title="ejemplo: 5 = 5%" class="form-control"   name="descuento" id="descuento" tabindex="5" required="true" placeholder="0" ng-change="calculo(venta, desc)" value="0" ng-model="desc">
                </div>
                 <div class="form-group">
                  <label for="last_name">Descuento:</label>
                  <input type="number" class="form-control" id="porcentaje_descuento" name="porcentaje_descuento" required="true"  readonly="readonly">
                </div>
                
                <div class="form-group">
                  <label for="precio_compra">Precio de compra*:</label>
                  <input type="text" class="form-control" id="precio_compra" readonly="readonly" name="precio_compra">
                </div>
                <div class="form-group">
                  <label for="existencia">Cantidad ingreso*:</label>
                  <input type="number" class="form-control" id="existencia" name="existencia" tabindex="6"required="true">
                </div>
                <div class="form-group">
                  <label for="factura">Seleccione Numero de factura de ingreso:</label>
                  

                  <select id="factura" title="Si no aparece en esta lista click en agregar facturas de ingreso"  name="factura" required="true"  tabindex="7" class="form-control">
                      @foreach($fac_ing as $l)
                  
                      <option value="{{ $l -> numero}}">{{ $l -> numero}} -Serie: {{ $l -> serie}} </option>
                  
                  @endforeach
                   </select>
                   Si no esta factura en listado agregela:
                   <a href="{{ url('/facturas_ingreso') }}" class="btn btn-warning"><span class="fa fa-plus"></span>Agregar Facturas de ingreso</a>
                </div>
                <div class="form-group">
                  <label for="laboratorio_id">Laboratorio:</label>
                   <select class="itemName form-control" style="width:550px;" tabindex="8" id="laboratorio_id" name="laboratorio_id" required="true" ></select>
                 
                 
                </div>
                 <div class="form-group">
                  <label for="laboratorio_id">Sucursal:</label>
                 <select id="sucursal" onclick="valsuc()" name="sucursal" title="seleccione sucursal de bodega" required="true" tabindex="9" class="form-control">
                      @foreach($sucursal as $s)
                  
                      <option value="{{ $s -> id}}">Cod: {{ $s -> id}} - {{ $s -> nombre}} </option>
                  
                  @endforeach
                   </select>
                </div>
               
                  <input type="hidden" class="form-control" value="0" id="registro" tabindex="10" name="registro" required="true">
                
                <div class="form-group">
                  <label for="fecha_nacimiento">Fecha de vencimiento:</label>
                  <input type="date" class="form-control" id="fecha_vencimiento" tabindex="11" name="fecha_vencimiento" required="true">
                </div>

                <div class="form-group">
                  <label for="fecha_nacimiento">generico:</label>
                  
                   <input maxlength="50" type="text" name="generico" id="generico" class="form-control">
                </div>
              
              
              <button type="submit" class="btn btn-success" ng-click="addItemB()"  tabindex="13">Agregar</button>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-window-close"></span>Cerrar</button>
          </div>
        </div>
        
      </div>
    </div>
    <!-- add code ends -->
 
     <!-- add code ends -->
  <!-- View Modal start -->
    <div class="modal fade" id="viewModal" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Detalles</h4>
          </div>
          <div class="modal-body">
            <center><span class="fa fa-camera fa-3x"></span></center>
            <p><b>Codigo de producto : </b><span id="view_codigo_producto" class="text-success"></span></p>
            <p><b>Aplica Descuento : </b><span id="view_aplica_descuento" class="text-success"></span></p>
            <p><b>Porcentaje de descuento : </b><span id="view_porcentaje_descuento" class="text-success"></span></p>
            <p><b>Nombre : </b><span id="view_nombre" class="text-success"></span></p>
             <p><b>Precio de compra : </b><span id="view_precio_compra" class="text-success"></span></p>
            <p><b>Precio de venta : </b><span id="view_precio_venta" class="text-success"></span></p>
            <p><b>Existencias : </b><span id="view_existencia" class="text-success"></span></p>
            <p><b>Factura : </b><span id="view_factura" class="text-success"></span></p>
            <p><b>Laboratorio : </b><span id="view_laboratorio_id" class="text-success"></span></p>
            <p><b>Sucursal : </b><span id="view_sucursal" class="text-success"></span></p>
            <p><b>Registro : </b><span id="view_registro" class="text-success"></span></p>
            <p><b>Fecha de vencimiento : </b><span id="view_fecha_vencimiento" class="text-success"></span></p>
            <p><b>Generico : </b><span id="view_generico" class="text-success"></span></p>
            <p><b>Observaciones : </b><span id="view_observaciones" class="text-success"></span></p>
            
            
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"></button>
          </div>
        </div>
        
      </div>
    </div>
    <!-- view modal ends -->
 
    <!-- Edit Modal start -->
    <div class="modal fade" id="editModal" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Editar</h4>
          </div>
          <div class="modal-body">
            <form action="{{ url('medicinas/update') }}" method="post">
              {{ csrf_field() }}
              <div class="form-group">
                <div class="form-group">
                  <label for="edit_codigo_producto">Codigo de Producto:</label>
                  <input type="number" class="form-control" id="edit_codigo_producto" name="edit_codigo_producto">
                </div>
                <div class="form-group">
                  <label for="edit_aplica_descuento">Aplica Descuento :</label>
                  <input type="text" class="form-control" id="edit_aplica_descuento" name="edit_aplica_descuento">
                </div>
                <div class="form-group">
                  <label for="edit_porcentaje_descuento"> % descuento :</label>
                  <input type="text" class="form-control" id="edit_porcentaje_descuento" name="edit_porcentaje_descuento">
                </div>
                <div class="form-group">
                  <label for="edit_nombre">Nombre :</label>
                  <input type="text" class="form-control" id="edit_nombre" name="edit_nombre">
                </div>
                 <div class="form-group">
                  <label for="edit_precio_venta">Precio de Venta Q.</label>
                  <input type="text" class="form-control" id="edit_precio_venta" name="edit_precio_venta">
                </div>
                 <div class="form-group">
                  <label for="edit_nombre">Precio de compra :</label>
                  <input type="text" class="form-control" id="edit_precio_compra" name="edit_precio_compra">
                </div>
                 <div class="form-group">
                  <label for="edit_existencia">Existencias: :</label>
                  <input type="text" class="form-control" id="edit_existencia" name="edit_existencia">
                </div>
                 <div class="form-group">
                  <label for="edit_nombre">Factura :</label>
                  <input type="text" class="form-control" id="edit_factura" name="edit_factura">
                </div>
                 <div class="form-group">
                  <label for="edit_laboratorio_id">Laboratorio :</label>
                  <input type="text" class="form-control" id="edit_laboratorio_id" name="edit_laboratorio_id">
                </div>
                <div class="form-group">
                  <label for="edit_sucursal_id">Sucursal :</label>
                  <input type="number" class="form-control" id="edit_sucursal" name="edit_sucursal">
                </div>
                 <div class="form-group">
                  <label for="edit_registro">Registro :</label>
                  <input type="text" class="form-control" id="edit_registro" name="edit_registro">
                </div>
                 <div class="form-group">
                  <label for="edit_nombre">Fecha de vencimiento :</label>
                  <input type="text" class="form-control" id="edit_fecha_vencimiento" name="edit_fecha_vencimiento">
                </div>
                 <div class="form-group">
                  <label for="edit_nombre">Precio de Generico :</label>
                  <input type="text" class="form-control" id="edit_generico" name="edit_generico">
                </div>
               
              </div>
              
              <button type="submit" class="btn btn-default">Modificar</button>
              <input type="hidden" id="edit_id" name="edit_id">
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>
          
        </div>
        
      </div>
    </div>
    <!-- Edit code ends -->
 
  </div>
  </div>
  </div>
  <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
<script src="http://code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
  <input type="hidden" id="search" value="{{ url('/live_search/action/') }}">

  <input type="hidden" id="autoCompleteSelect" value="{{ url('/select2-autocomplete-ajax/') }}">
  
  <script type="text/javascript">
    
      $('#addModalProd').on('shown.bs.modal', function() {
          $('#codigo_producto').focus();
          document.getElementById("porcentaje_descuento").value = 0;
        });
  

      function searchData() {
        // busqueda de data
        var nSearch = $("#nSearch").val();
        var urlVar = $("#search").val();
        var union = urlVar+"/"+nSearch;
        $.ajax({
          url: union,
          method: 'get',
          dataType: 'json',
          success: function (response) {
            console.log(response);
            $('.pagination').hide();
            var carga = response.length;
            //alert(response[0].especie + ' ' + carga);
            $("#MyTable tr").remove(); 
            $('#MyTable > tbody:last-child').append('<tr>'+
                '<th>Cod </th>'+
                '<th>Nombre</th>'+
                '<th>Precio Compra</th>'+
                '<th>Precio Venta</th>'+
                '<th>existencias</th>'+
                '<th>vencimiento </th>'+
                '<th>generico </th>'+
                '<th>Sucursal </th>'+
                '<th>Laboratorio </th>'+
                '<th>opciones</th>'+
              '</tr>');
            for (j = 0; j < carga; j++) {
              $('#MyTable > tbody:last-child').append('<tr>' +
                '<td class="border" style="line-height:12pt;"><!--D-->' + response[j].codigo_producto + '</td>' +
                '<td class="border" style="line-height:12pt;"><!--D-->' + response[j].nombre + '</td>' +
                '<td class="border" style="line-height:12pt;"><!--D-->' + response[j].precio_compra + '</td>' +
                '<td class="border" style="line-height:12pt;"><!--D-->' + response[j].precio_venta + '</td>' +
                '<td class="border" style="line-height:12pt;"><!--D-->' + response[j].existencia + '</td>' +
                '<td class="border" style="line-height:12pt;"><!--D-->' + response[j].fecha_vencimiento + '</td>' +
                '<td class="border" style="line-height:12pt;"><!--D-->' + response[j].generico + '</td>' +
                '<td class="border" style="line-height:12pt;"><!--D-->' + response[j].sucursalname + '</td>' +
                '<td class="border" style="line-height:12pt;"><!--D-->' + response[j].lab + '</td>' +
                '<td>'+
                  '<button class="btn btn-info" data-toggle="modal" data-target="#viewModal" onclick="fun_view('+response[j].id+')" title="ver"><span class="fa fa-eye"></span><strong>Ver Detalles</strong></button>'+
                '</td></tr >');
            }
            },

           error: function(response){
                 //alert("Error: "+response);
                 /*
                  <thead>
                    <tr>
                      <th>Cod </th>
                      <th>Nombre</th>
                      <th>Precio Compra</th>
                      <th>Precio Venta</th>
                      <th>existencias</th>
                      <th>vencimiento </th>
                      <th>generico </th>
                      <th>Sucursal </th>
                      <th>Laboratorio </th>
                      <th>opciones</th>
                    </tr>
                  </thead>
                  */
            }
          });
      }


      



//SELECT DE ALUMNO
      $('.itemName').select2({
        placeholder: 'Seleccione un Laboratorio',
        ajax: {
          url: '{{ url('/select2-autocomplete-ajax') }}',
          dataType: 'json',
          delay: 250,
          processResults: function (data) {
            return {
              results:  $.map(data, function (item) {
                    return {

                        text: item.nombre+' '+item.direccion,
                        id: item.id
                    }
                })
            };
          },
          cache: true
        }
      });

  $("#laboratorio_id").change(function(){
    
});




        var app = angular.module('calc', []).controller('myCtrl', function ($scope, $http) {
            // consultar todos los datos
            
       


        
                          //datos en consola
            $scope.calculo = function (i, j) {

                var n1 = parseFloat(i).toFixed(2);//venta
                var n2 = parseFloat(j).toFixed(2);//descuento

                if(!n2)
                {
                  
                  document.getElementById('porcentaje_descuento').value = '0';
                  n2=document.getElementById('porcentaje_descuento').value;
                  //alert(n2);
                }
                

                if (j > 100) {
                    document.getElementById("porcentaje_descuento").value = "";
                    document.getElementById("precio_compra").value = "";
                    alert("Porcentaje de descuento debe de ser menor al 100%");
                }
                else {

                    

                    //var pdes = (n1 * n2) / 100;
                    //var pcompr = n1 - pdes;

                    var pdes = (n2/100);
                    var pcompr = n1/((pdes)+1);


                    var pdesc = parseFloat(pdes).toFixed(2);
                    var pcompra = parseFloat(pcompr).toFixed(2);



                    document.getElementById("porcentaje_descuento").value = pdesc;
                    document.getElementById("precio_compra").value = pcompra;
                }

            }

               $scope.addItemB = function(){
                    
        

                          var usuario = "{{ Auth::user()->email }}";
                          var tipo_proceso = "CREA PRODUCTO";
                          var codigo = document.getElementById("codigo_producto").value;
                          var descripcion = codigo;

                          //alert(id);
                          $http.post("{{ url('api/bitacora') }}?usuario="+usuario+"&tipo_proceso="+tipo_proceso+"&descripcion="+descripcion);

                          console.log("agregado en log");
                          //alert("agregado en log");

                    }



             // buscar detalles de producto por id
             $scope.buscar = function (i) {

                $scope.errortext = "";
                var cod = parseInt(i);
                //servicio que busca todo de producto por id
                $http.get("{{ url('api/ventas/') }}/" + cod).then(function (response) {
                    $scope.items = response.data;
                    $scope.results = [];
                    //datos en consola
                    //console.log(response.data[0]["nombre"]);
                    console.log(response.data);

                        if(response.data.length>1)
                    {
                        alert("Producto con este codigo ya existente");
                        



                    }
                    //alert("Producto con este codigo ya existente");
                    var name = response.data[0]["nombre"];
                    var precio = response.data[0]["precio_venta"];
                    var existencia = response.data[0]["existencia"];
                    document.getElementById("precio_anterior").style.visibility='visible';
                    document.getElementById("lblprecioant").style.visibility='visible';



                    
                    document.getElementById("nombre").value = name;
                    document.getElementById("precio_anterior").value = precio;
                    document.getElementById("existencia").value = existencia;
                   
                    document.getElementById("precio_venta").focus();
 
                  });
               

                
                
              

            }




            



            });
        
        
    </script>

 <link href="{{ url('/') }}/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">

                 
                 <link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/1.10.15/css/jquery.dataTables.css">
                <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
                <script src="//code.jquery.com/jquery-2.1.3.min.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.js"></script>

                <script language="javascript" type="text/javascript">
                        /*
                                    
                    $(document).ready(function () {
                      $('#MyTable').DataTable( {
                        
                            "language": {
                                "lengthMenu": "Cantidad por pagina",
                                "zeroRecords": "Nada que mostrar",
                                "info": "Mostrando pagina _PAGE_ de _PAGES_",
                                "infoEmpty": "No records available",
                                "infoFiltered": "(filtrados from _MAX_ total records)",
                                "sSearch": "Buscar:",
                                 "dom": '<"top"Blf>rt<"bottom"ip>',
                                "buttons": ['copy', 'excel', 'csv', 'pdf', 'print'],
                                "select": true,
                            }
                        } );
                     
                       
                  } );
                  */
                </script>

  <script type="text/javascript">
    function newSearch()
    {
      var view_url = $("#hidden_new_search").val();

      var conf = confirm("Desea nueva busqueda??");
      if(conf){
        //alert(view_url);
      window.location.href = view_url;
      }
      else
      {

      }
    }
    function fun_view(id)
    {
      var view_url = $("#hidden_view").val();
      $.ajax({
        url: view_url,
        type:"GET", 
        data: {"id":id}, 
        success: function(result){
          //console.log(result);
          $("#view_codigo_producto").text(result.codigo_producto);
          $("#view_aplica_descuento").text(result.aplica_descuento);
          $("#view_porcentaje_descuento").text(result.porcentaje_descuento);
          $("#view_nombre").text(result.nombre);
          $("#view_precio_venta").text(result.precio_venta);
          $("#view_precio_compra").text(result.precio_compra);
          $("#view_existencia").text(result.existencia);
          $("#view_factura").text(result.factura);
          $("#view_laboratorio_id").text(result.lab);
          $("#view_sucursal").text(result.sucursalname);
          $("#view_registro").text(result.registro);
          $("#view_fecha_vencimiento").text(result.fecha_vencimiento);
          $("#view_generico").text(result.generico);
        }
      });
    }
 
    function fun_edit(id)
    {
      var view_url = $("#hidden_view").val();
      $.ajax({
        url: view_url,
        type:"GET", 
        data: {"id":id}, 
        success: function(result){
          //console.log(result);
          $("#edit_id").val(result.id);
          $("#edit_codigo_producto").val(result.codigo_producto);
          $("#edit_aplica_descuento").val(result.aplica_descuento);
          $("#edit_porcentaje_descuento").val(result.porcentaje_descuento);
          $("#edit_nombre").val(result.nombre);
          $("#edit_precio_venta").val(result.precio_venta);
          $("#edit_precio_compra").val(result.precio_compra);
          $("#edit_existencia").val(result.existencia);
          $("#edit_factura").val(result.factura);
          $("#edit_laboratorio_id").val(result.laboratorio_id);
          $("#edit_sucursal").val(result.sucursal);
          $("#edit_registro").val(result.registro);
          $("#edit_fecha_vencimiento").val(result.fecha_vencimiento);
          $("#edit_generico").val(result.generico);
        }
      });
    }
 
    function fun_delete(id)
    {
      var conf = confirm("Esta seguro que desea eliminar??");
      if(conf){
        var delete_url = $("#hidden_delete").val();
        $.ajax({
          url: delete_url,
          type:"POST", 
          data: {"id":id,_token: "{{ csrf_token() }}"}, 
          success: function(response){
            alert(response);
            location.reload(); 
          }
        });
      }
      else{
        return false;
      }
    }



    
  </script>


           


          </div>

          <!-- /.col-lg-12 -->

      </div>



  </div>

@endsection
