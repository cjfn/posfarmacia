<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\model\ventas\ventas;
use App\model\factura\factura;
use App\model\ventas\detalle_factura;
use App\model\medicina\medicina;

use App\model\bitacora\bitacora;
use DB;


class apiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
         $data = factura::all()->toArray();

            return response()->json($data);
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        try{
            $deletedRows = medicina::where('existencia', '<', 0)->delete();

            $data = new factura;
            $data -> total = 0.00;
            //$data -> activo = 1;
            //$data -> usuario = $request -> usuario ;
            $data -> usuario = $request -> usuario;
            $data -> save(); 
            $id = $data->id;
            $fecha = $data->fecha_created;
            return response()->json(['status'=>true, 'factura creada', 'id' => $id, 'fecha_created' => $fecha],200);
        }
        catch(Exception $e)
        {

            return response()->json(['status'=>false, 'error'],400);
        }
    }

     public function store_detalle(Request $request)
    {
        //

        try{
            $data = new detalle_factura;
            $data -> factura = $request -> id;
            $data -> codigo_producto = $request -> codigo_producto;
            $data -> cantidad = $request -> cantidad;
            $data -> subtotal = $request -> subtotal;
            $data -> save(); 

            $id = $data->id;
            return response()->json(['status'=>true, 'factura creada', 'id' => $id],200);
        }
        catch(Exception $e)
        {

            return response()->json(['status'=>false, 'error'],400);
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
          try{
      
            $info=medicina::rightjoin('sucursales as s', 'medicinas.sucursal','=','s.id')
            ->leftjoin('laboratorios as l','medicinas.laboratorio_id','=','l.id')
        
        ->select(DB::raw('DISTINCT(medicinas.id)'), 'medicinas.laboratorio_id', 'medicinas.codigo_producto', 'medicinas.nombre', 'medicinas.precio_compra', 'medicinas.precio_venta','medicinas.existencia','medicinas.fecha_vencimiento','medicinas.generico', 's.nombre as sucursal', 'l.nombre as lab')
           
            ->where('codigo_producto', 'like', $id)
            
            ->get();
            return response()->json($info);

           
             
        }
        catch(Exception $e)
        {
            return response()->json(['status'=>false, 'error'],400);    
        }

                
            
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   

    public function update(Request $request, $id)
    {
        if (!$id) {
            throw new HttpException(400, "Invalid id");
        }
        $factura = factura::find($id);
        $factura->total = $request->input('total');
       
        if ($factura->save()) {
            return $factura;
        }
        throw new HttpException(400, "Invalid data");
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
