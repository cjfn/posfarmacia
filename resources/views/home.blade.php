@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-md-offset-2">
                <div class="panel panel-primary">
                    <div class="panel-heading"><strong>MENU DE ACCESO</strong></div>

                    <div class="panel-body">
                        Bienvenido, <strong> ADMINISTRE </strong>de una manera ágil y rapida su inventario.
            <center>
                <h3>Top 10 de ventas </h3>


            </center>
            <div id="canvas-holder" class="">
            <canvas id="chart-area" ></canvas>
            <canvas id="chart-area2"></canvas>
            <br/>
             <div class="alert alert-info alert-dismissable">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>Instrucciones <span class="fa fa-warning"></span> 1 </strong> Primero: agrega los proveedores o <strong> <a href="{{url('/')}}/laboratorios"> LABORATORIOS</a>,</strong><strong> <a href="{{url('/')}}/sucursales"> SUCURSALES</a></strong> de tu empresa y crea usuarios con diferentes permisos de acceso <strong>ADMINISTRADOR, ENCARGADO O VENDEDOR</strong>.
              </div>
             <div class="alert alert-info alert-dismissable">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>Instrucciones <span class="fa fa-warning"></span> 2 </strong> Segundo: ingresa los <a href="{{url('/')}}/Productos"> medicinas</a> a tu inventario, asignales un laboratorio de origen, y una sucursal o  <strong>BODEGA</strong>.
              </div>
              <center>
                <h2>
                    <span class="fa fa-money"></span> Optimize recursos y genere ganancias para su negocio en tiempo real
           
                </h2>
              </center>
            
            </div>
          
            <center>
                 <div class="table-responsive">
                    <!--
                <img src="{{ url('/') }}/imgs/farmacia.jpg"  class="img-fluid" alt="Responsive image">-->

            </div>
            </center>
            <div class="alert alert-warning alert-dismissable">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>¡Cuidado! <span class="fa fa-warning"></span></strong> Todos los cambios que realices seran monitoreados, es por ello que debes de estar seguro antes de hacer un ingreso de mercaderia en <strong> <a href="{{url('/')}}/medicinas"> medicinas</a></strong>, realices <strong><a href="{{url('/')}}/ventas"> ventas</a></strong> o realices ajustes(si tus permisos lo permiten), 
            </div>
             
           
      
            
            <script>
                var pieData = [

                @foreach($data as $key => $dat)

                 {
                                    value:  {{ $dat->cantidad }},
                                    color: "#DC143C",
                                    highlight: "#000080",
                                    label: "Cod.: {{ $dat->codigo_producto }}, Cant"
                                },

               
                 
                @endforeach
                /*
                                {
                                    value: 16,
                                    color: "#e3e860",
                                    highlight: "#a9ad47",
                                    label: "Android"
                                },
                                {
                                    value: 11,
                                    color: "#eb5d82",
                                    highlight: "#b74865",
                                    label: "Firefox"
                                },
                                {
                                    value: 10,
                                    color: "#5ae85a",
                                    highlight: "#42a642",
                                    label: "Internet Explorer"
                                },
                                {
                                    value: 8.6,
                                    color: "#e965db",
                                    highlight: "#a6429b",
                                    label: "Safari"
                                }
                                */
                ];

              
                var ctx = document.getElementById("chart-area").getContext("2d");
                var ctx2 = document.getElementById("chart-area2").getContext("2d");
                
                window.myPie = new Chart(ctx).Pie(pieData);
                window.myPie = new Chart(ctx2).Doughnut(pieData);
               
            </script>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
