
@extends('layouts.app')



@section('content')

<meta name="csrf-token" content="{{ csrf_token() }}" />



<div class="container">
    <div class="row">
 
        <div class="col-md-12">

             
    <div class="panel panel-primary">
                <div class="panel-heading"><strong>TUS USUARIOS</strong>
                </div>
    <div class="panel-body">
    <h2>Administra los usuarios que tienen acceso a tu plataforma</h2>
    @if ($message = Session::get('success'))

      <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
      </div>
    @endif
    <div class="table-responsive">
    <table class="table table-bordered" id="MyTable">
    <center>
       <a href="{{ URL::previous() }}"><span class="fa fa-mail-reply"></span>Atras</a>

        @if(Auth::user()->nombre_tipo_usuario=='ADMIN')
                     <button type="button" class="btn btn-success" data-toggle="modal" data-target="#addModal"><span class="fa fa-plus"></span>Agregar</button>
                    @else

                    @endif
  
    </center>
      <thead>
        <tr>
          <th>Nombre</th>
          <th>Email</th>
          <th>password</th>
          <th>tipo_usuario</th>
          <th>Opciones</th>
        </tr>
      </thead>
      <tbody>
      @foreach($data as $x)
        <tr>
          <td>{{$x -> name}}</td>
          <td>{{$x -> email}}</td>
          <td>{{$x -> password}}</td>
          <td>{{$x -> nombre_tipo_usuario}}</td>

          <td>
                    @if(Auth::user()->nombre_tipo_usuario=='ADMIN')
                     <button class="btn btn-info" data-toggle="modal" data-target="#viewModal" onclick="fun_view('{{$x -> id}}')"><span class="fa fa-eye"></span>Ver</button>
                      <button class="btn btn-warning" data-toggle="modal" data-target="#editModal" onclick="fun_edit('{{$x -> id}}')"><span class="fa fa-pencil-square"></span>Editar</button>
                      <button class="btn btn-danger" onclick="fun_delete('{{$x -> id}}')"><span class="fa fa-times-circle"></span>Eliminar</button>
                    @else

                    @endif
  
              
          </td>
        </tr>
       @endforeach
      </tbody>
    </table>
  </div>
    <input type="hidden" name="hidden_view" id="hidden_view" value="{{url('usuariosABC123DEF456usuariosABC123DEF456/view')}}">
    <input type="hidden" name="hidden_delete" id="hidden_delete" value="{{url('usuariosABC123DEF456usuariosABC123DEF456/delete')}}">
    <!-- Add Modal start -->
    <div class="modal fade" id="addModal" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Agregar nuevo Usuario</h4>
          </div>
          <div class="modal-body">
            <form action="{{ url('usuariosABC123DEF456usuariosABC123DEF456') }}" method="post">
              {{ csrf_field() }}
              <div class="form-group">
                <div class="form-group">
                  <label for="first_name">Nombre:</label>
                  <input type="text" class="form-control" id="name" name="name" required="true">
                </div>
                <div class="form-group">
                  <label for="last_name">Email:</label>
                  <input type="text" class="form-control" id="email" name="email" required="true">
                </div>
                <div class="form-group">
                  <label for="last_name">Password:</label>
                  <input type="password" class="form-control" id="password" name="password" required="true">
                </div>
                <div class="form-group">
                  <label for="last_name">Tipo de Usuario:</label>
                  
                  <select id="nombre_tipo_usuario" name="nombre_tipo_usuario" class="form-control">
                    <option value="ADMIN">ADMIN</option>
                    <option value="ENCARGADO">ENCARGADO</option>
                    <option value="VENDEDOR">VENDEDOR</option>
                  </select>
                </div>

                 <div class="form-group">
                  <label for="last_name">Sucursal:</label>
                  
                 

                     <select id="sucursal" title="seleccione sucursal"  name="sucursal" required="true" class="form-control">
                      @foreach($sucursal as $l)
                  
                      <option value="{{ $l -> id}}"> {{ $l -> nombre}} </option>
                  
                  @endforeach
                   </select>
                </div>
              </div>
              
              <button type="submit" class="btn btn-default"><span class="fa fa-plus"></span>Agregar</button>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-window-close"></span>Cerrar</button>
          </div>
        </div>
        
      </div>
    </div>
    <!-- add code ends -->
 
    <!-- View Modal start -->
    <div class="modal fade" id="viewModal" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Detalles</h4>
          </div>
          <div class="modal-body">
            <p><b>Nombre : </b><span id="view_name" class="text-success"></span></p>
            <p><b>Email : </b><span id="view_email" class="text-success"></span></p>
            <p><b>Password : </b><span id="view_password" class="text-success"></span></p>

            <p><b>Sucursal : </b><span id="view_sucursal" class="text-success"></span></p>
            <p><b>Tipo de usuario : </b><span id="view_nombre_tipo_usuario" class="text-success"></span></p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"></button>
          </div>
        </div>
        
      </div>
    </div>
    <!-- view modal ends -->
 
    <!-- Edit Modal start -->
    <div class="modal fade" id="editModal" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Editar</h4>
          </div>
          <div class="modal-body">
            <form action="{{ url('usuariosABC123DEF456usuariosABC123DEF456/update') }}" method="post">
              {{ csrf_field() }}
              <div class="form-group">
                <div class="form-group">
                  <label for="edit_nombre">Nombre:</label>
                  <input type="text" class="form-control" id="edit_name" name="edit_name">
                </div>
                <div class="form-group">
                  <label for="edit_descripcion">Email :</label>
                  <input type="text" class="form-control" id="edit_email" name="edit_email">
                </div>
                <div class="form-group">
                  <label for="edit_telefono">Password :</label>
                  <input type="password" class="form-control" id="edit_password" name="edit_password">
                
                <div class="form-group">
                  <label for="edit_correo">Tipo de Usuario :</label>

                  <select id="edit_tipo_usuario" name="edit_tipo_usuario" class="form-control">
                    <option value="ADMIN">ADMIN</option>
                    <option value="ENCARGADO">ENCARGADO</option>
                    <option value="VENDEDOR">VENDEDOR</option>
                  </select>
                </div

                <div class="form-group">
                  <label for="last_name">Sucursal:</label>
                  
                 

                     <select id="edit_sucursal" title="seleccione sucursal"  name="edit_sucursal" required="true" class="form-control">
                      @foreach($sucursal as $l)
                  
                      <option value="{{ $l -> id}}"> {{ $l -> nombre}} </option>
                  
                  @endforeach
                   </select>
                </div>
               
              </div>
              
              <button type="submit" class="btn btn-default">Modificar</button>
              <input type="hidden" id="edit_id" name="edit_id">
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>
          
        </div>
        
      </div>
    </div>
    <!-- Edit code ends -->
 
  </div>
  </div>
  </div>

 <link href="{{ url('/') }}/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">

                 
                 <link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/1.10.15/css/jquery.dataTables.css">
                <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
                <script src="//code.jquery.com/jquery-2.1.3.min.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.js"></script>

                <script language="javascript" type="text/javascript">
                                    
                    $(document).ready(function () {
                      $('#MyTable').DataTable( {
                        
                            "language": {
                                "lengthMenu": "Cantidad por pagina",
                                "zeroRecords": "Nada que mostrar",
                                "info": "Mostrando pagina _PAGE_ de _PAGES_",
                                "infoEmpty": "No records available",
                                "infoFiltered": "(filtrados from _MAX_ total records)",
                                "sSearch": "Buscar:",
                                 "dom": '<"top"Blf>rt<"bottom"ip>',
                                "buttons": ['copy', 'excel', 'csv', 'pdf', 'print'],
                                "select": true,
                            }
                        } );

                       $('#addModal').on('shown.bs.modal', function() {
                        $('#name').focus();
                      });
                  } );
                </script>

  <script type="text/javascript">
    function fun_view(id)
    {
      var view_url = $("#hidden_view").val();
      $.ajax({
        url: view_url,
        type:"GET", 
        data: {"id":id}, 
        success: function(result){
          //console.log(result);
          $("#view_name").text(result.name);
          $("#view_email").text(result.email);
          $("#view_password").text(result.password);
          $("#view_sucursal").text(result.sucursal);
          $("#view_nombre_tipo_usuario").text(result.nombre_tipo_usuario);
        }
      });
    }
 
    function fun_edit(id)
    {
      var view_url = $("#hidden_view").val();
      $.ajax({
        url: view_url,
        type:"GET", 
        data: {"id":id}, 
        success: function(result){
          //console.log(result);
          $("#edit_id").val(result.id);
          $("#edit_name").val(result.name);
          $("#edit_email").val(result.email);
          $("#edit_password").val(result.password);
          $("#edit_sucursal").val(result.sucursal);
          $("#edit_tipo_usuario").val(result.nombre_tipo_usuario);
        }
      });
    }
 
    function fun_delete(id)
    {
      var conf = confirm("Esta seguro que desea eliminar??");
      if(conf){
        var delete_url = $("#hidden_delete").val();
        $.ajax({
          url: delete_url,
          type:"POST", 
          data: {"id":id,_token: "{{ csrf_token() }}"}, 
          success: function(response){
            alert(response);
            location.reload(); 
          }
        });
      }
      else{
        return false;
      }
    }
  </script>


           


          </div>

          <!-- /.col-lg-12 -->

      </div>



  </div>

@endsection

