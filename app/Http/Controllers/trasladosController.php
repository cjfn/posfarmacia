<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\medicina\medicina;
use App\model\laboratorio\laboratorio;
use App\model\sucursal\sucursal;
use App\model\traslados\traslados;
use App\User;
use DB;
use Illuminate\Support\Facades\Input;
use Session;
use DOMDocument;
use Auth;   
class trasladosController extends Controller
{
    //
    public function index()
    {
         $data = medicina::join('sucursales as s', 'medicinas.sucursal','=','s.id')
        ->join('laboratorios as l','medicinas.laboratorio_id','=','l.id')
        
        ->select(DB::raw('DISTINCT(medicinas.id)'), 'medicinas.codigo_producto', 'medicinas.nombre', 'medicinas.precio_compra', 'medicinas.precio_venta','medicinas.existencia','medicinas.fecha_vencimiento','medicinas.generico', 's.nombre as sucursal', 'l.nombre as lab')
       ->where('medicinas.id','!=', Auth::user()->sucursal)
        ->get();
         $lab = laboratorio::where('activo','=', '1')->get();
         $user = User::where('activo','=', '1')->get();
         //$sucursal = sucursal::where('activo','=', '1')->get();
         $sucursal = sucursal::where('id','!=', Auth::user()->sucursal)->get();
         //traslados = traslados::all();

         $traslados = traslados::join('sucursales as s','traslados.sucursal_destino','=','s.id')

         

         ->select(DB::raw('DISTINCT(traslados.id)'),'traslados.id','traslados.producto_codigo','traslados.sucursal_origen','traslados.sucursal_destino','traslados.usuario','traslados.estado','traslados.fecha_created','traslados.fecha_updated','traslados.activo','traslados.cantidad','traslados.usuario_created','traslados.OBSERVACIONES','s.nombre as bodega')
         ->orderBy('traslados.id', 'DESC')
         ->paginate(10);

         
            return view('traslados.index',['data'=>$traslados,'medicina'=>$data, 'user'=>$user, 'sucursal'=>$sucursal]);
    }

   public function add(Request $request)
        {


            $data = new traslados;
            $data -> producto_codigo = $request -> producto_codigo;

            $fecha_ven = $request -> fecha_vence;
            $suc_or = medicina::where('codigo_producto', $request -> producto_codigo)
            ->where('fecha_vencimiento','=',$fecha_ven)
            ->first();
            $data -> sucursal_origen = $suc_or->sucursal;
            $data -> sucursal_destino = $request -> sucursal_destino;
            $data -> estado = 'BODEGA TRANSITORIA';
            $data -> usuario = $request -> usuario;
            $data -> cantidad = $request -> cantidad;
            $data -> usuario_created = Auth::user()->email;
            $data -> OBSERVACIONES = $request -> OBSERVACIONES;

            $data -> fecha_vencimiento = $fecha_ven;
    
        // nueva existencia actualiza origen
            $nueva_existencia = $suc_or -> existencia - $data -> cantidad;
            $suc_or -> existencia = $nueva_existencia;
            $suc_or -> save();           

            $data -> save();
            return back()
                    ->with('success','Traslado agregado exitosamente.');
        }

        /*
         * View data
         */

        public function show(Request $request)
        {
            if($request->ajax()){
                $id = $request->id;
                $info = traslados::join('sucursales as s','traslados.sucursal_destino','=','s.id')
        

         
         ->select('traslados.id','traslados.producto_codigo','traslados.sucursal_origen','traslados.sucursal_destino','traslados.usuario','traslados.fecha_vencimiento','traslados.estado','traslados.fecha_created','traslados.fecha_updated','traslados.activo','traslados.cantidad','traslados.OBSERVACIONES','s.nombre as bodega')
         ->where('traslados.id',$id)
         ->first();

                /*
                $info=DB::table('medicina as m')
            ->join('laboratorio as l','m.laboratorio_id','=','l.id')
            ->join('sucursal as s','m.sucursal','=','s.id')
             ->where('m.id','=',$id)
             ->first();
             */
                //echo json_decode($info);
                return response()->json($info);
            }
        }

        public function getTraslados(Request $request)
        {
            if($request->ajax()){
                
                $info = traslados::orderBy('id', 'DESC')->get();
                //echo json_decode($info);
                return response()->json($info);
            }
        }

         /*
        *   Update data
        */
        public function update(Request $request)
        {


            $id = $request -> edit_id;
            $data = traslados::find($id);
            $origen = medicina::where('codigo_producto','=', $data -> producto_codigo)
            ->where('fecha_vencimiento','=',$data->fecha_vencimiento)
            ->first();
            //$data -> sucursal_origen = $origen ->   sucursal;
            $data -> sucursal_destino = $request -> edit_sucursal_destino;
            $data -> estado = $request -> edit_estado;
            $data -> OBSERVACIONES = $request -> edit_OBSERVACIONES;
            $data -> fecha_updated = date('Y-m-d H:i:s');
            $data -> usuario = $request -> edit_usuario;
            
            $data -> save();


            // agregar existencias a nuevo inventario en nueva sucursal

            if($request -> edit_estado=='RECIBIDO')// si el estado es igual a
            {
                $exist_nsucursal = medicina::where('codigo_producto','=', $data -> producto_codigo)
                ->where('fecha_vencimiento','=',$data->fecha_vencimiento)
                

                    ->where('sucursal','=',$data -> sucursal_destino)
                    ->count();// si existe producto en nueva sucursal
                if($exist_nsucursal>=1)// si existen
                {
                    $suc_dest = medicina::where('codigo_producto', $data -> producto_codigo)
                    ->where('fecha_vencimiento','=',$data->fecha_vencimiento)
                    ->where('sucursal','=',$data -> sucursal_destino)
                    ->first();
                    //restar nueva existencia
                    $nueva_existencia = $suc_dest -> existencia + $data -> cantidad;
                    $suc_dest -> existencia = $nueva_existencia;
                    $suc_dest -> save();
                }
                else
                {
                     $suc_orig = medicina::where('codigo_producto', $data -> producto_codigo)
                    ->where('fecha_vencimiento','=',$data->fecha_vencimiento)
                    ->where('sucursal','=',$data -> sucursal_origen)
                    ->first();
                    $datanproducto = new medicina;
                    $datanproducto -> codigo_producto = $suc_orig -> codigo_producto;
                    $datanproducto -> aplica_descuento = $suc_orig -> aplica_descuento;
                    $datanproducto -> porcentaje_descuento = $suc_orig -> descuento;
                    $datanproducto -> nombre = $suc_orig -> nombre;
                    $datanproducto -> precio_compra = $suc_orig -> precio_compra;
                    $datanproducto -> precio_venta = $suc_orig -> precio_venta;
                    $datanproducto -> existencia = $data -> cantidad;
                    $datanproducto -> factura = $suc_orig -> factura;
                    $datanproducto -> porcentaje_descuento = $suc_orig -> porcentaje_descuento;
                    $datanproducto -> laboratorio_id = $suc_orig -> laboratorio_id;
                    $datanproducto -> sucursal = $data -> sucursal_destino;
                    $datanproducto -> usuario = Auth::user()->email ;
                    $datanproducto -> codigo_producto = $suc_orig->codigo_producto;
                    $datanproducto -> fecha_vencimiento = $suc_orig -> fecha_vencimiento;
                    $nuevoidunico = $suc_orig -> codigo_producto.$data -> fecha_vencimiento.$data ->sucursal_destino;
                    $datanproducto -> idunico = $nuevoidunico;
                    $datanproducto -> save();
                }
            }
 return back()
                    ->with('finalizado','Datos modificados exitosamente.');
        }
}
