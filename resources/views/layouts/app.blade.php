    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Farmacia la Salud Zacapa</title>

        <!-- Styles -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">
        <!-- Styles --><!-- Styles -->
        <link rel="stylesheet" href="{{ url('/')}}/css/bootstrap.min.css">
        <!-- Styles -->
        <link rel="stylesheet" href="{{ url('/')}}/css/font-awesome.min.css">
         <link href="{{ url('/') }}/css/bootstrap.css" rel="stylesheet" type="text/css">
        <!-- Custom Fonts -->
        <link href="{{ url('/') }}/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!-- Mis Estilos CSS-->
        <link href="{{ url('/') }}/css/estilos.css" rel="stylesheet" type="text/css">
        <link rel="shortcut icon" type="image/x-icon" href="{{ url('/') }}/favicon.ico" />
          

        <script src="{{ url('/') }}/js/Chart.js"></script>
        <script src="{{ url('/') }}/js/angular.min.js"></script>
        <!-- Scripts -->
        <script>
            window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
            ]); ?>
        </script>
        <style type="text/css">
            #dvLoading
            {
               background: url("{{ url('/') }}/imgs/loading.gif") no-repeat center center;
               height: 300px;
               width: 300px;
               position: fixed;
               z-index: 1000;
               left: 50%;
               top: 50%;
               margin: -25px 0 0 -25px;

            }
        </style>
        <script type="text/javascript">
            function myfocus() {

                $(window).load(function(){
                  $('#dvLoading').fadeOut(2000);
                  document.getElementById("#dvLoading").style.display = none;
                });

                //alert("hola lo estoy chingando");
         var meses = new Array ("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
        var f=new Date();
        //alert(f.getDate() + " de " + meses[f.getMonth()] + " de " + f.getFullYear());

        document.getElementById("nowdate").innerHTML = f.getDate() + "/" + [f.getMonth()] + "/" + f.getFullYear();
         document.getElementById("codigo_producto").focus();
         document.getElementById("btnaddprod").focus();
         document.getElementById("addNFact").focus();

         

         
    }
        </script>
    </head>
    <body onload="myfocus()">

        <div id="app">
              <nav class="navbar navbar-default navbar-static-top">
                <div class="container">
                    <div class="navbar-header">

                        <!-- Collapsed Hamburger -->
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                            <span class="sr-only">Toggle Navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <!-- Branding Image -->
                        <H4>FARMACIAS LA SALUD ZACAPA - fecha: <span title="bienvenido a su sitio, almacene inventario y mantenga el control de sus ventas, y muchas cosas mas..." id="nowdate" name="nowdate"></span></H4>
                    </div>

                    <div class="collapse navbar-collapse" id="app-navbar-collapse">
                        <!-- Left Side Of Navbar -->
                        <ul class="nav navbar-nav">
                            &nbsp;
                        </ul>

                        <!-- Right Side Of Navbar -->
                        <ul class="nav navbar-nav navbar-right">
                            <!-- Authentication Links -->
                            @if (Auth::guest())
                                <li><a href="{{ url('/login') }}">Login</a></li>
                                <li><a href="{{ url('/register') }}">Registro</a></li>
                            @else
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                        <strong>Usuario {{ Auth::user()->name }} <span class="caret"></span></strong>
                                    </a>

                                    <ul class="dropdown-menu" role="menu">
                                        <li>
                                            <a href="{{ url('/logout') }}"
                                                onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                                Logout
                                            </a>

                                            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                                {{ csrf_field() }}
                                            </form>
                                        </li>
                                    </ul>
                                </li>
                            @endif
                        </ul>
                    </div>
                </div>
            </nav>
            <div style="visibility: hidden;">
        {{$año = date('Y')}}
                            {{$mes = date('m')}}
                            {{$dia = date('d')}}
                        </div>


    @if(Auth::user()->nombre_tipo_usuario=='ADMIN')


    <nav class="navbar navbar-inverse sidebar" role="navigation">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-sidebar-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">ADMINISTRADOR {{ Auth::user()->name }} </a>
                <center>
                    <a href="#">
                    <strong> </strong>
                </a>
                </center>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-sidebar-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="{{ url('/home') }}">Home<span class="glyphicon glyphicon-home"></a></li>
                     <li class="dropdown">
                        <a href="#"  title="ingrese o administre nuevas actividades en su inventario">Ingresos <span class="caret"></span><span style="font-size:16px;"></span></a>
                        <ul class="menu" role="menu">
                            <li><a href="{{ url('/laboratorios') }}" title="ingrese laboratorios">Laboratorios</a></li>
                            <li><a href="{{ url('/medicinas') }}" title="ingrese nuevos productos a su inventario">Medicinas</a></li>
                            <li><a href="{{ url('/sucursales') }}" title="ingrese nuevas sucursales de su empresa para asignarles productos">sucursales</a></li>
                             <li><a href="{{ url('/facturas_ingreso') }}" title="ingrese nuevas facturas con ingreso de nuevo producto">Facturas</a></li>
                             <li><a href="{{ url('/misventas') }}" title="ingrese nuevos pedidos o facturas de ventas">Mis Ventas</a></li>
                        </ul>
                    </li>
                    <li ><a href="{{ url('/ventas') }}" title="cree nuevos pedidos y facture sus pedidos">Ventas<span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-usd"></span></a></li>

                    <li ><a href="{{ url('/ajustes') }}" title="Realice movimientos a sus productos">Ajustes<span style="font-size:16px;" class="pull-right hidden-xs showopacity fa fa-cogs"></span></a></li>

                    <li ><a href="{{ url('/traslados') }}" title="Realice movimientos a sus productos">Traslados<span style="font-size:16px;" class="pull-right hidden-xs showopacity fa fa-random"></span></a></li>
                   <li>

                    <li ><a href="{{ url('/reporte') }}" title="Cree reportes por fechas de sus ventas, ingresos o ajustes">Menu Reportes<span style="font-size:16px;" class="pull-right hidden-xs showopacity   fa fa-file-pdf-o"></span></a></li>
                   <li>
                 
                 
                     <li class="active"><a href="{{ url('/usuariosABC123DEF456usuariosABC123DEF456') }}">Usuarios<span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-user"></span></a></li>
                     

                    
                </ul>
            </div>
        </div>
    </nav>
                            



    @elseif(Auth::user()->nombre_tipo_usuario=='VENDEDOR')

    <nav class="navbar navbar-inverse sidebar" role="navigation">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-sidebar-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">VENDEDOR {{ Auth::user()->name }} </a>
                <center>
                    <a href="#">
                    <strong> </strong>
                </a>
                </center>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-sidebar-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="{{ url('/home') }}">Home<span class="glyphicon glyphicon-home"></a></li>
                     <li class="dropdown">
                        <a href="#"  title="ingrese o administre nuevas actividades en su inventario">Ingresos <span class="caret"></span><span style="font-size:16px;"></span></a>
                        <ul class="menu" role="menu">
                            <li><a href="{{ url('/laboratorios') }}" title="ingrese laboratorios"> Laboratorios</a></li>
                            <li><a href="{{ url('/medicinas') }}" title="ingrese nuevos productos a su inventario">Medicinas</a></li>
                           
                             <li><a href="{{ url('/misventas') }}" title="ingrese nuevos pedidos o facturas de ventas">Mis Ventas</a></li>
                        </ul>
                    </li>
                   

                   
                   
                    
                     

                    
                </ul>
            </div>
        </div>
    </nav>



    @elseif(Auth::user()->nombre_tipo_usuario=='ENCARGADO')

    <nav class="navbar navbar-inverse sidebar" role="navigation">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-sidebar-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">ENCARGADO {{ Auth::user()->name }} </a>
                <center>
                    <a href="#">
                    <strong> </strong>
                </a>
                </center>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-sidebar-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="{{ url('/home') }}">Home<span class="glyphicon glyphicon-home"></a></li>
                     <li class="dropdown">
                        <a href="#"  title="ingrese o administre nuevas actividades en su inventario">Ingresos <span class="caret"></span><span style="font-size:16px;"></span></a>
                        <ul class="menu" role="menu">
                            <li><a href="{{ url('/laboratorios') }}" title="ingrese laboratorios"> Laboratorios</a></li>
                            <li><a href="{{ url('/medicinas') }}" title="ingrese nuevos productos a su inventario">Medicinas</a></li>
                            <li><a href="{{ url('/sucursales') }}" title="ingrese nuevas sucursales de su empresa para asignarles productos">Sucursales</a></li>
                             <li><a href="{{ url('/facturas_ingreso') }}" title="ingrese nuevas facturas con ingreso de nuevo producto">Facturas</a></li>
                             <li><a href="{{ url('/misventas') }}" title="ingrese nuevos pedidos o facturas de ventas">Mis Ventas</a></li>
                        </ul>
                    </li>
                    <li ><a href="{{ url('/ventas') }}" title="cree nuevos pedidos y facture sus pedidos">Ventas<span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-usd"></span></a></li>

                    <li ><a href="{{ url('/ajustes') }}" title="Realice movimientos a sus productos">Ajustes<span style="font-size:16px;" class="pull-right hidden-xs showopacity fa fa-cogs"></span></a></li>

                    <li ><a href="{{ url('/traslados') }}" title="Realice movimientos a sus productos">Traslados<span style="font-size:16px;" class="pull-right hidden-xs showopacity fa fa-random"></span></a></li>
                   
                <li ><a href="{{ url('/reporte') }}" title="Cree reportes por fechas de sus ventas, ingresos o ajustes">Menu Reportes</a>
                   </li>
          

                    
                </ul>
            </div>
        </div>
    </nav>


    @endif



    <div class="main">

        <div id="dvLoading" class="container"></div>
         @yield('content')
    </div>
           
        </div>

        <!-- Scripts -->

        

        <script type="text/javascript" language="javascript" src="{{ url('/') }}/Datatables/media/js/jquery.dataTables.js">
     <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js"></script>
      


        <script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.12.4.js">
        </script>
     

        <script type="text/javascript" language="javascript" class="init"></script>

       
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}

        <script src="{{ url('/') }}//js/Chart.bundle.min.js"></script>
        
        <script src="{{ url('/') }}//js/Chart.min.js"></script>
      

        <script src="{{ url('/') }}//js/myscript.js"></script>


        </script>
        </script>
        <script type="text/javascript" language="javascript" class="init"></script>

       

        <script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>


    </body>
    </html>
