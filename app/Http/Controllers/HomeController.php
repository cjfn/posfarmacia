<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DB;

use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $data = DB::table("detalle_facturas")->select(DB::raw("SUM(cantidad) as cantidad"),"codigo_producto")
        ->groupBy("codigo_producto")
        ->orderBy("cantidad", "DESC")
        ->take(10)
        ->get();

        $datainverse = DB::table("detalle_facturas")->select(DB::raw("SUM(cantidad) as cantidad"),"codigo_producto")
        ->groupBy("codigo_producto")
        ->orderBy("cantidad", "ASC")
        ->take(5)
        ->get();

         $user = Auth::user()->nombre_tipo_usuario;
         $activo = Auth::user()->activo;
         //ADMIN, ENCARGADO, VENDEDOR
         if($activo==1)
         {
             if($user=='ADMIN')
             {
                return view('home',['data'=>$data,'datainverse'=>$datainverse]);
             }

             if($user=='VENDEDOR')
             {
                return view('homevendedor');
             }
             if($user=='ENCARGADO')
             {
                return view('homeencargado');
             }
         }
         else
         {
            return view('nowelcome');
         }
        
        
        
    }
}
