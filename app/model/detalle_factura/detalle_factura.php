<?php

namespace App\model\detalle_factura;

use Illuminate\Database\Eloquent\Model;

class detalle_factura extends Model
{
    //
     protected $table = 'detalle_facturas';
	 protected $primarykey='id';
	 public $timestamps = false;
    
    protected $fillable = [
        'factura', 'codigo_producto', 'cantidad', 'subtotal', 'fecha_created', 'fecha_updated','fecha_vencimiento', 'usuario'
    ];
}