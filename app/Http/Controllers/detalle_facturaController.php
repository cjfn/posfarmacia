<?php

namespace App\Http\Controllers;

use App\model\medicina\medicina;
use App\model\laboratorio\laboratorio;
use App\model\sucursal\sucursal;
use App\model\ventas\ventas;
use App\model\factura\factura;
use App\model\detalle_factura\detalle_factura;
use DB;

use Auth;

use Illuminate\Http\Request;

class detalle_facturaController extends Controller
{
    
      public function index()
    {

        $deletedRows = medicina::where('existencia', '<', 0)->delete();
         $data = medicina::all();

         $lab = laboratorio::all();
         $sucursal = sucursal::all();
         $factura = factura::last();
         
            return view('detalle_factura.index',['data'=>$data,'lab'=>$lab,'sucursal'=>$sucursal, 'id'=>$id]);
    }

         public function create(Request $request)
  {


    /*
  		$data = new factura;
            $data -> total = 0.0;
            $data -> save();
        $id = $data -> id;
        */
        $detalle_factura = detalle_factura::all();

      $factura = factura::where('id',$requestid)
      //->select('total')
      ->first();


        

        return view('factura.create',['data'=>$detalle_factura,/*'id'=>$id,*/ 'detalle_factura'=>$detalle_factura]);
  }

   public function add(Request $request)
        {

          
            $data = new detalle_factura;
            $data -> factura = $request -> id;
            $data -> codigo_producto = $request -> codigo_producto;
            $data -> cantidad = $request -> cantidad;
            $data -> subtotal = $request -> subtotal;
            $data -> usuario = Auth::user()->email ;
            $data -> save();
            $deletedRows = medicina::where('existencia', '<', 0)->delete();
            return back()
                    ->with('success','Producto agregado exitosamente.');
        }

      public function show($id)
    {
        //
        
            //$data = detalle_factura::all();
      

      //$data = detalle_factura::findbyid($id);

     /* $data = detalle_factura::where('factura',$id)
      ->get();
*/
      $data2 = medicina::all();
      /*
      SELECT DISTINCT(m.idunico), m.codigo_producto, m.nombre, df.id, df.cantidad, df.fecha_created, df.subtotal FROM detalle_facturas as df join medicinas as m on df.idunico=m.idunico where df.factura=1238
      */
         
     
      $data = DB::table("detalle_facturas as df")
        ->join ("medicinas as m","df.idunico", "=","m.idunico")
        ->where("df.factura","=", $id)
        
        ->select(DB::raw("DISTINCT(m.idunico)"), "m.codigo_producto", "m.porcentaje_descuento", "m.nombre", "m.precio_venta", "df.id","df.cantidad", "df.fecha_created", "df.subtotal")
        ->get();

// cambios hoy

$count = detalle_factura::where('factura',$id)->count();



        $sucursal = sucursal::where('id',Auth::user()->sucursal)->first();


      $factura = factura::where('id',$id)
      //->select('total')
      ->first();



/*
       $data= detalle_factura::join ('medicinas as m','detalle_facturas.codigo_producto','=','m.id')
        ->where('detalle_facturas.factura','=',$id)
        ->get();

           
  */       
           
         return view('detalle_factura.show',['data'=>$data, 'data2'=>$data2, 'id'=>$id, 'factura'=>$factura, 'sucursal'=>$sucursal,'count'=>$count]);     

          
          // ->Orwhere('a.ciclo','LIKE', '%'.$query2.'%')
         
       
    }

     public function delete(Request $request)
        {
            $id = $request -> id;
            $data = detalle_factura::find($id);
            $medicina = medicina::find($id);

            $nuevo_total = ($medicina->existencia)-($data->cantidad);
            $medicina -> existencia=$nuevo_total;

            $medicina -> update();
            //$response = $data -> delete();
            $data -> activo = false;
            $response = $data -> update();
            if($response)
                echo "Detalle de factura modificado exitosamente.";
            else
                echo "There was a problem. Please try again later.";
        }

}
