<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\medicina\medicina;
use App\model\laboratorio\laboratorio;
use App\model\sucursal\sucursal;
use App\model\facturas_ingreso\facturas_ingreso;
use DB;
use Auth;

use Illuminate\Support\Facades\Input;
use Session;
use DOMDocument;

class medicinaController extends Controller
{
    //
     public function index()
    {
         //$data = medicina::orderBy('id', 'DESC')->get();

        $data = medicina::join('sucursales as s', 'medicinas.sucursal','=','s.id')
        ->join('laboratorios as l','medicinas.laboratorio_id','=','l.id')
        
        ->select(DB::raw('DISTINCT(medicinas.id)'), 'medicinas.codigo_producto','medicinas.aplica_descuento','medicinas.factura','medicinas.porcentaje_descuento','medicinas.nombre', 'medicinas.precio_compra', 'medicinas.precio_venta','medicinas.laboratorio_id','medicinas.sucursal','medicinas.observaciones','medicinas.existencia','medicinas.fecha_vencimiento','medicinas.generico', 's.nombre as sucursalname', 'l.nombre as lab')
        ->orderBy('medicinas.id','ASC')
        //->where('medicinas.existencia','>',0)
        ->paginate(10);
        //->get();
         $fac_ing = facturas_ingreso::all();
         $lab = laboratorio::where('activo','=', '1')->get();
         $sucursal = sucursal::where('activo','=', '1')->get();

         //$users = DB::table('users')->paginate(10);

            return view('medicina.index',['data'=>$data,'lab'=>$lab,'sucursal'=>$sucursal, 'fac_ing' => $fac_ing]);
    }

   public function add(Request $request)
        {
            $data = new medicina;
            $data -> codigo_producto = $request -> codigo_producto;
            $data -> aplica_descuento = $request -> aplica_descuento;
            $data -> porcentaje_descuento = $request -> descuento;
            $data -> nombre = $request -> nombre;
            $data -> precio_compra = $request -> precio_compra;
            $data -> precio_venta = $request -> precio_venta;
            $data -> existencia = $request -> existencia;
            $data -> factura = $request -> factura;
            $data -> laboratorio_id = $request -> laboratorio_id;
            $data -> registro = $request -> registro;
            $data -> fecha_vencimiento = $request -> fecha_vencimiento;
            $data -> sucursal = $request -> sucursal;
            $data -> generico = $request -> generico;
            $data -> usuario = Auth::user()->email ;
            $codunico = ($request -> codigo_producto.$request -> fecha_vencimiento.$request -> sucursal);
            $data -> idunico = $codunico;

            $find= medicina::where('idunico',  $codunico)->count();

            if($find!=0)
            {
                return back()
                    ->with('danger','Producto ya existente.');
            }
            else
            {
                 $data -> save();
            return back()
                    ->with('success','Producto agregado exitosamente.');
            }

            
           
        }

        /*
         * View data
         */
        public function view(Request $request)
        {
            if($request->ajax()){
                $id = $request->id;
                //$info = medicina::find($id);
                $info = medicina::join('sucursales as s', 'medicinas.sucursal','=','s.id')
        ->join('laboratorios as l','medicinas.laboratorio_id','=','l.id')
        
        ->where('medicinas.id','=', $id)
         
        ->select(DB::raw('DISTINCT(medicinas.id)'), 'medicinas.codigo_producto','medicinas.aplica_descuento','medicinas.factura','medicinas.porcentaje_descuento','medicinas.nombre', 'medicinas.precio_compra', 'medicinas.precio_venta','medicinas.laboratorio_id','medicinas.sucursal','medicinas.observaciones','medicinas.existencia','medicinas.fecha_vencimiento','medicinas.generico', 's.nombre as sucursalname', 'l.nombre as lab')
        ->first();


                

                /*
                $info=DB::table('medicina as m')
            ->join('laboratorio as l','m.laboratorio_id','=','l.id')
            ->join('sucursal as s','m.sucursal','=','s.id')
             ->where('m.id','=',$id)
             ->first();
             */
                //echo json_decode($info);
                return response()->json($info);
            }
        }

        public function getlaboratorios(Request $request)
        {
            if($request->ajax()){
                
                $info = medicina::orderBy('id', 'DESC')->get();
                //echo json_decode($info);
                return response()->json($info);
            }
        }

         /*
        *   Update data
        */
        public function update(Request $request)
        {
            $id = $request -> edit_id;
            $data = medicina::find($id);
            $data -> codigo_producto = $request -> edit_codigo_producto;
            $data -> aplica_descuento = 1;
            $data -> porcentaje_descuento = $request -> edit_porcentaje_descuento;
            $data -> nombre = $request -> edit_nombre;
            $data -> precio_venta = $request -> edit_precio_venta;
            $data -> precio_venta = $request -> edit_precio_venta;
            $data -> existencia = $request -> edit_existencia;
            $data -> factura = $request -> edit_factura;
            $data -> laboratorio_id = $request -> edit_laboratorio_id;
            $data -> fecha_vencimiento = $request -> edit_fecha_vencimiento;
            $data -> sucursal = $request -> edit_sucursal;
            $data -> generico = $request -> edit_generico;
            $data -> observaciones = $request -> edit_observaciones;
            $data -> fecha_updated =  date('Y-m-d');
            $data -> usuario = Auth::user()->email ;
            $data -> save();
            return back()
                    ->with('finalizado','Datos modificados exitosamente.');
        }
 
        /*
        *   Delete record
        */
        public function delete(Request $request)
        {
            $id = $request -> id;
            $data = medicina::find($id);
            //$response = $data -> delete();
            $data -> activo = false;
            $response = $data -> update();
            if($response)
                echo "Dato eliminado exitosamente.";
            else
                echo "There was a problem. Please try again later.";
        }

        
    public function existproduct(Request $request)
    {
        $info = medicina::join('sucursales as s','medicinas.sucursal','=','s.id')
            ->join('laboratorios as l','medicinas.laboratorio_id','=','l.id')
            ->select('medicinas.laboratorio_id', 'medicinas.codigo_producto', 'medicinas.nombre', 'medicinas.precio_compra', 'medicinas.precio_venta','medicinas.sucursal as id_suc','medicinas.existencia','medicinas.fecha_vencimiento','medicinas.generico', 's.nombre as sucursal', 'l.nombre as lab', 'medicinas.idunico')
            ->where('existencia','>', 0)
            ->where('codigo_producto','=', $request->id)
            ->where('sucursal','=', $request->sucursal)
            ->get();
    }


}
