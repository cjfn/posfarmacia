
@extends('layouts.app')



@section('content')

<meta name="csrf-token" content="{{ csrf_token() }}" />



<div class="container" ng-app="calc" ng-controller="myCtrl">
    <div class="row">
 
        <div class="col-md-12">

             
    <div class="panel panel-primary">
                <div class="panel-heading">CREA REPORTES
                </div>
    <div class="panel-body">
    <h2>Crea reporte de tus ventas, productos y mucho mas</h2>
    @if ($message = Session::get('success'))

      <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
      </div>
    @endif
    <div class="table-responsive">
    <table class="table table-bordered" id="MyTable">
    <center>
      <a href="{{ URL::previous() }}" class="btn btn-default"><span class="fa fa-mail-reply"></span>Atras</a>
  <h3> <strong>SELECCIONE</strong> su tipo de repote
    
    </center>
    <ul class="nav nav-tabs">
     @if(Auth::user()->nombre_tipo_usuario=='ADMIN')
           
	 <li class="active"><a data-toggle="tab" href="#home">Ventas</a></li>
      <li><a data-toggle="tab" href="#menu1">Ingresos</a></li>
      <li><a data-toggle="tab" href="#menu2">Ajustes/descargas</a></li>
                    @endif

       <li><a data-toggle="tab" href="#menu3">Fecha de vencimiento</a></li>
    </ul>

    <div class="tab-content">
	
      <div id="home" class="tab-pane fade in active">
        @if(Auth::user()->nombre_tipo_usuario=='ADMIN')
	<h3>Reporte de ventas por fecha</h3>
        <form action="" method="post">
              {{ csrf_field() }}
              <div class="form-group">
                <div class="form-group">
                  <label for="first_name">Fecha de inicio:</label>
                  <input type="date" tabindex="1" class="form-control" id="from1" name="from1" required="true">
                </div>
                <div class="form-group">
                  <label for="last_name">Fecha de fin:</label>
                  <input type="date" tabindex="2" class="form-control" id="to1" name="to1" required="true">
                </div>
                <div class="form-group">
                 <select id="sucursal"  name="sucursal" title="seleccione sucursal de bodega" required="true" tabindex="3" class="form-control">
                      @foreach($sucursal as $s)
                  
                      <option value="{{ $s -> id}}">Cod: {{ $s -> id}} - {{ $s -> nombre}} </option>
                  
                  @endforeach
                  </select>
                </div>
               
              </div>
              <center>
              <span class="btn btn-success btn-lg" tabindex="3" onclick="reporteventas()" onkeypress="reporteventas()"><span class="fa fa-file-pdf-o"></span>Ver reporte por  fechas</span>
              </center>
      </form>
      </div>
      <div id="menu1" class="tab-pane fade">
        <h3>Reporte de ingresos por fecha</h3>
        <form action="" method="post">
              {{ csrf_field() }}
              <div class="form-group">
                <div class="form-group">
                  <label for="first_name">Fecha de inicio:</label>
                  <input type="date" tabindex="1" class="form-control" id="from2" name="from2" required="true">
                </div>
                <div class="form-group">
                  <label for="last_name">Fecha de fin:</label>
                  <input type="date" tabindex="2" class="form-control" id="to2" name="to2" required="true">
                </div>
                 <div class="form-group">
                 <select id="sucursal2"  name="sucursal2" title="seleccione sucursal de bodega" required="true" tabindex="3" class="form-control">
                      @foreach($sucursal as $s)
                  
                      <option value="{{ $s -> id}}">Cod: {{ $s -> id}} - {{ $s -> nombre}} </option>
                  
                  @endforeach
                  </select>
                </div>
               
              </div>
              <center>
              <span class="btn btn-success btn-lg" tabindex="3" onclick="reportein()" onkeypress="reportein()"><span class="fa fa-file-pdf-o"></span>Ver reporte por  fechas</span>
              </center>
             

      </form>
     
    </div>
      
      <div id="menu2" class="tab-pane fade">
        <h3>Reporte de ajustes por fecha</h3>
        <form action="" method="post">
              {{ csrf_field() }}
              <div class="form-group">
                 
                <div class="form-group">
                  <label for="first_name">Fecha de inicio:</label>
                  <input type="date" tabindex="1" class="form-control" id="from3" name="from3" required="true">
                </div>
                <div class="form-group">
                  <label for="last_name">Fecha de fin:</label>
                  <input type="date" tabindex="2" class="form-control" id="to3" name="to3" required="true">
                </div>
                 <div class="form-group">
                 <select id="sucursal3"  name="sucursal3" title="seleccione sucursal de bodega" required="true" tabindex="3" class="form-control">
                      @foreach($sucursal as $s)
                  
                      <option value="{{ $s -> id}}">Cod: {{ $s -> id}} - {{ $s -> nombre}} </option>
                  
                  @endforeach
                  </select>
                </div>
               
              </div>
              <center>
              <span class="btn btn-success btn-lg" tabindex="3" onclick="reporteajustes()" onkeypress="reporteajustes()"><span class="fa fa-file-pdf-o"></span>Ver reporte por  fechas</span>
              </center>
      </form>
	 @endif
      </div>


       <div id="menu3" class="tab-pane fade">
        <h3>Reporte de fechas de vencimiento</h3>
        <form action="" method="post">
              {{ csrf_field() }}
              <div class="form-group">

                <div class="form-group">
                  <label for="first_name">Laboratorio:</label>
                  <span class="form-control">
                  <select class="itemName form-control" style="width:700px;" tabindex="1" id="laboratorio_id" name="laboratorio_id" required="true" ></select>
                </span>
                </div>
                <div class="form-group">
                  <label for="first_name">Fecha de inicio:</label>
                  <input type="date" tabindex="2" class="form-control" id="from4" name="from4" required="true">
                </div>
                <div class="form-group">
                  <label for="last_name">Fecha de fin:</label>
                  <input type="date" tabindex="3" class="form-control" id="to4" name="to4" required="true">
                </div>
                 <div class="form-group">
                 <select id="sucursal4"  name="sucursal4" title="seleccione sucursal de bodega" required="true" tabindex="3" class="form-control">
                      @foreach($sucursal as $s)
                  
                      <option value="{{ $s -> id}}">Cod: {{ $s -> id}} - {{ $s -> nombre}} </option>
                  
                  @endforeach
                  </select>
                </div>
               
              </div>
              <center>
              <span class="btn btn-success btn-lg" tabindex="3" onclick="reportevencimiento()" onkeypress="reportevencimiento()"><span class="fa fa-file-pdf-o"></span>Ver reporte por  fechas</span>
              </center>
      </form>
      </div>
    </div>
    
  </div>
    <input type="hidden" name="hidden_view" id="hidden_view" value="{{url('medicinas/view')}}">
    <input type="hidden" name="hidden_delete" id="hidden_delete" value="{{url('medicinas/delete')}}">
    
  </div>
  </div>
  </div>
  <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
<script src="http://code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<script type="text/javascript">



function printDiv(nombreDiv) {
     var contenido= document.getElementById(nombreDiv).innerHTML;
     var contenidoOriginal= document.body.innerHTML;

     document.body.innerHTML = contenido;

     window.print();

     document.body.innerHTML = contenidoOriginal;
}
//SELECT DE ALUMNO
      $('.itemName').select2({
        placeholder: 'Seleccione un Laboratorio',
        ajax: {
          url: '{{ url('/select2-autocomplete-ajax') }}',
          dataType: 'json',
          delay: 250,
          processResults: function (data) {
            return {
              results:  $.map(data, function (item) {
                    return {

                        text: item.nombre+' '+item.direccion,
                        id: item.id
                    }
                })
            };
          },
          cache: true
        }
      });

  $("#laboratorio_id").change(function(){
    
});
  function reporteventas()
  {
    //alert("me llamas");
    var from1 = document.getElementById("from1").value;
    var to1 = document.getElementById("to1").value;
    var sucursal = document.getElementById("sucursal").value;
    var hour = " 00:00:00"
    var from = from1.concat(hour);
    var to = to1.concat(hour);
    location.href="{{ url('/invoiceVentasEntreFecha?from=') }}"+ from + "&to="+to + "&sucursal="+sucursal;

  }

  function reportein()
  {
    //alert("me llamas");
    var from2 = document.getElementById("from2").value;
    var to2 = document.getElementById("to2").value;
    var sucursal = document.getElementById("sucursal2").value;
    var hour = " 00:00:00"
    var from = from2.concat(hour);
    var to = to2.concat(hour);
    location.href="{{ url('/invoiceProductosEntreFechapdf?from=') }}"+ from + "&to="+to+ "&sucursal="+sucursal;

  }

  function reporteajustes()
  {
    //alert("me llamas");
    var from3 = document.getElementById("from3").value;
    var to3 = document.getElementById("to3").value;
     var sucursal = document.getElementById("sucursal3").value;
    var hour = " 00:00:00"
    var from = from3.concat(hour);
    var to = to3.concat(hour);
    location.href="{{ url('/invoiceAjustesEntreFecha?from=') }}"+ from + "&to="+to+ "&sucursal="+sucursal;

  }

  function reportevencimiento()
  {
     var from4 = document.getElementById("from4").value;
    var to4 = document.getElementById("to4").value;
     var sucursal = document.getElementById("sucursal4").value;
    var lab = document.getElementById("laboratorio_id").value;
    var hour = " 00:00:00"
    var from = from4.concat(hour);
    var to = to4.concat(hour);
    location.href="{{ url('/invoiceVencimientosEntreFecha?from=') }}"+ from4 + "&to="+to4+"&lab="+lab+ "&sucursal="+sucursal;
    //http://localhost:8000/invoiceVencimientosEntreFecha?from=2020-08-08&to=2020-08-10&lab=4
    //location.href="{{ url('/invoiceVencimientosEntreFecha?from=') }}"+ from4 + "&to="+to4;
  }

  
</script>
  

           


          </div>

          <!-- /.col-lg-12 -->

      </div>



  </div>

@endsection
