<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\model\sucursal\sucursal;

class sucrusalesController extends Controller
{
    //
     public function index()
    {
         $data = sucursal::where('activo','=', '1')->get();
            return view('sucursal.index')->with('data',$data);
    }

   public function add(Request $request)
        {
            $data = new sucursal;
            $data -> nombre = $request -> nombre;
            $data -> direccion = $request -> direccion;
            $data -> telefono = $request -> telefono;
            $data -> encargado = $request -> encargado;
            $data -> save();
            return back()
                    ->with('success','Sucursal agregado exitosamente.');
        }

        /*
         * View data
         */
        public function view(Request $request)
        {
            if($request->ajax()){
                $id = $request->id;
                $info = sucursal::find($id);
                //echo json_decode($info);
                return response()->json($info);
            }
        }

        public function getlaboratorios(Request $request)
        {
            if($request->ajax()){
                
                $info = sucursal::all();
                //echo json_decode($info);
                return response()->json($info);
            }
        }

         /*
        *   Update data
        */
        public function update(Request $request)
        {
            $id = $request -> edit_id;
            $data = sucursal::find($id);
            $data -> nombre = $request -> edit_nombre;
            $data -> direccion = $request -> edit_direccion;
            $data -> telefono = $request -> edit_telefono;
            $data -> encargado = $request -> edit_encargado;
            $data -> save();
            return back()
                    ->with('finalizado','Datos modificados exitosamente.');
        }
 
        /*
        *   Delete record
        */
        public function delete(Request $request)
        {
            $id = $request -> id;
            $data = sucursal::find($id);
             $data -> activo = false;
            $response = $data -> update();
            if($response)
                echo "Dato eliminado exitosamente.";
            else
                echo "There was a problem. Please try again later.";
        }
}
