<?php

namespace App\model\traslados;

use Illuminate\Database\Eloquent\Model;

class traslados extends Model
{
    //
      protected $table = 'traslados';
	 protected $primarykey='id';
	 public $timestamps = false;
    
    protected $fillable = [

    	//ESTADOS - ENTREGADO, RECIBIDO
        'producto_codigo', 'sucursal_origen', 'sucursal_destino', 'usuario', 'fecha_updated', 'cantidad', 'estado', 'OBSERVACIONES', 'usuario_created', 'usuario_updated', 'fecha_vencimiento'
    ];
}
