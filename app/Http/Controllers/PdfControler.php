<?php

namespace App\Http\Controllers;



use Illuminate\Http\Request;

use App\model\ventas\ventas;
use App\model\factura\factura;
use App\model\ventas\detalle_factura;
use App\model\medicina\medicina;
use App\model\sucursal\sucursal;
use DB;
use Illuminate\Support\Facades\Input;
use Session;
use DOMDocument;
use Maatwebsite\Excel\Facades\Excel;

class PdfControler extends Controller
{
    

public function reportes(Request $request)
{
    $sucursal = sucursal::all();
     return view('reportes.index',['sucursal'=>$sucursal]);
    
}

public function productos(Request $request)
{
      $data = medicina::all();
       $date = date('Y-m-d');
        $invoice = "2222";
        $view =  \View::make('pdf.invoiceproductos', compact('data', 'date', 'invoice'))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view)->setPaper('legal', 'landscape');
        return $pdf->download('invoice');
}

    public function invoicethisventa(Request $request) 
    {

     

        
      $data = DB::table("detalle_facturas as df")
        ->join ("medicinas as m","df.idunico", "=","m.idunico")
        ->where("df.factura","=", $request->id)
        
        ->select(DB::raw("DISTINCT(m.idunico)"), "m.codigo_producto", "m.porcentaje_descuento", "m.nombre", "m.precio_venta", "df.id","df.cantidad", "df.fecha_created", "df.subtotal")
        ->get();

        $id = $request->id;
        $sucursal = $request->sucursal;
        

        //total de alunos
        $data_total = detalle_factura::join('facturas as f', 'detalle_facturas.factura','=','f.id')
    
        ->select(' df.id', 'detalle_facturas.codigo_producto')
         ->where('detalle_facturas.factura', $request->id)
        ->count();
         $mes = 'T';
        $year = date('Y');

         $data_total_ventas = detalle_factura::where('factura', $request->id)
         ->sum('subtotal');

        $date = date('Y-m-d');
        $invoice = "2222";
        $view =  \View::make('pdf.invoicethisventa', compact('data', 'data_total', 'data_total_ventas', 'date', 'invoice','mes','year','id','sucursal'))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view)->setPaper('letter', 'landscape');
        return $pdf->stream('invoice');
    }

    public function invoice() 
    {

        //traigo todos los alumnos con cursos  para pdf

        /*
        SELECT * FROM detalle_facturas as df join facturas as f on df.factura=f.id 
        join medicinas as m on df.codigo_producto=m.codigo_producto where df.fecha_vencimiento=m.fecha_vencimiento

        SELECT * FROM detalle_facturas as df left join facturas as f on df.factura=f.id join medicinas as m on df.fecha_vencimiento=m.fecha_vencimiento and df.codigo_producto=m.codigo_producto
        */
      
         
         $data=DB::table('detalle_facturas as df')
                ->join('facturas as f', 'df.factura', '=', 'f.id')
                 ->join('medicinas as m', 'df.idunico', '=', 'm.idunico')
                 ->orderBy('df.id', 'DESC')
                 
                
                
                    

            
                 ->get();
        

        //total de alunos
        $data_total = detalle_factura::join('facturas as f', 'detalle_facturas.factura','=','f.id')
       
        ->select(' df.id', 'detalle_facturas.codigo_producto')
        ->count();
         $mes = 'TODAS LAS VENTAS';
        $year = date('Y');

         $data_total_ventas = detalle_factura::sum('subtotal');

        $date = date('Y-m-d');
        $invoice = "2222";
        $view =  \View::make('pdf.invoice', compact('data', 'data_total', 'data_total_ventas', 'date', 'invoice','mes','year'))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view)->setPaper('legal', 'landscape');
        return $pdf->stream('invoice');
    }

     public function invoiceVentasPorFecha(Request $request) 
    {
        ini_set('max_execution_time', 60);
       
        $data = DB::table('detalle_facturas as df')
        ->join('facturas as f', 'df.factura', '=', 'f.id')
                 ->join('medicinas as m', 'df.idunico', '=', 'm.idunico')
                 ->orderBy('df.id', 'DESC')
        
        ->whereYear('df.fecha_created', '=', $request -> year)
        ->whereMonth('df.fecha_created', '=', $request -> mes)
        ->whereDay('df.fecha_created', '=', $request -> dia)
        
        ->select(DB::raw('DISTINCT(df.id)'), 'df.id', 'm.codigo_producto', 'df.cantidad', 'df.subtotal','df.fecha_created','m.nombre','m.precio_venta', 'f.id', 'f.usuario', 'm.porcentaje_descuento','m.precio_compra')
        ->get();



        //total de alunos
        $data_total =DB::table('detalle_facturas as df')
                 ->join('facturas as f', 'df.factura', '=', 'f.id')
                 ->leftjoin('medicinas as m', 'df.fecha_vencimiento', '=', 'm.fecha_vencimiento')

        ->whereYear('f.fecha_created', '=', $request -> year)
        ->whereMonth('f.fecha_created', '=', $request -> mes)
        ->whereDay('f.fecha_created', '=', $request -> dia)
        
        ->select(' df.id', 'm.codigo_producto')
        ->count();

         $data_total_ventas = detalle_factura::whereYear('fecha_created', '=', $request -> year)
        ->whereMonth('fecha_created', '=', $request -> mes)
        ->whereDay('fecha_created', '=', $request -> dia)
        ->sum('subtotal');

        $date = date('Y-m-d');
        $invoice = "2222";
        $mes = $request ->mes;
        $year = $request -> year;
        $view =  \View::make('pdf.invoice', compact('data', 'data_total', 'data_total_ventas', 'date', 'invoice','mes','year'))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view)->setPaper('legal', 'landscape');
        return $pdf->stream('invoice');
    }

     public function invoiceVentasEntreFecha(Request $request) 
    {

        /*
        $from='2018-03-24 00:00:00';
        $to='2018-03-25 00:00:00';
        */
        ini_set('max_execution_time', 60);

        $from= $request->from;
        $to = $request->to;
        $sucursal = $request->sucursal;
              $data=DB::table('detalle_facturas as df')
                 ->join('medicinas as m', 'df.idunico', '=', 'm.idunico')
                 ->select(DB::raw('DISTINCT(m.idunico)'), 'm.codigo_producto', 'm.nombre','m.precio_venta', 'm.porcentaje_descuento','m.precio_compra', 'df.cantidad', 'df.id as det','df.subtotal','df.fecha_created', 'df.factura as id', 'df.usuario')


                   ->orderBy('det', 'DESC')
                 
                 ->where('m.sucursal','=', $sucursal)
                 //->where('m.existencia','>',0)
                 ->whereBetween('df.fecha_created', array($from, $to))
                 
                   
                 ->get();
        
        $data_total = DB::table('facturas as f')
              ->join('detalle_facturas as df', 'df.factura', '=', 'f.id')
                 ->join('medicinas as m', 'df.idunico', '=', 'm.idunico')
                 ->orderBy('df.id', 'DESC')
                 ->where('m.sucursal','=', $sucursal)
                 ->whereBetween('df.fecha_created', array($from, $to))
        ->count();
         $data_total_ventas = DB::table('facturas as f')
              ->join('detalle_facturas as df', 'df.factura', '=', 'f.id')
                 ->join('medicinas as m', 'df.idunico', '=', 'm.idunico')
                 ->where('m.sucursal','=', $sucursal)
                 ->whereBetween('df.fecha_created', array($from, $to))
        ->sum('df.subtotal');


        $date = date('Y-m-d');
        $invoice = "2222";
        $mes = $request ->mes;
        $year = $request -> year;
        $view =  \View::make('pdf.invoice', compact('data', 'data_total', 'data_total_ventas', 'date', 'invoice','mes','year'))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view)->setPaper('legal', 'landscape');
        return $pdf->stream('invoice');
    }


    public function invoiceAjustesAll(Request $request) 
    {
        

        $mes = 'TODAS LAS VENTAS';
        $year = date('Y');
        $dia = date('d');
        
        ini_set('max_execution_time', 60);
        $data = medicina::join('sucursales as s', 'medicinas.sucursal','=','s.id')
        ->join('laboratorios as l','medicinas.laboratorio_id','=','l.id')
         ->orderBy('medicinas.fecha_updated', 'DESC')
        
        ->select(DB::raw('DISTINCT(medicinas.id)'), 'medicinas.codigo_producto','medicinas.aplica_descuento','medicinas.factura','medicinas.porcentaje_descuento','medicinas.nombre', 'medicinas.precio_compra', 'medicinas.precio_venta','medicinas.laboratorio_id','medicinas.sucursal', 'medicinas.fecha_created', 'medicinas.fecha_updated', 'medicinas.observaciones','medicinas.existencia','medicinas.fecha_vencimiento','medicinas.generico', 'medicinas.usuario', 's.nombre as sucursalname', 'l.nombre as lab')
       
        ->where('medicinas.fecha_updated', '!=', null)

        
        ->get();




        $date = date('Y-m-d');
        $invoice = "2222";
        $mes = $request ->mes;
        $year = $request -> year;
        $view =  \View::make('pdf.invoiceajustes', compact('data', 'date', 'invoice','mes','year','dia'))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view)->setPaper('a4', 'landscape');
        return $pdf->stream();
    }

    public function TopVentas(Request $request)
    {

        //select sum(cantidad), codigo_producto from detalle_facturas GROUP BY codigo_producto
        $data = DB::table("detalle_facturas")->select(DB::raw("SUM(cantidad) as cantidad"),"codigo_producto")
        ->groupBy("codigo_producto")
        ->orderBy("cantidad", "DESC")
        ->get(10);

        $datainverse = DB::table("detalle_facturas")->select(DB::raw("SUM(cantidad) as cantidad"),"codigo_producto")
        ->groupBy("codigo_producto")
        ->orderBy("cantidad", "ASC")
        ->get(5);

        $date = date('Y-m-d');
        $invoice = "Top de ventas realizadas";
        $mes = $request ->mes;
        $year = $request -> year;
        $view =  \View::make('pdf.invoiceTopVentas', compact('data','datainverse', 'date', 'invoice','mes','year','dia'))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view)->setPaper('letter', 'portrait');
        return $pdf->stream();

    }

     public function invoiceAjustesFecha(Request $request) 
    {
        ini_set('max_execution_time', 60);
    
        $data = medicina::join('sucursales as s', 'medicinas.sucursal','=','s.id')
        ->join('laboratorios as l','medicinas.laboratorio_id','=','l.id')
        ->orderBy('medicinas.fecha_updated', 'DESC')
        
        ->select(DB::raw('DISTINCT(medicinas.id)'), 'medicinas.codigo_producto','medicinas.aplica_descuento','medicinas.factura','medicinas.porcentaje_descuento','medicinas.nombre', 'medicinas.precio_compra', 'medicinas.precio_venta','medicinas.laboratorio_id','medicinas.sucursal', 'medicinas.fecha_created', 'medicinas.fecha_updated', 'medicinas.observaciones','medicinas.existencia','medicinas.fecha_vencimiento','medicinas.generico', 'medicinas.usuario','s.nombre as sucursalname', 'l.nombre as lab')
       
        ->where('medicinas.fecha_updated', '!=', null)

        ->whereYear('medicinas.fecha_updated', '=', $request -> year)
        ->whereMonth('medicinas.fecha_updated', '=', $request -> mes)
        ->whereDay('medicinas.fecha_updated', '=', $request -> dia)
        
        
        ->get();




        $date = date('Y-m-d');
        $invoice = "2222";
        $mes = $request ->mes;
        $year = $request -> year;
        $dia = $request -> dia;
        $view =  \View::make('pdf.invoiceajustes', compact('data', 'date', 'invoice','mes','year','dia'))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view)->setPaper('legal', 'landscape');
        return $pdf->stream('invoice');
    }

    public function invoiceAjustesEntreFecha(Request $request) 
    {

        /*
        $from='2018-03-24 00:00:00';
        $to='2018-03-25 00:00:00';
        */
        ini_set('max_execution_time', 60);
        $from= $request->from;
        $to = $request->to;
        $sucursal = $request->sucursal;
        $data=medicina::join('sucursales as s', 'medicinas.sucursal','=','s.id')
        ->join('laboratorios as l','medicinas.laboratorio_id','=','l.id')
        ->orderBy('medicinas.fecha_updated', 'DESC')
        
        ->select(DB::raw('DISTINCT(medicinas.id)'), 'medicinas.codigo_producto','medicinas.aplica_descuento','medicinas.factura','medicinas.porcentaje_descuento','medicinas.nombre', 'medicinas.precio_compra', 'medicinas.precio_venta','medicinas.laboratorio_id','medicinas.sucursal', 'medicinas.fecha_created', 'medicinas.fecha_updated', 'medicinas.observaciones','medicinas.existencia','medicinas.fecha_vencimiento','medicinas.generico', 'medicinas.usuario','s.nombre as sucursalname', 'l.nombre as lab')
       
                    ->where('medicinas.fecha_updated', '!=', null)
                     ->where('medicinas.sucursal','=', $sucursal)

                 ->whereBetween('medicinas.fecha_updated', array($from, $to))
                 ->get();
        //total de alunos
       

        $date = date('Y-m-d');
        $invoice = "2222";
        $mes = $request ->mes;
        $year = $request -> year;
        $dia = $request -> dia;
        $view =  \View::make('pdf.invoiceajustes', compact('data', 'date', 'invoice','mes','year','dia'))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view)->setPaper('legal', 'landscape');
        return $pdf->stream('invoice');
    }

      public function invoiceVencimientosEntreFecha(Request $request) 
    {

        
        //$from='2018-06-01';
        //$to='2018-06-03';
        
        
        $from= $request->from;
        $to = $request->to;
        $lab = $request->lab;
        $sucursal = $request->sucursal;
        ini_set('max_execution_time', 60);
        $data = DB::table('medicinas')
                 ->join('sucursales as s', 'medicinas.sucursal','=','s.id')
                 ->join('laboratorios as l','medicinas.laboratorio_id','=','l.id')
                 
                 //->where('df.factura', '=', $request->id)
                   //->where('medicinas.fecha_vencimiento','>=', $from)
                   ->where('medicinas.sucursal','=', $sucursal)
                 ->whereBetween('medicinas.fecha_vencimiento', array($from, $to))
                 ->where('medicinas.laboratorio_id','=', $lab)
                 ->where('medicinas.existencia','>', 0)
                 ->orderBy('medicinas.fecha_vencimiento', 'ASC')
                 ->select( 'medicinas.codigo_producto','medicinas.aplica_descuento','medicinas.factura','medicinas.porcentaje_descuento','medicinas.nombre', 'medicinas.precio_compra', 'medicinas.precio_venta','medicinas.laboratorio_id','medicinas.sucursal', 'medicinas.fecha_created', 'medicinas.fecha_updated', 'medicinas.observaciones','medicinas.existencia','medicinas.fecha_vencimiento','medicinas.generico', 'medicinas.usuario','s.nombre as sucursalname', 'l.nombre as lab')

         
                 
 
                 ->get();
        

        $date = date('Y-m-d');
        $invoice = "2222";
        $mes = $request ->mes;
        $year = $request -> year;
        $dia = $request -> dia;
        /*
        $view =  \View::make('pdf.invoicevencimientos', compact('data', 'date', 'invoice','mes','year','dia'))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view)->setPaper('legal', 'landscape');
        return $pdf->download('invoice');
        */


         return view('pdf.invoicevencimientos',['data'=>$data,'date'=>$date,'mes'=>$mes, 'year' => $year, 'from'=> $from, 'to'=>$to]);
   
    }


        public function invoiceProductosAll(Request $request) 
    {

      

        Excel::create('Documento', function($excel){
            $excel->sheet('Datos', function($sheet){
                $data = DB::table('medicinas')
                 ->join('sucursales as s', 'medicinas.sucursal','=','s.id')
                 ->join('laboratorios as l','medicinas.laboratorio_id','=','l.id')
       
        
        ->where('medicinas.existencia','>', 0)
        
        ->select(DB::raw('DISTINCT(medicinas.id)'), 'medicinas.codigo_producto','medicinas.aplica_descuento','medicinas.factura','medicinas.porcentaje_descuento','medicinas.nombre', 'medicinas.precio_compra', 'medicinas.fecha_created', 'medicinas.fecha_updated', 'medicinas.precio_venta','medicinas.laboratorio_id','medicinas.sucursal','medicinas.observaciones', 'medicinas.usuario','medicinas.existencia','medicinas.fecha_vencimiento','medicinas.generico', 's.nombre as sucursalname', 'l.nombre as lab')
        ->orderBy('medicinas.codigo_producto', 'ASC')
        ->get();
        $data= json_decode( json_encode($data), true);
        $sheet->fromArray($data);
            });
        })->export('xls');
    }


        public function invoiceProductosFecha(Request $request) 
    {

       //ini_set('max_execution_time', 60);
        $data = medicina::join('sucursales as s', 'medicinas.sucursal','=','s.id')
        ->join('laboratorios as l','medicinas.laboratorio_id','=','l.id')
        ->orderBy('medicinas.codigo_producto', 'ASC')
        
        ->select(DB::raw('DISTINCT(medicinas.id)'), 'medicinas.codigo_producto','medicinas.aplica_descuento','medicinas.factura','medicinas.porcentaje_descuento','medicinas.nombre', 'medicinas.precio_compra', 'medicinas.fecha_created', 'medicinas.fecha_updated', 'medicinas.precio_venta','medicinas.laboratorio_id','medicinas.sucursal','medicinas.observaciones','medicinas.existencia','medicinas.fecha_vencimiento','medicinas.usuario','medicinas.generico', 's.nombre as sucursalname', 'l.nombre as lab')
        ->whereYear('medicinas.fecha_created', '=', $request -> year)
        ->whereMonth('medicinas.fecha_created', '=', $request -> mes)

        ->get();




        $date = date('Y-m-d');
        $invoice = "2222";
        $mes = $request ->mes;
        $year = $request -> year;
        $view =  \View::make('pdf.invoiceingresos', compact('data', 'date', 'invoice','mes','year'))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view)->setPaper('legal', 'landscape');
        return $pdf->stream('invoice');
    }

    public function invoiceProductosEntreFecha(Request $request) 
    {
        //ini_set('max_execution_time', 60);
        

        /*
        
       $data=medicina::join('sucursales as s', 'medicinas.sucursal','=','s.id')
        ->join('laboratorios as l','medicinas.laboratorio_id','=','l.id')
        ->orderBy('medicinas.fecha_updated', 'DESC')
        
        ->select(DB::raw('DISTINCT(medicinas.id)'), 'medicinas.codigo_producto','medicinas.aplica_descuento','medicinas.factura','medicinas.porcentaje_descuento','medicinas.nombre', 'medicinas.precio_compra', 'medicinas.precio_venta','medicinas.laboratorio_id','medicinas.sucursal', 'medicinas.fecha_created', 'medicinas.fecha_updated', 'medicinas.observaciones','medicinas.existencia','medicinas.fecha_vencimiento','medicinas.generico', 'medicinas.usuario','s.nombre as sucursalname', 'l.nombre as lab')
       
        //->where('medicinas.fecha_updated', '!=', null)

        ->where('medicinas.existencia','>', 0)
        ->whereBetween('medicinas.fecha_created', array($from, $to))
        

        ->get();




        $date = date('Y-m-d');
        $invoice = "2222";
        $mes = $request ->mes;
        $year = $request -> year;
        $view =  \View::make('pdf.invoiceingresos', compact('data', 'date', 'invoice','mes','year'))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view)->setPaper('legal', 'landscape');
        return $pdf->stream('invoice');
        */
         $sucursal = $request->sucursal;
         $from= $request->from;
        $to = $request->to;

           Excel::create('Ingresos', function($excel) use ($from, $to)  {
            $excel->sheet('Datos', function($sheet) use ($from, $to)  {
               
                $data=medicina::join('sucursales as s', 'medicinas.sucursal','=','s.id')
        ->join('laboratorios as l','medicinas.laboratorio_id','=','l.id')
        ->orderBy('medicinas.fecha_updated', 'DESC')
        
        ->select(DB::raw('DISTINCT(medicinas.id)'), 'medicinas.codigo_producto','medicinas.aplica_descuento','medicinas.factura','medicinas.porcentaje_descuento','medicinas.nombre', 'medicinas.precio_compra', 'medicinas.precio_venta','medicinas.laboratorio_id','medicinas.sucursal', 'medicinas.fecha_created', 'medicinas.fecha_updated', 'medicinas.observaciones','medicinas.existencia','medicinas.fecha_vencimiento','medicinas.generico', 'medicinas.usuario','s.nombre as sucursalname', 'l.nombre as lab')
       
        //->where('medicinas.fecha_updated', '!=', null)

        //->where('medicinas.existencia','>', 0)
         ->where('medicinas.sucursal','=', $sucursal)
        ->whereBetween('medicinas.fecha_created', array($from, $to))
        

        ->get();
        $data= json_decode( json_encode($data), true);
        $sheet->fromArray($data);
            });
        })->export('xls');
    }
 
 
   

public function invoiceProductosEntreFechapdf(Request $request) 
    {
         $from= $request->from;
        $to = $request->to;
        //ini_set('max_execution_time', 60);
        
        //$data = medicina::all();
         $sucursal = $request->sucursal;


         $data=DB::table('medicinas as m')
              ->join('laboratorios as l', 'm.laboratorio_id', '=', 'l.id')
                 ->join('sucursales as s', 'm.sucursal', '=', 's.id')
                 ->orderBy('m.fecha_created', 'ASC')
                 ->where('m.sucursal','=', $sucursal)
                 ->whereBetween('m.fecha_created', array($from, $to))
                 ->select('m.codigo_producto','m.aplica_descuento','m.factura','m.porcentaje_descuento','m.nombre', 'm.precio_compra', 'm.precio_venta','m.laboratorio_id','m.sucursal', 'm.fecha_created', 'm.fecha_updated', 'm.observaciones','m.existencia','m.fecha_vencimiento','m.generico', 'm.usuario','s.nombre as sucursalname', 'l.nombre as lab')
                 ->get();



        $date = date('Y-m-d');
        $invoice = "2222";
        $mes = date('m');
        $year = date('Y');
        /*
        $view =  \View::make('pdf.invoiceingresos', compact('data', 'date', 'invoice','mes','year'))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view)->setPaper('legal', 'landscape');
        return $pdf->stream('invoice');
        */

         return view('pdf.invoiceingresos',['data'=>$data,'date'=>$date,'mes'=>$mes, 'year' => $year, 'from'=> $from, 'to'=>$to]);
   
        

 
   
    }
}
