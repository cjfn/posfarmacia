<?php

namespace App\Http\Controllers;

	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;

	use App\model\laboratorio\laboratorio;
    use DB;

class laboratoriosController extends Controller
{
    //
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $data = laboratorio::where('activo','=', '1')->get();
            return view('laboratorio.index')->with('data',$data);
    }

   public function add(Request $request)
        {
            $data = new laboratorio;
            $data -> nombre = $request -> nombre;
            $data -> direccion = $request -> direccion;
            $data -> telefono = $request -> telefono;
            $data -> correo = $request -> correo;
            $data -> save();
            return back()
                    ->with('success','Laboratorio agregado exitosamente.');
        }

        /*
         * View data
         */
        public function view(Request $request)
        {
            if($request->ajax()){
                $id = $request->id;
                $info = laboratorio::find($id);
                //echo json_decode($info);
                return response()->json($info);
            }
        }

        public function getlaboratorios(Request $request)
        {
            if($request->ajax()){
                
                $info = laboratorio::all();
                //echo json_decode($info);
                return response()->json($info);
            }
        }

         /*
        *   Update data
        */
        public function update(Request $request)
        {
            $id = $request -> edit_id;
            $data = laboratorio::find($id);
            $data -> nombre = $request -> edit_nombre;
            $data -> direccion = $request -> edit_direccion;
            $data -> telefono = $request -> edit_telefono;
            $data -> correo = $request -> edit_correo;
            $data -> save();
            return back()
                    ->with('finalizado','Datos modificados exitosamente.');
        }
 
        /*
        *   Delete record
        */
        public function delete(Request $request)
        {
            $id = $request -> id;
            $data = laboratorio::find($id);
            $data -> activo = false;
            $response = $data -> update();
            if($response)
                echo "Dato eliminado exitosamente.";
            else
                echo "There was a problem. Please try again later.";
        }

          public function dataAjax(Request $request)
    {
      $data = [];

        if($request->has('q')){
            $search = $request->q;
            $data = DB::table("laboratorios")
                ->select("id","nombre","direccion")
                ->where('nombre','LIKE',"%$search%")
                ->where('activo','=', '1')
                ->get();
        }

        return response()->json($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
          $create = laboratorio::create($request->all());
        return response()->json($create);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        laboratorio::find($id)->delete();
        return response()->json(['done']);
    }
}
