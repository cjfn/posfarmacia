<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::resource('ventas','apiController', ['only'=>['index','store','show','update','destroy']]);



Route::resource('detalle','apiDetalleController', ['only'=>['index','store','show','update','destroy']]);


//Route::post('facturas/update', 'apiController@update');
Route::resource('productos','apiProductosController', ['only'=>['index','store','show','update','destroy','bySucursal']]);


Route::resource('bitacora','bitacoraController', ['only'=>['index','store','show','update','destroy']]);



Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');
