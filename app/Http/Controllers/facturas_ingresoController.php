<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\model\facturas_ingreso\facturas_ingreso;

class facturas_ingresoController extends Controller
{
    //
      public function index()
    {
         $data = facturas_ingreso::all();
            return view('facturas_ingreso.index')->with('data',$data);
    }

   public function add(Request $request)
        {
            $data = new facturas_ingreso;
            $data -> serie = $request -> serie;
            $data -> numero = $request -> numero;
            $data -> cantidad_total = $request -> cantidad_total;
            $data -> total_factura_ingreso = $request -> total_ingreso;
            $data -> observaciones = $request -> observaciones;
            $data -> save();
            return back()
                    ->with('success','Factura de ingreso agregado exitosamente.');
        }

        /*
         * View data
         */
        public function view(Request $request)
        {
            if($request->ajax()){
                $id = $request->id;
                $info = total_ingreso::find($id);
                //echo json_decode($info);
                return response()->json($info);
            }
        }
}
